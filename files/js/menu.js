var _today = new Date();
var selectedDate = _today;
var clickedIndex = null;
var menuData = null;
var currentIndex = 0;
var currentType = -1;
var currentAge = -1;
var currentH1 = null;
var kcal = '열량(kcal)/단백질(g)';
$(function () {

    $('#date').html(_today.getFullYear() + '. ' + (_today.getMonth() + 1));
    loadMenu();
});

function loadMenu() {

    var year = selectedDate.getFullYear();
    var month = selectedDate.getMonth() + 1;
    var type = $('#type').val();
    var age = $('#age').val();

    console.log("선택된 달 : " + month);
    console.log("선택된 타입 : " + type);
    console.log("선택된 연령 : " + age);

    $('#menu').empty();
    $('#link').children().remove();

    $.post('menu/get',
        {
            age: age,
            year: year,
            month: month,
            type: type
        },
        function (response, status) {
            if (response) {
                // console.log(response);
                var json_response = JSON.parse(response);

                if (json_response.length !== 0) {

                    var btnDownload = '<div id="downloadMenu" class="right">\n' +
                        '                            엑셀로 내보내기\n' +
                        '                        </div>';
                    $('#link').append(btnDownload);

                    menuData = json_response;
                    drawMenu(json_response);
                }
            } else {

            }
        }
    )
        .fail(function (response) {
            // alert(response.responseText);
        });
}

function drawMenu(json_response) {
    var type = $('#type').val();
    var week = 0;
    var list = 0;
    var count = 1;


    var menuDate = new Date(selectedDate.getFullYear(), selectedDate.getMonth(), selectedDate.getDate());

    menuDate.setDate(1);

    var startWeekNumber = menuDate.getDay();

    console.log('시작 일 : ' + menuDate);
    console.log('시작 요일 : ' + startWeekNumber);
    console.log('타입 : ' + type);

    var lastDayOfMonth = new Date(menuDate.getFullYear(), menuDate.getMonth() + 1, 0);
    lastDayOfMonth = lastDayOfMonth.getDate();


    var listCount = 0;

    console.log('마지막 일 : ' + lastDayOfMonth);

    var date_ul = '<table><tr><td class="head">날짜</td>';
    var note_ul = '<table><tr><td class="side">비고</td>';

    switch (type) {

        case '0':
        case '1':
        case '2':
            var menu0_ul = '<table><tr><td class="side">오전 간식</td>';
            var menu1_ul = '<table><tr><td class="side">점심</td>';
            var menu2_ul = '<table><tr><td class="side">오후 간식</td>';
            var kcal_ul = '<table><tr><td class="side">열량(kcal)/단백질(g)</td>';
            var salt_ul = '<table><tr><td class="side">염도</td>';
            break;
        case '3':
            var menu0_ul = '<table><tr><td class="side">저녁</td>';
            var kcal_ul = '<table><tr><td class="side">열량(kcal)/단백질(g)</td>';
            var salt_ul = '<table><tr><td class="side">염도</td>';
            break;
        case '4':
            var menu0_ul = '<table><tr><td class="side">초기</td>';
            var menu1_ul = '<table><tr><td class="side">중기</td>';
            var menu2_ul = '<table><tr><td class="side">오전 간식</td>';
            var menu3_ul = '<table><tr><td class="side">후기</td>';
            var menu4_ul = '<table><tr><td class="side">오후 간식</td>';
            break;
    }


    for (var i = 1; i < startWeekNumber; i++) {
        date_ul += '<td class="head"></td>';
        note_ul += '<td class="foodInfo"></td>';
        switch (type) {

            case '0':
            case '1':
            case '2':
                menu0_ul += '<td></td>';
                menu1_ul += '<td></td>';
                menu2_ul += '<td></td>';
                kcal_ul += '<td></td>';
                salt_ul += '<td></td>';
                break;
            case '3':
                if (menuDate.getDay() != 6) {
                    menu0_ul += '<li class="foodInfo"></li>';
                    kcal_ul += '<li class="foodInfo"></li>';
                    salt_ul += '<li class="foodInfo"></li>';
                }
                break;
            case '4':
                menu0_ul += '<td></td>';
                menu1_ul += '<td></td>';
                menu2_ul += '<td></td>';
                menu3_ul += '<td></td>';
                menu4_ul += '<td></td>';
                break;
        }

    }

    while (count <= lastDayOfMonth) {

        if (menuDate.getDay() == 0) {

            date_ul += '</tr></table>';
            $('#menu').append(date_ul);
            date_ul = '<table><tr><td class="head">날짜</td>';


            switch (type) {

                case '0':
                case '1':
                case '2':
                    menu0_ul += '</tr>';
                    menu1_ul += '</tr>';
                    menu2_ul += '</tr>';
                    $('#menu').append(menu0_ul);
                    $('#menu').append(menu1_ul);
                    $('#menu').append(menu2_ul);

                    menu0_ul = '<table><tr><td class="side">오전 간식</td>';
                    menu1_ul = '<table><tr><td class="side">점심</td>';
                    menu2_ul = '<table><tr><td class="side">오후 간식</td>';

                    kcal_ul += '</tr>';
                    $('#menu').append(kcal_ul);
                    kcal_ul = '<table><tr><td class="side">칼로리</td>';

                    salt_ul += '</tr>';
                    $('#menu').append(salt_ul);
                    salt_ul = '<table><tr><td class="side">염도</td>';
                    break;
                case '3':
                    menu0_ul += '</tr>';
                    $('#menu').append(menu0_ul);

                    menu0_ul = '<table><tr><td class="side">저녁</td>';

                    kcal_ul += '</tr>';
                    $('#menu').append(kcal_ul);
                    kcal_ul = '<table><tr><td class="side">열량(kcal)/단백질(g)</td>';

                    salt_ul += '</tr>';
                    $('#menu').append(salt_ul);
                    salt_ul = '<table><tr><td class="side">염도</td>';

                    break;
                case '4':

                    menu0_ul += '</tr>';
                    menu1_ul += '</tr>';
                    menu2_ul += '</tr>';
                    menu3_ul += '</tr>';
                    menu4_ul += '</tr>';

                    $('#menu').append(menu0_ul);
                    $('#menu').append(menu1_ul);
                    $('#menu').append(menu2_ul);
                    $('#menu').append(menu3_ul);
                    $('#menu').append(menu4_ul);

                    menu0_ul = '<table><tr><td class="side">초기</td>';
                    menu1_ul = '<table><tr><td class="side">중기</td>';
                    menu2_ul = '<table><tr><td class="side">오전 간식</td>';
                    menu3_ul = '<table><tr><td class="side">후기</td>';
                    menu4_ul = '<table><tr><td class="side">오후 간식</td>';

                    break;
            }
            note_ul += '</tr></table>';
            $('#menu').append(note_ul);
            note_ul = '<table><tr><td class="side">비고</td>';
        } else {


            switch (type) {

                case '0':
                case '1':
                case '2':
                    var kcal = json_response[listCount].menu_kcal + '/' + json_response[listCount].menu_protein;
                    var salt = json_response[listCount].menu_salt != null ? json_response[listCount].menu_salt + '%' : '염도 입력';
                    var note = json_response[listCount].menu_note != null ? json_response[listCount].menu_note : '<img src="files/css/images/modify_button.svg"/>';
                    date_ul += '<td class="head">\n' +
                        '                        ' + menuDate.getDate() + '\n' +
                        '                    </td>';
                    kcal_ul += '<td class="foodInfo">\n' +
                        '                        ' + kcal + '\n' +
                        '                    </td>';
                    salt_ul += '<td class="salt"><div class ="foodInfo"><ul><li class="pointColor1" value=' + listCount + ' >\n' +
                        '                        ' + salt + '\n' +
                        '                    </li></ul></div></td>';
                    note_ul += '<td class="note"><div class ="foodInfo"><ul><li value=' + listCount + ' >\n' +
                        '                        ' + note + '\n' +
                        '                    </li></ul></div></td>';

                    console.log(count);
                    console.log(json_response[listCount].menu1);
                    var menu1 = commaToBr(json_response[listCount].menu1) + '</br>' + commaToBr(json_response[listCount].menu2) + '</br>' + commaToBr(json_response[listCount].menu3) + '</br>' + commaToBr(json_response[listCount].menu4) + '</br>' + commaToBr(json_response[listCount].menu5);
                    menu0_ul += '<td><div class ="foodInfo"><ul><li>' + commaToBr(json_response[listCount].menu0) + '</li></ul></div></td>';
                    menu1_ul += '<td><div class ="foodInfo"><ul><li>' + menu1 + '</li></ul></div></td>';
                    menu2_ul += '<td><div class ="foodInfo"><ul><li>' + commaToBr(json_response[listCount].menu6) + '</li></ul></div></td>';
                    listCount += 1;
                    break;
                case '3':
                    if (menuDate.getDay() != 6) {

                        var kcal = json_response[listCount].menu_kcal + '/' + json_response[listCount].menu_protein;
                        var salt = json_response[listCount].menu_salt != null ? json_response[listCount].menu_salt + '%' : '염도 입력';
                        var note = json_response[listCount].menu_note != null ? json_response[listCount].menu_note : '<img src="files/css/images/modify_button.svg"/>';

                        date_ul += '<td class="head">\n' +
                            '                        ' + menuDate.getDate() + '\n' +
                            '                    </td>';
                        kcal_ul += '<td class="foodInfo">\n' +
                            '                        ' + kcal + '\n' +
                            '                    </td>';
                        salt_ul += '<td class="salt"><div class ="foodInfo"><ul><li class="pointColor1" value=' + listCount + ' >\n' +
                            '                        ' + salt + '\n' +
                            '                    </li></ul></div></td>';
                        note_ul += '<td class="note"><div class ="foodInfo"><ul><li value=' + listCount + ' >\n' +
                            '                        ' + note + '\n' +
                            '                    </li></ul></div></td>';


                        var menu1 = getSplitedString(json_response[listCount].menu0) + '<br>' + getSplitedString(json_response[listCount].menu1) + '<br>' + getSplitedString(json_response[listCount].menu2) + '<br>' + getSplitedString(json_response[listCount].menu3) + '<br>' + getSplitedString(json_response[listCount].menu4);
                        menu0_ul += '<td><div class ="foodInfo"><ul><li>' + menu1 + '</li></ul></div></td>';
                        listCount += 1;
                    }
                    break;
                case '4':
                    var note = json_response[listCount].menu_note != null ? json_response[listCount].menu_note : '<img src="files/css/images/modify_button.svg"/>';

                    date_ul += '<td class="head">\n' +
                        '                        ' + menuDate.getDate() + '\n' +
                        '                    </td>';
                    note_ul += '<td class="note"><div class ="foodInfo"><ul><li value=' + listCount + ' >\n' +
                        '                        ' + note + '\n' +
                        '                    </li></ul></div></td>';

                    menu0_ul += '<td><div class ="foodInfo"><ul><li>' + commaToBr(json_response[listCount].menu0) + '</li></ul></div></td>';
                    menu1_ul += '<td><div class ="foodInfo"><ul><li>' + commaToBr(json_response[listCount].menu1) + '</li></ul></div></td>';
                    menu2_ul += '<td><div class ="foodInfo"><ul><li>' + commaToBr(json_response[listCount].menu2) + '</li></ul></div></td>';
                    menu3_ul += '<td><div class ="foodInfo"><ul><li>' + commaToBr(json_response[listCount].menu3) + '</li></ul></div></td>';
                    menu4_ul += '<td><div class ="foodInfo"><ul><li>' + commaToBr(json_response[listCount].menu4) + '</li></ul></div></td>';
                    listCount += 1;

                    break;
            }

        }
        count += 1;
        menuDate.setDate(menuDate.getDate() + 1);

    }

    switch (type) {

        case '0':
        case '1':
        case '2':

            if (menuDate.getDay() != 0) {
                for (var j = menuDate.getDay(); j <= 6; j++) {
                    menu0_ul += '<td></td>';
                    menu1_ul += '<td></td>';
                    menu2_ul += '<td></td>';
                    date_ul += '<td class="head"></td>';
                    kcal_ul += '<td></td>';
                    salt_ul += '<td></td>';
                    note_ul += '<td></td>';
                }
            }

            date_ul += '</tr></table>';
            $('#menu').append(date_ul);


            menu0_ul += '</tr></table>';
            menu1_ul += '</tr></table>';
            menu2_ul += '</tr></table>';
            $('#menu').append(menu0_ul);
            $('#menu').append(menu1_ul);
            $('#menu').append(menu2_ul);

            kcal_ul += '</tr></table>';
            $('#menu').append(kcal_ul);

            salt_ul += '</tr></table>';
            $('#menu').append(salt_ul);

            note_ul += '</tr></table>';
            $('#menu').append(note_ul);

            break;
        case '3':


            if (menuDate.getDay() != 0) {
                for (var j = menuDate.getDay(); j <= 5; j++) {
                    menu0_ul += '<td></td>';
                    date_ul += '<td class="head"></td>';
                    kcal_ul += '<td></td>';
                    salt_ul += '<td></td>';
                    note_ul += '<td></td>';
                }
            }
            date_ul += '</tr></table>';
            $('#menu').append(date_ul);
            menu0_ul += '</tr></table>';
            $('#menu').append(menu0_ul);

            kcal_ul += '</tr></table>';
            $('#menu').append(kcal_ul);

            salt_ul += '</tr></table>';
            $('#menu').append(salt_ul);

            note_ul += '</tr></table>';
            $('#menu').append(note_ul);


            break;
        case '4':
            if (menuDate.getDay() != 0) {
                for (var j = menuDate.getDay(); j <= 6; j++) {
                    menu0_ul += '<td></td>';
                    menu1_ul += '<td></td>';
                    menu2_ul += '<td></td>';
                    menu3_ul += '<td></td>';
                    menu4_ul += '<td></td>';
                    date_ul += '<td class="head"></td>';
                    note_ul += '<td></td>';
                }
            }
            date_ul += '</tr></table>';
            $('#menu').append(date_ul);

            menu0_ul += '</tr></table>';
            menu1_ul += '</tr></table>';
            menu2_ul += '</tr></table>';
            menu3_ul += '</tr></table>';
            menu4_ul += '</tr></table>';
            $('#menu').append(menu0_ul);
            $('#menu').append(menu1_ul);
            $('#menu').append(menu2_ul);
            $('#menu').append(menu3_ul);
            $('#menu').append(menu4_ul);

            note_ul += '</tr></table>';
            $('#menu').append(note_ul);

            break;
    }


}

function getSplitedString(string) {

    var jbSplit = string.split('\n');
    if (jbSplit.length > 1) {
        return jbSplit[0] + '<br>' + jbSplit[1];
    } else {
        return string
    }

}


function stringToDate(string) {
    var result = new Date(string);
    return result;
}


$(document).on('click', '#before', function () {

    console.log('before');
    // var currentDate = selectedDate;

    console.log(selectedDate);
    selectedDate.setMonth(selectedDate.getMonth() - 1, 1);
    console.log(selectedDate);
    //
    //
    //
    // var weeklyPick = stringToDate(newDate);
    // console.log(weeklyPick);
    // selectedDate = weeklyPick;

    $('#date').html(selectedDate.getFullYear() + '. ' + (selectedDate.getMonth() + 1));

    requestType();
    loadMenu();

});

$(document).on('click', '#after', function () {
    console.log('after');
    // var currentDate = selectedDate;
    console.log(selectedDate);
    selectedDate.setMonth(selectedDate.getMonth() + 1, 1);
    console.log(selectedDate);
    // var weeklyPick = stringToDate(newDate);
    //
    // selectedDate = weeklyPick;

    $('#date').html(selectedDate.getFullYear() + '. ' + (selectedDate.getMonth() + 1));

    requestType();
    loadMenu();
});


$(document).on('click', '#fileUpload', function () {
    console.log('식단등록');

    $.get("/menu/show_popup", function (data) { //list의 버튼 클릭시 외부 url태그 가져오기
        $('#uploadPopUp').addClass('popup');
        $('.popup').append(data);
    });
});

$(document).on('change', '#type', function () {

    currentType = $('#type').val();
    loadMenu();

});

$(document).on('change', '#age', function () {

    currentAge = $('#age').val();

    requestType();
    // loadMenu();

    $('#menu').empty();
    $('#link').children().remove();

});

function requestType() {

    console.log('타입 요청');

    $.get("menu/get_type", {age: currentAge, year: selectedDate.getFullYear(), month: selectedDate.getMonth() + 1})
        .done(function (data) {

            $('#type').children().remove();
            var list = JSON.parse(data);

            var option = '<option value="-1">식단 유형</option>';

            for (var i = 0; i < list.length; i++) {

                option += '<option value="' + list[i].meals_type + '">' + getMealsTypeString(list[i].meals_type) + '</option>';

            }
            $('#type').append(option);

            currentType = -1;

            $('#type').val(currentType);

            console.log('타입 리셋'+ $('#type').val());

        })
        .fail(function () {
            console.log("error");
        })
}


$(document).on('click', '#submitBtn', function () {


    var month = $('#popMonth').val();
    var age = $('#popAge').val();
    var type = $('#popType').val();

    console.log(month);
    var form = $('#myForm')[0];
    var data = new FormData(form);
    data.append('age', age);
    data.append('month', month);
    data.append('type', type);
    data.append('check', false);

    $.ajax({
        url: 'menu/do_upload',
        type: "post",
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        async: false,
        success: function (data) {
            console.log(data);

            if (data === 'exist') {
                var confirmResult = confirm('같은 카테고리의 파일이 존재 합니다.\n덮어 쓰시겠습니까?');

                if (confirmResult) {

                    var data = new FormData(form);
                    data.append('age', age);
                    data.append('month', month);
                    data.append('type', type);
                    data.append('check', true);

                    $.ajax({
                        url: 'menu/do_upload',
                        type: "post",
                        data: data,
                        processData: false,
                        contentType: false,
                        cache: false,
                        async: false,
                        success: function (data) {
                            console.log(data);

                            $('.popup').children().remove();
                            $('.popup').removeClass('popup');

                            requestType();
                        },
                        error: function (data) {
                            console.log(data);
                        }
                    });

                } else {

                }

            } else {
                $('.popup').children().remove();
                $('.popup').removeClass('popup');

                requestType();
            }


        },
        error: function (data) {
            console.log(data);
        }
    });

});


$(document).on('click', '#selectFile', function () {
    console.log('select');
    $('#file').focus();
});

$(document).on('click', '.note', function () {

    console.log('비고등록');
    console.log($(this).find('li').val());

    clickedIndex = $(this).find('li').val();


    $.ajax({
        url: 'menu/show_note',
        type: "post",
        data: {note: menuData[clickedIndex].menu_note},
        processData: false,
        contentType: false,
        cache: false,
        async: false,
        success: function (data) {

            $('#notePopUp').addClass('popup');
            $('.popup').append(data);
        }
    });


    document.getElementById('inputNote').value = menuData[clickedIndex].menu_note;


})

$(document).on('popupbeforeposition', '#notePopUp', function () {

    console.log('ddgefere')

});

$(document).on('click', '#notePopUp', function () {

});


$(document).on('click', '.salt', function () {


    console.log('염도등록');
    console.log($(this).find('li').val());

    clickedIndex = $(this).find('li').val();

    $.ajax({
        url: 'menu/show_salt',
        type: "post",
        data: {salt: menuData[clickedIndex].menu_salt},
        processData: false,
        contentType: false,
        cache: false,
        async: false,
        success: function (data) {

            $('#saltPopUp').addClass('popup');
            $('.popup').append(data);
        }
    });

    document.getElementById('inputSalt').value = menuData[clickedIndex].menu_salt;

})

$(document).on('click', '#submitSalt', function () {

    console.log('염도등록 요청');

    var salt = $('#inputSalt').val();


    $.post('menu/updateSalt',
        {
            menuDate: menuData[clickedIndex].menu_date,
            type: currentType,
            age: currentAge,
            salt: salt

        },
        function (response, status) {
            console.log(response);
        }
    )
        .done(function () {
            loadMenu();
            if (parseFloat(salt) > 0.4) {
                alert('오늘도 고생하신 조리사님!\n0.40% 이하 저염조리 부탁드립니다');
            }else{
                alert('송파구 어린이의 저염 식습관!\n조리사님 덕분입니다');
            }
        })

        .fail(function (response) {
            // alert(response.responseText);
        });


    $('.popup').children().remove();
    $('.popup').removeClass('popup');
});

$(document).on('click', '#submitNote', function () {

    var note = $('#inputNote').val();


    console.log('비고 등록 요청');
    console.log(menuData[clickedIndex].menu_date);
    console.log(currentType);
    console.log(currentAge);
    console.log(note);

    if (note.length <= 20) {
        $.post('menu/updateNote',
            {
                menuDate: menuData[clickedIndex].menu_date,
                type: currentType,
                age: currentAge,
                note: note

            },
            function (response, status) {
                console.log('결과 받음 : ' + status);
                console.log(response);
            }
        )
            .done(function () {
                loadMenu();
            })

            .fail(function (response) {
                alert(response.responseText);
            });

        console.log('비고등록');
        $('.popup').children().remove();
        $('.popup').removeClass('popup');
    } else {

        alert("20자 내로 입력해 주세요");
    }


});


$(document).on('click', '#downloadMenu', function () {

    $year = selectedDate.getFullYear();
    $month = selectedDate.getMonth() + 1;

    $("#link").attr('href', 'menu/downloadMenu?year=' + $year + "&month=" + $month + "&age=" + currentAge + "&type=" + currentType);

});

/**
 * 한글포함 문자열 길이를 구한다
 */
function getTextLength(str) {
    var len = 0;
    for (var i = 0; i < str.length; i++) {
        if (escape(str.charAt(i)).length == 6) {
            len++;
        }
        len++;
    }
    return len;
}

function getMealsTypeString(type) {

    switch (type) {

        case '0':
            return '일반식';
        case '1':
            return '죽식';
        case '2':
            return '아토피 건강식';
        case '3':
            return '저녁식';
        case '4':
            return '이유식';
    }

}

function commaToBr(str) {
    if (str != null)
        return str.replace(/\n/g, "<br/>");
    else
        return "";

}


function isNumberKey(evt) {

    var charCode = (evt.which) ? evt.which : event.keyCode;

    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))

        return false;


    // Textbox value

    var _value = event.srcElement.value;


    // 소수점(.)이 두번 이상 나오지 못하게

    var _pattern0 = /^\d*[.]\d*$/; // 현재 value값에 소수점(.) 이 있으면 . 입력불가

    if (_pattern0.test(_value)) {

        if (charCode == 46) {

            return false;

        }

    }


    // 1000 이하의 숫자만 입력가능

    var _pattern1 = /^\d{2}$/; // 현재 value값이 3자리 숫자이면 . 만 입력가능

    if (_pattern1.test(_value)) {

        if (charCode != 46) {

            alert("100 이하의 숫자만 입력가능합니다");

            return false;

        }

    }


    // 소수점 둘째자리까지만 입력가능

    var _pattern2 = /^\d*[.]\d{2}$/; // 현재 value값이 소수점 둘째짜리 숫자이면 더이상 입력 불가

    if (_pattern2.test(_value)) {

        alert("소수점 둘째자리까지만 입력가능합니다.");

        return false;

    }


    return true;

}