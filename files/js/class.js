
$('#addClass').on('click', function(){
    $.get("/adminClass/addPopup",  function(data) { //list의 버튼 클릭시 외부 url태그 가져오기
        $('#addPopup').addClass('popup');
        $('.popup').append(data);
    });
});

$(document).on('click', '#submitClass', function(){
    var className =  $('#addClassName').val();
    // var teacherSeq = $('#addClassTeacher').val();

    //학급이름 정규식 필요

    if (className == ''){
        alert('학급명을 입력해주세요.');
        $('#addClassName').focus();
    } /*else if (teacherSeq == 'selected'){
        alert('선생님을 선택해주세요.');
        $('#addClassTeacher').focus();
    }*/ else {
        $.post('adminClass/addClass',
            {
                className : className,
                // teacherSeq : teacherSeq,
            },
            function(response, status){

                if (response == 'success') {
                    alert('새로운 학급이 생성되었습니다.');
                    location.reload();
                } else if(response == 'duplicate') {
                    alert('이미 같은 이름의 학급이 존재합니다.');
                    $('#addClassName').focus();
                }
            }

        );
    }
});

$('.modifyPopup').on('click', function(){
    var index = $(this).attr('value');
    var url = '/adminClass/modifyPopup/' + index;

    $.get(url, function(data) { //list의 버튼 클릭시 외부 url태그 가져오기
        $('#addPopup').addClass('popup');
        $('.popup').append(data);
    });
});

$(document).on('click', '#submitModifyClass', function(){
   var className = $('#addClassName').val();
   var teacherSeq = $('#addClassTeacher').val();
   var classSeq = $('#preschoolClassSeq').val();

   if(className == ''){
       alert('학급명을 입력해주세요.');
       $('#addClassName').focus();
   } else {
       $.post('adminClass/modifyClass',
           {
               classSeq : classSeq,
               className : className,
               teacherSeq : teacherSeq,
           },
           function(response, status){
                console.log(response);
               if (response == 'success') {
                   alert('수정되었습니다.');
                   location.reload();
               } else if(response == 'duplicate') {
                   alert('같은 이름의 학급이 존재합니다.');
                   $('#addClassName').focus();
               } else if(response == 'failed') {
                   alert('학급을 수정할 수 없습니다.');
               }

           })
   }
});

$('input:checkbox[name="headCheckBox"]').on('change', function(){
    $('input:checkbox[name="bodyCheckBox"]').each(function(){
        $(this).prop('checked', !$(this).prop('checked'));
    })
})

$('#delSelectedClass').on('click', function(){
    var array = new Array();

    var num = $('input[name="bodyCheckBox"]:checked').length;

    if(num != 0){
        var delAble = true;
        $('input:checkbox[name="bodyCheckBox"]:checked').each(function(index){
            array.push($(this).val());

            if($(this).parent().parent().children().eq(5).text() != 0){
                delAble = false;
                alert('원생이 있는 학급은 삭제할 수 없습니다.');
                return false;
            }

        });

        if(delAble){
            var confirmDel = confirm('선택한 학급을 삭제하시겠습니까?');

            if(confirmDel){
                $.post('adminClass/deleteClass',
                    {
                        classSeqArr : array
                    },
                    function(response, status){
                        console.log(response);

                        if(response == 'success') {
                            alert('선택한 학급을 삭제하였습니다.');
                            location.reload();
                        } else {
                            alert('학급을 삭제할 수 없습니다.');
                        }
                    });
            }
        }
    } else {
        alert('선택된 학급이 없습니다.')
    }
    
})