$(document).ready(function(){
    $('#summernote').summernote({
        lang: 'ko-KR',
        tabsize: 1,
        height: 180,
        fontNames: ['Nanum Gothic', 'Gothic A1', 'Noto Serif KR', 'NanumMyeongjo', 'Stylish'],
        fontNamesIgnoreCheck: ['Nanum Gothic', 'Gothic A1', 'Noto Serif KR', 'NanumMyeongjo', 'Stylish'],
        fontSizes: ['12', '13', '14', '15', '16', '17', '18'],
        toolbar: [
            ['font', ['bold', 'italic', 'underline']],
            ['para', ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull']],
            ['fontname', ['fontname']],
            ['fontsize', ['fontsize']],
        ],
    });

    $('.note-editable').css('text-align', 'inherit');
})


$(document).on('click', '#submitNotice', function(){

    var userSeq = $('#userSeq').val();
    var userName = $('#userName').val();
    var instType = $('#instType').val();
    var instSeq = $('#instSeq').val();
    var noticeType = $('#noticeType').val();
    var noticeFix = $('input[name="fix"]:checked').length;
    var noticeTitle = $('#noticeTitle').val();
    var noticeContent = $('#summernote').summernote('code');

    var noticeFile = $('#noticeFile')[0].files[0]; //과제 파일
    if(noticeFile != null) {
        var noticeFileSize = $('#noticeFile')[0].files[0].size/1024; //kb 크기
    }

    var length_regex =  /^[\w\Wㄱ-ㅎㅏ-ㅣ가-힣]{1,50}$/;

    var contentCheck = noticeContent.replace(/(<([^>]+)>)/gi, "");
    contentCheck=  contentCheck.replace(/&nbsp;/gi, "");

    if(noticeTitle == ''){
        alert('제목을 입력해 주세요.');
    } else if (length_regex.test(noticeTitle) === false) {
        alert("제목에 쓸 수 있는 최대 글자수는 50글자입니다.");
    } else if($.trim(noticeTitle).length == 0) {
        alert('공백만 입력할 수는 없습니다.');
    } else if(noticeType == 0) {
        alert('분류를 선택해주세요')
    } else if(contentCheck == ''){
        if ($('#summernote').summernote('isEmpty')) {
            alert('공지사항 내용을 입력해주세요.');
        } else {
            alert('공지사항 내용을 입력해 주세요.');
        }
    } else if($.trim(contentCheck).length == 0) {
        alert('공백만 입력할 수는 없습니다.');
    } else if(noticeFile != null && noticeFileSize > 10240){
        alert('10MB 이하의 파일을 업로드해주세요');
    } else {

        var uploadData = new FormData();
        uploadData.append('userSeq', userSeq);
        uploadData.append('userName', userName);
        uploadData.append('instType', instType);
        uploadData.append('instSeq', instSeq);
        uploadData.append('noticeType', noticeType);
        uploadData.append('noticeFix', noticeFix);
        uploadData.append('noticeTitle', noticeTitle);
        uploadData.append('noticeContent', noticeContent);

        if(noticeFile != null){
            uploadData.append('noticeFile', noticeFile);
        }

        $.ajax({
            url: '/notice/registerNotice',
            type: "POST",
            data: uploadData,
            processData: false,
            contentType: false,
            success: function(data){

                console.log(data);

                var response = JSON.parse(data);

                if (response.status == 'success') {
                    alert('공지사항이 등록되었습니다.');
                    location.replace('content/'+response.noticeSeq);
                } else {
                    alert(response.message);
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });

    }
});

$('#backNoticeList').on('click', function(){
    // $(location).attr('href', '/notice');
    window.history.back();
});

$(document).on('change', '#noticeFile', function(){

    $('#fileShow').children().remove();

    if(window.FileReader) {
        var filename = $(this)[0].files[0].name;
    } else {
        var filename = $(this).val().split('/').pop().split('\\').pop();
    }

    var filetag = '<p>'+filename+' &nbsp;&nbsp;&nbsp;&nbsp;<a class="delFile"><i class="fas fa-times"></i></a></p>';

    $('#fileShow').append(filetag);
});

$(document).on('click', '.delFile', function(){
    $('#noticeFile').val('');

    $(this).parent().parent().children().remove();
})

$('#modifyNotice').on('click', function(){
    var noticeSeq = $('#noticeSeq').val();
    var userSeq = $('#userSeq').val();

    var confirmDel = confirm('공지사항을 수정하시겠습니까?');
    //
    if(confirmDel){
        $(location).attr('href', '/notice/modify/'+noticeSeq);
    }
})

$(document).on('click', '#submitModiefiedNotice', function(){
    var noticeSeq = $('#noticeSeq').val();
    var noticeFix = $('input[name="fix"]:checked').length;
    var noticeTitle = $('#noticeTitle').val();
    var noticeContent = $('#summernote').summernote('code');

    var noticeFile = $('#noticeFile')[0].files[0]; //과제 파일
    // console.log(noticeFile);
    if(noticeFile != null) {
        var noticeFileSize = $('#noticeFile')[0].files[0].size/1024; //kb 크기
    }

    var length_regex =  /^[\w\Wㄱ-ㅎㅏ-ㅣ가-힣]{1,50}$/;

    var contentCheck = noticeContent.replace(/(<([^>]+)>)/gi, "");
    contentCheck=  contentCheck.replace(/&nbsp;/gi, "");

    if(noticeTitle == ''){
        alert('제목을 입력해 주세요.');
    } else if (length_regex.test(noticeTitle) === false) {
        alert("제목에 쓸 수 있는 최대 글자수는 50글자입니다.");
    } else if($.trim(noticeTitle).length == 0) {
        alert('공백만 입력할 수는 없습니다.');
    } else if(noticeType == 0) {
        alert('분류를 선택해주세요')
    } else if(contentCheck == ''){
        if ($('#summernote').summernote('isEmpty')) {
            alert('공지사항 내용을 입력해주세요.');
        } else {
            alert('공지사항 내용을 입력해 주세요.');
        }
    } else if($.trim(contentCheck).length == 0) {
        alert('공백만 입력할 수는 없습니다.');
    } else if(noticeFile != null && noticeFileSize > 10240){
        alert('10MB 이하의 파일을 업로드해주세요');
    } else {

        var uploadData = new FormData();
        uploadData.append('noticeSeq', noticeSeq);
        uploadData.append('noticeFix', noticeFix);
        uploadData.append('noticeTitle', noticeTitle);
        uploadData.append('noticeContent', noticeContent);

        if(noticeFile != null){
            uploadData.append('noticeFile', noticeFile);
        }

        $.ajax({
            url: '/notice/modifyNotice',
            type: "POST",
            data: uploadData,
            processData: false,
            contentType: false,
            success: function(data){

                console.log(data);

                var response = JSON.parse(data);

                if (response.status == 'success') {
                    alert('공지사항이 수정되었습니다.');
                    console.log('/notice/content/'+noticeSeq);
                    location.replace('/notice/content/'+noticeSeq);
                    // $(location).attr('href', '/notice/content'+noticeSeq);
                } else {
                    alert(response.message);
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });

    }
});

$('#deleteNotice').on('click', function(){
    var noticeSeq = $('#noticeSeq').val();
    var userSeq = $('#userSeq').val();

    console.log(noticeSeq);
    console.log(userSeq);

    var confirmDel = confirm('공지사항을 삭제하시겠습니까?');
    //
    if(confirmDel){
        $.post('/notice/deleteNotice',
            {
                userSeq : userSeq,
                noticeSeq : noticeSeq,
            },
            function(response, status){

            console.log(response);

                if(response == 'success') {
                    alert('공지사항을 삭제하였습니다.');
                    location.replace('/notice');
                } else if(response == 'failed') {
                    alert('공지사항을 삭제할 수 없습니다.');
                }
            });
    }
});