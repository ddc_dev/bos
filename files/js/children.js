$('#addChild').on('click', function(){
    $.get("/children/addPopup",  function(data) { //list의 버튼 클릭시 외부 url태그 가져오기
        $('#addPopup').addClass('popup');
        $('.popup').append(data);
    });
});


$(document).on('click', '#submitChild', function(){
    var childName =  $('#childName').val();
    var guardianContact = $('#guardianContact').val();
    var preschoolClassSeq = $('#selectClass').val();
    var preschoolClassName = $('#selectClass option:checked').text();
    var preschoolSeq = $('#preschoolSeq').val();
    var childGender = $('#childGender').val();
    var birthdayYear = $('#birthdayYear').val();
    var birthdayMonth = $('#birthdayMonth').val();
    var birthdayDay = $('#birthdayDay').val();

    var regexName = /^[0-9a-zA-Z가-힝]*$/;
    var regexContact =/(01[016789])([1-9]{1}[0-9]{2,3})([0-9]{4})$/;

    if(childName.length == 0) {
        alert('원생 이름을 입력해주세요.');
        $('#childName').focus();
    } else if(guardianContact.length == 0) {
        alert('휴대전화 번호를 입력해주세요.');
        $('#guardianContact').focus();
    } else if(preschoolClassSeq == 'selected'){
        alert('학급을 선택해주세요.')
        $('#selectClass').focus();
    } else if(regexName.test(childName) === false) {
        alert('이름에는 특수문자가 포함될 수 없습니다.');
        $('#childName').focus();
    } else if(regexContact.test(guardianContact) === false) {
        alert('\'-\'을 제외한 숫자만 입력해주세요');
        $('#guardianContact').focus();
    } else if(!(birthdayYear == 0 && birthdayMonth == 0 && birthdayDay == 'selected') && !(birthdayYear != 0 && birthdayMonth != 0 && birthdayDay != 'selected')){
        alert('생년월일을 모두 입력해주세요.');
        console.log(birthdayYear);
        console.log(birthdayMonth);
        console.log(birthdayDay);
    } else {
        $.post('children/registerChild',
            {
                childName : childName,
                guardianContact : guardianContact,
                preschoolClassSeq : preschoolClassSeq,
                preschoolClassName : preschoolClassName,
                preschoolSeq : preschoolSeq,
                childGender : childGender,
                birthdayYear : birthdayYear,
                birthdayMonth : birthdayMonth,
                birthdayDay : birthdayDay,
            },
            function(response, status){

                console.log(response);

                if (response == 'success') {
                    alert('새로운 원생이 등록되었습니다.');
                    location.reload();
                } else if(response == 'failed') {
                    alert('원생을 등록할 수 없습니다.');
                }
            })
    }

});


$(document).on('change', '#birthdayMonth', function(){
    updateNumberOfDays();
})

//function to update the days based on the current values of month and year
function updateNumberOfDays(){
    $('#birthdayDay').html('');
    month = $('#birthdayMonth').val();
    year = $('#birthdayYear').val();
    days = daysInMonth(month, year);

    var dayHtml = '<option value="selected">태어난 일</option>';

    for(i=0; i < days ; i++){
        dayHtml += '<option value="'+i+'">'+(i+1)+'일</option>';
    }
    $('#birthdayDay').append(dayHtml);
}

//helper function
function daysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
}

$('#delSelectedChild').on('click', function(){
    var array = new Array();

    var num = $('input[name="bodyCheckBox"]:checked').length;

    if(num != 0){

        $('input:checkbox[name="bodyCheckBox"]:checked').each(function(index){
            array.push($(this).val());
        });

        var confirmDel = confirm('선택한 원생을 삭제하시겠습니까?');
        //
        if(confirmDel){
            $.post('children/deleteChildren',
                {
                    childSeqArr : array
                },
                function(response, status){
                    console.log(response);

                    if(response == 'success') {
                        alert('선택한 원생을 삭제하였습니다.');
                        location.reload();
                    } else {
                        alert('원생을 삭제할 수 없습니다.');
                    }
                });
        }
    } else {
        alert('선택된 원생이 없습니다.')
    }
});

$('#selectPreschoolClass').on('change', function(){
    var preschoolClassSeq = $(this).val();

    if(preschoolClassSeq == 'all') {
        location.reload();
    } else {
        $.post('children/getChildrenListByClass',
            {
                preschoolClassSeq : preschoolClassSeq
            },
            function(response, status){
                appendList(response);
            })
    }

})


/**
 * =========================================================================================================================================
 */


$(document).on('click', '#submitExcel', function () {


    var form = $('#addChildrenExcelForm')[0];
    var data = new FormData(form);


    $.ajax({
        url: 'children/excelUpLoad',
        type: "post",
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        async: false,
        success: function (data) {

            console.log(data);

            $('.popup').children().remove();
            $('.popup').removeClass('popup');

            window.location.reload();
        },
        error: function (data) {
          console.log(data);
        }
    });

});


$(document).on('click','#downloadChildExcel',function () {

    var childClass = $('#selectPreschoolClass').val();


    $("#downloadChildExcelLink").attr('href','children/createXLS?class='+childClass);


});


$(document).on('click','#addExcel', function(){
    $.get("/children/addPopupExcel",  function(data) { //list의 버튼 클릭시 외부 url태그 가져오기
        $('#addPopupExcel').addClass('popup');
        $('.popup').append(data);
    });
});


function appendList(response){

    var list = JSON.parse(response);

    $('.box').children().remove();

    var headLine = ' <div class="displayFlex marginBottom20">\n' +
        '                <div class="width30 displayFlex alignItemsCenter">\n' +
        '                    <h1 style="font-size: 18px; text-align: left">\n' +
        '                        학생 리스트\n' +
        '                    </h1>\n' +
        '                </div>\n' +
        '            </div>' +
        '            <div class="displayFlex marginBottom20">' +
        '                <!--버튼-->\n' +
        '                <div class="width100 buttonBox">\n' +
        '                    <a id="submitSelectedChild">\n' +
        '                        <div class="left">\n' +
        '                            선택 승인 요청\n' +
        '                        </div>\n' +
        '                    </a>\n' +
        '                    <a id="delSelectedChild">\n' +
        '                        <div class="left">\n' +
        '                            선택 삭제\n' +
        '                        </div>\n' +
        '                    </a>\n' +
        '                    <a>\n' +
        '                        <div class="left">\n' +
        '                            일괄 등록\n' +
        '                        </div>\n' +
        '                    </a>\n' +
        '                    <a id="downloadChildExcelLink">\n' +
        '                        <div id="downloadChildExcel" class="left">\n' +
        '                            엑셀 내보내기\n' +
        '                        </div>\n' +
        '                    </a>\n' +
        '                    <a id="sendSMS">\n' +
        '                        <div  class="left">\n' +
        '                            문자 보내기\n' +
        '                        </div>\n' +
        '                    </a>\n' +
        '                </div>\n' +
        '                <!--//버튼-->\n' +
        '            </div>\n' +
        '<ul>\n' +
        '                <li class="head width5"><input type="checkbox" name="headCheckBox" class="checkBox"/></li>\n' +
        '                <li class="head width10">이름</li>\n' +
        '                <li class="head width10">학급</li>\n' +
        '                <li class="head width40">보호자 이메일</li>\n' +
        '                <li class="head width10">전화번호</li>\n' +
        '                <li class="head width15">디바이스 S/N</li>\n' +
        '                <li class="head width10">접근 권한</li>\n' +
        '            </ul>'

    $('.box').append(headLine);

    var bodyLine = ''
    for(var i = 0; i < list.length; i ++){

        var activated = ''
        if(list[i].activated == 1){
            activated = '<li class="pointColor3 width10">승인 대기 중</li>\n';
        }else if(list[i].activated == 2){
            activated = '<li class="pointColor3 width10">연동 요청 필요</li>\n';
        }else if(list[i].activated == 3){
            activated = '<li class="pointColor3 width10">연동 요청 중</li>\n';
        } else if(list[i].activated == 4){
            activated = '<li class="pointColor1 width10">승인 완료</li>\n';
        }


        var name = list[i].bos_name;
        if (name === null){
            name = list[i].child_name;
        }
        bodyLine += '<ul>\n' +
            '                <li width5><input type="checkbox" value="'+list[i].child_seq+'" name="bodyCheckBox" class="checkBox"/></li>\n' +
            '                <li width10>'+name+'</li>\n' +
            '                <li width10>'+list[i].preschool_class_name+'</li>\n' +
            '                <li width40>'+list[i].user_mail+'</li>\n' +
            '                <li width10>'+list[i].guardian_contact+'</li>\n' +
            '                <li width15></li>\n' +
            activated +
            '            </ul>'
    }
    $('.box').append(bodyLine);
    //
    // var pagination = '<?=$pagination?>';
    // $('.box').append(pagination);
}



$(document).on('click','#sendSMS', function(){
    var array = new Array();

    var num = $('input[name="bodyCheckBox"]:checked').length;

    if(num != 0){

        $('input:checkbox[name="bodyCheckBox"]:checked').each(function(index){
            array.push($(this).val());
        });

        var confirmDel = confirm('선택한 원생에게 안내 문자를 보내시겠습니까?');
        //
        if(confirmDel){
            $.post('children/sendSMS',
                {
                    childSeqArr : array
                },
                function(response, status){
                    console.log(response);

                    if(response == 'success') {
                        alert('안내 문자가 전송되었습니다.');
                        location.reload();
                    } else {
                        alert('안내 문자 전송에 실패 했습니다.');
                    }
                });
        }
    } else {
        alert('선택된 원생이 없습니다.')
    }
});

$(document).on('click','#submitSelectedChild', function(){
    var array = new Array();

    var num = $('input[name="bodyCheckBox"]:checked').length;

    if(num != 0){

        $('input:checkbox[name="bodyCheckBox"]:checked').each(function(index){
            array.push($(this).val());
        });

        var confirmDel = confirm('선택한 원생들을 승인 하시겠습니까?');

        if(confirmDel){
            $.post('children/submitChildren',
                {
                    childSeqArr : array
                },
                function(response, status){
                    console.log(response);

                    if(response == 'success') {
                        alert('승인이 완료 되었습니다.');
                        location.reload();
                    } else {
                        alert('승인에 실패 했습니다.');
                    }
                });
        }

    } else {
        alert('선택된 원생이 없습니다.')
    }
});
