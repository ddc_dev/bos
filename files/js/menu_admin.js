var _today = new Date();
var selectedDate = _today;
var dateType = 'day';

$(function () {



    if (dateType=='day'){
        $('#date').html(selectedDate.getFullYear() + '. ' + (selectedDate.getMonth() + 1+'. '+ selectedDate.getDate()));
    } else if(dateType=='month'){
        selectedDate.setDate(1);
        $('#date').html(selectedDate.getFullYear() + '. ' + (selectedDate.getMonth() + 1));
    }


});


$(document).on('click', '#before', function () {

    console.log('before');
    console.log(selectedDate);

    if (dateType=='day'){
        selectedDate.setDate(selectedDate.getDate() - 1);
        $('#date').html(selectedDate.getFullYear() + '. ' + (selectedDate.getMonth() + 1+'. '+ selectedDate.getDate()));
    } else if(dateType=='month'){
        selectedDate.setMonth(selectedDate.getMonth() - 1);
        $('#date').html(selectedDate.getFullYear() + '. ' + (selectedDate.getMonth() + 1));
    }




});

$(document).on('click', '#after', function () {
    console.log('after');

    console.log(selectedDate);
    if (dateType=='day'){
        selectedDate.setDate(selectedDate.getDate() + 1);
        $('#date').html(selectedDate.getFullYear() + '. ' + (selectedDate.getMonth() + 1+'. '+ selectedDate.getDate()));
    } else if(dateType=='month'){
        selectedDate.setMonth(selectedDate.getMonth() + 1);
        $('#date').html(selectedDate.getFullYear() + '. ' + (selectedDate.getMonth() + 1));
    }




});

$(document).on('change', '#dateType', function () {
    dateType = $('#dateType').val();

    if (dateType=='day'){
        $('#date').html(selectedDate.getFullYear() + '. ' + (selectedDate.getMonth() + 1+'. '+ selectedDate.getDate()));
    } else if(dateType=='month'){
        selectedDate.setDate(1);
        $('#date').html(selectedDate.getFullYear() + '. ' + (selectedDate.getMonth() + 1));
    }

});

$(document).on('click','#downloadAdminExcel', function () {

    var dateString = selectedDate.getFullYear()+'-'+(selectedDate.getMonth()+1)+'-'+selectedDate.getDate();

    $("#excelLink").attr('href','menu/downloadMenuAdmin?type='+dateType+'&date='+dateString);

})
