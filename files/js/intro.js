$(document).ready(function(){

    var instType;
    var instProvince;
    var instProvinceName;
    var instCity;
    var instCityName;
    var instVillage;
    var instVillageName;
    var instName;
    var instContact;


});


$(document).on('click', '#registerInst', function(){
    $.get("/auth/agreement",  function(data) { //list의 버튼 클릭시 외부 url태그 가져오기
        $('#registerPopup').addClass('popup');
        $('.popup').append(data);
    });
});

$(document).on('click', '#agreeAll', function(){

    var num = $('input[name="agreeCheck"]:checked').length;

    if(num != 3) {
        alert('약관에 동의하셔야 서비스를 이용할 수 있습니다.');
    } else {
        $.get("/auth/formInst",  function(data) { //list의 버튼 클릭시 외부 url태그 가져오기
            $('.popup').children().remove();
            $('.popup').append(data);
        });
    }
});

$(document).on('change', '#instType', function(){
    instType = $('#instType').val();

    console.log*('changed');

    if (instType == "office"){
        $('#village').css('display', 'none');
    } else {
        $('#village').css('display', 'block');
    }
});

$(document).on('keyup', '#instContact', function(event) {
    instContact = $('#instContact').val();

    var regexContact = /^[0-9]*$/;
    if(!regexContact.test(instContact)) {
        alert('숫자만 입력해주세요.');
        $('#instContact').val(instContact.replace(/[^0-9]/g,""));
    }
});

$(document).on('change', '#province', function(){
    var city_select = '<option value="select">시/군/구</option>';
    var village_select = '<option value="seelct">읍/면/동</option>';

    var province_code = $(this).val();

    $('#city').children().remove();
    $('#village').children().remove();
    $('#village').append(village_select);

    if (province_code == "3611000000"){
        $('#city').css('display', 'none');
        city_select = '';
    } else {
        $('#city').css('display', 'block');
    }

    $.post('main/city',
        {
            province_code : province_code
        },
        function(response, status){

            var json_response = JSON.parse(response);
            // console.log(json_response);

            for(var number = 0; number < json_response.length; number ++){
                city_select += '<option value="'+json_response[number].city_code+'">'+json_response[number].city_name+'</option>';
            }
            $('#city').append(city_select);

            if(instProvince != null) {
                $('#city').val(instCity).prop('selected', true);
                $('#city').change()
                instProvince = null
            }

            if (province_code == "3611000000"){
                $('#city').change();
            }
        }
    );
});

$(document).on('change', '#city', function(){
    var village_select = '<option value="select">읍/면/동</option>';

    var city_code = $(this).val();

    $('#village').children().remove();

    $.post('main/village',
        {
            city_code : city_code
        },
        function(response, status){
            var json_response = JSON.parse(response);
            // console.log(response);

            for(var number = 0; number < json_response.length; number ++) {
                village_select += '<option value="'+json_response[number].village_code+'">'+json_response[number].village_name+'</option>';
            }
            $('#village').append(village_select);

            if(instCity != null) {
                $('#village').val(instVillage).prop('selected', true);
                $('#village').change()
                instCity = null
                instVillage = null
            }
        })
})

$(document).on('click', '#toStep2', function(){

    instType = $('#instType').val();
    instProvince = $('#province').val();
    instProvinceName = $('#province option:checked').text()
    instCity = $('#city').val();
    instCityName = $('#city option:checked').text()
    instVillage = $('#village').val();
    instVillageName = $('#village option:checked').text();
    instName = $('#instName').val();
    instContact = $('#instContact').val();

    var regexContact = /^\d{2,3}\d{3,4}\d{4}$/;

    if (instName.length < 3) {
        alert('기관명은 최소 3자 이상이어야 합니다.');
    } else if (!regexContact.test(instContact)) {
        alert('전화번호를 확인해주세요.');
    } else if(instType == 'select'){
        alert('기관 유형을 선택해주세요.');
    } else if(instProvince == 'select') {
        alert('시/도를 선택해주세요.');
    } else if(instCity == 'select') {
        alert('시/군/구를 선택해주세요.');
    } else if(instType == 'preschool' && instVillage == 'select') {
        alert('읍/면/동을 선택해주세요.');
    } else if(instName == '') {
        alert('기관명을 입력해주세요.');
    } else {
        $.get("/auth/formAdmin",  function(data) { //list의 버튼 클릭시 외부 url태그 가져오기
            $('.popup').children().remove();
            $('.popup').append(data);
        });
    }
});

$(document).on('click', '#cancelFormInst', function(){
    $('.popup').children().remove();
    $.get("/auth/agreement",  function(data) { //list의 버튼 클릭시 외부 url태그 가져오기
        $('#registerPopup').addClass('popup');
        $('.popup').append(data);
    });
})

$(document).on('click', '#toStep3', function(){

    var adminName = $('#adminName').val();
    var adminMail = $('#adminMail').val();
    var adminPW = $('#adminPW').val();
    var adminPWCheck = $('#adminPWCheck').val();
    var adminContact = $('#adminContact').val();

    var regexName = /^[0-9a-zA-Z가-힝]*$/;
    var regexMail=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    var regexPW = /^.*(?=^.{8,20}$)(?=.*\d)(?=.*[a-zA-Z])(?=.*[!@#$%^&+=]).*$/;
    var regexContact =/(01[016789])([1-9]{1}[0-9]{2,3})([0-9]{4})$/;

    var regex_space = /\s/;

    if(adminName.length == 0) {
        alert('관리자 이름을 입력해주세요.');
        $('#adminName').focus();
    } else if (adminName.length < 3) {
        alert('관리자명은 최소 3자 이상이어야 합니다.');
    } else if(adminMail.length == 0) {
        alert('메일 주소를 입력해주세요.');
        $('#adminMail').focus();
    } else if(adminPW.length == 0) {
        alert('비밀번호를 입력해주세요.');
        $('#adminPW').focus();
    } else if(adminPWCheck.length == 0) {
        alert('비밀번호를 다시 한 번 입력해주세요.');
        $('#adminPWCheck').focus();
    } else if(adminContact.length == 0) {
        alert('휴대전화 번호를 입력해주세요');
        $('#adminContact').focus();
    } else if(regexName.test(adminName) === false) {
        alert('이름에는 특수문자가 포함될 수 없습니다.');
        $('#adminName').focus();
    } else if(regexMail.test(adminMail) === false) {
        alert('메일주소가 형식에 맞지 않습니다.');
        $('#adminMail').focus();
    } else if(regexPW.test(adminPW) === false) {
        alert('비밀번호는 영문, 숫자, 특수문자를 포함한 8 ~ 20자로 입력해주세요.');
        $('#adminPW').focus();
    } else if(regexPW.test(adminPWCheck) === false) {
        alert('비밀번호는 영문, 숫자, 특수문자를 포함한 8 ~ 20자로 입력해주세요.');
        $('#adminPWCheck').focus();
    } else if(regexContact.test(adminContact) === false) {
        alert('\'-\'을 제외한 숫자만 입력해주세요');
        $('#adminContact').focus();
    } else if(adminPW !== adminPWCheck) {
        alert('비밀번호를 다시 한 번 확인해주세요.');
        $('#adminPWCheck').focus();
    } else if(!isContactVertified) {
        alert('휴대폰 인증을 진행해 주세요.');
        $('#adminContact').focus();
    }  else {
        $.post('/auth/register',
            {
                instType : instType,
                instProvince : instProvince,
                instProvinceName : instProvinceName,
                instCity : instCity,
                instCityName : instCityName,
                instVillage : instVillage,
                instVillageName : instVillageName,
                instName : instName,
                instContact : instContact,
                adminName : adminName,
                adminMail : adminMail,
                adminPW : adminPW,
                adminContact : adminContact,
            },
            function(response, status){
                console.log(response);

                var json_response = JSON.parse(response);

                console.log(json_response);
                alert(json_response.msg);

                if(json_response.url){
                    location.replace(json_response.url);
                }
            });

    }

});

$(document).on('click', '#cancelFormAdmin', function(){
    $('.popup').children().remove();
    $.get("/auth/formInst",  function(data) { //list의 버튼 클릭시 외부 url태그 가져오기
        $('.popup').append(data);

        $('#instType').val(instType).prop("selected", true);
        $('#instType').change()
        $('#instName').val(instName);
        $('#instContact').val(instContact);
        $('#province').val(instProvince).prop('selected', true);
        $('#province').change()
    });
})

//이메일이나 비밀번호 input에 포커스 되어있을 경우 엔터키 입력시 로그인 진행
if($('#inputAdminMail').focus() || $('#inputAdminPW').focus()){
    $('#inputAdminMail').keydown(function(key){
        if(key.keyCode == 13){//키가 13이면 실행 (엔터는 13)
            loginProgress()
        }
    });

    $('#inputAdminPW').keydown(function(key){
        if(key.keyCode == 13){//키가 13이면 실행 (엔터는 13)
            loginProgress()
        }
    });
}

$('#signIn').on('click', function(){
    loginProgress()
})

function loginProgress(){
    var adminMail = $('#inputAdminMail').val();
    var adminPW = $('#inputAdminPW').val();

    var regexMail=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    if(adminMail.length == 0) {
        alert('메일 주소를 입력해주세요.');
        $('#inputAdminMail').focus();
    } else if(adminPW.length == 0) {
        alert('비밀번호를 입력해주세요.');
        $('#inputAdminPW').focus();
    } else if(regexMail.test(adminMail) === false) {
        alert('메일주소가 형식에 맞지 않습니다.');
        $('#adminMail').focus();
    } else {
        $.post('/auth/signIn',
            {
                userMail : adminMail,
                userPassword : adminPW,
            },
            function(response, status){
                console.log(response)

                var json_response = JSON.parse(response);

                console.log(json_response);
                if (json_response.statusCode == 200) {
                    $(location).attr('href', '/main');
                    // location.replace('auth/signedIn');
                } else if (json_response.statusCode == 401){
                    alert('이메일 혹은 비밀번호가 올바르지 않습니다.');
                } else if (json_response.statusCode == 404){
                    alert('존재하지 않는 사용자입니다.');
                } else {
                    alert('null');
                }
            });
    }
}



var changedPhoneNumber = null;
var confirmNumber = null;
var isSMSSended = false;
var isContactVertified = false;
var isConfirmNumberExpired = true;


$(document).on('click','#SendSMSForRegister',function () {

    var phoneNumber = $('#adminContact').val();

    changedPhoneNumber = phoneNumber;

    var regexContact =/(01[016789])([1-9]{1}[0-9]{2,3})([0-9]{4})$/;

    if(regexContact.test(phoneNumber) === false) {
        alert('\'-\'을 제외한 숫자만 입력해주세요');
        $('#adminContact').focus();
    }else{
        $.post('auth/confirmContact',
            {
                phoneNumber: phoneNumber
            },
            function (response, status) {
                console.log(response);

                if (response !== 'fail') {
                    isConfirmNumberExpired = false;
                    isSMSSended = true;
                    startTimer();
                    console.log('문자 전송 성공');
                    alert('문자 전송 성공! 인증번호를 입력해 주세요.');
                    confirmNumber = response;
                } else {
                    alert('문자 전송에 실패 했습니다.');
                }
            });

    }

});

$(document).on('click','#confirmContactNumber',function () {

    var confirmNumberInput = $('#adminContactCertificate').val();

    if (!isSMSSended){
        alert('인증번호를 전송해 주세요.');
    } else if(isConfirmNumberExpired){
        alert('시간이 만료 되었습니다.\n다시 인증을 진행해 주세요.');
    } else{
        if (confirmNumberInput === confirmNumber){
            alert('인증에 성공 했습니다!');
            isContactVertified = true;
        }else{
            console.log(confirmNumberInput+'/'+confirmNumber);
            alert('인증에 실패 했습니다. 다시 시도해 주세요.');
        }
    }



});

function startTimer() {

    tid=setInterval('msg_time()',1000); // 타이머 1초간격으로 수행
}

var stDate = new Date().getTime();
var edDate = new Date(stDate+120000); // 종료날짜
var RemainDate = edDate - stDate;

function msg_time() {
    var miniutes = Math.floor((RemainDate % (1000 * 60 * 60)) / (1000*60));
    var seconds = Math.floor((RemainDate % (1000 * 60)) / 1000);

    m = miniutes + ":" + seconds ; // 남은 시간 text형태로 변경

    document.getElementById('confirmTimer').value = m;

    if (RemainDate <= 0) {
        // 시간이 종료 되었으면..
        clearInterval(tid);   // 타이머 해제
        isConfirmNumberExpired = true;
        alert('시간이 만료 되었습니다.');
    }else{
        RemainDate = RemainDate - 1000; // 남은시간 -1초
    }
}