function is_numeric(val) {
    return $.isNumeric(val);
}

var isEmpty = function(value){
    if( value == "" || value == null || value == undefined || ( value != null && typeof value == "object" && !Object.keys(value).length ) )
    {
        return true
    }else{
        return false
    }
};


//게시판 목록 드롭다운 동작
$('#smNotice').on('click', function(){
    if($('#smNoticeSub').css('display') == 'block') {
        $('#smNoticeSub').css('display', 'none');
    } else {
        $('#smNoticeSub').css('display', 'block');
    }
});


$('#smManage').on('click', function(){
    if($('#smManageSub').css('display') == 'block') {
        $('#smManageSub').css('display', 'none');
    } else {
        $('#smManageSub').css('display', 'block');
    }
});

$(document).on('click', '#profile', function(){
    $.get("main/profilePopup",  function(data) { //list의 버튼 클릭시 외부 url태그 가져오기
        $('#profilePopup').addClass('popup');
        $('.popup').append(data);
    });
});

var isPhoneNumberChanged = false;
var changedPhoneNumber = null;
var confirmNumber = null;

$(document).on('click','#changeProfile',function () {

    console.log('회원 정보 변경');

    var password = $('#password').val();
    var passwordConfirm = $('#passwordConfirm').val();

    var regexPW = /^.*(?=^.{8,20}$)(?=.*\d)(?=.*[a-zA-Z])(?=.*[!@#$%^&+=]).*$/;

    var isPasswordChanged = false;

    if (password.length>0){ //비밀번호 변경 시도

        if(regexPW.test(password) === false){
            alert('비밀번호는 특수문자를 포함한 8 ~ 20자로 입력해주세요.');
            $('#password').focus();
        }else if(password !== passwordConfirm) {
            alert('비밀번호를 다시 한 번 확인해주세요.');
            $('#passwordConfirm').focus();
        }else{
            isPasswordChanged = true;
        }

    }

    //비밀번호 혹은 핸드폰 번호 변경이 시도됨
    if (isPasswordChanged === true || isPhoneNumberChanged === true) {

        var password = null;

        if (isPasswordChanged === true) {
            password = $('#password').val();
        }

        $.post('auth/changeProfile',
            {
                password: password,
                phoneNumber: changedPhoneNumber
            },
            function (response, status) {
                console.log(response);

                if (response === 'success') {
                    alert('회원 정보 변경에 성공 했습니다.');
                    location.reload();
                } else {
                    alert('회원 정보 변경에 실패 했습니다.');
                }
            });

    }else{
        alert('변경할 내용이 없습니다.');
    }

});

$(document).on('click','#sendChangePhoneNumberSMS',function () {

    var phoneNumber = $('#changePhoneNumber').val();

    changedPhoneNumber = phoneNumber;

    var regexContact =/(01[016789])([1-9]{1}[0-9]{2,3})([0-9]{4})$/;

    if(regexContact.test(phoneNumber) === false) {
        alert('\'-\'을 제외한 숫자만 입력해주세요');
        $('#changePhoneNumber').focus();
    }else{
        $.post('auth/confirmContact',
            {
                phoneNumber: phoneNumber
            },
            function (response, status) {
                console.log(response);

                if (response !== 'fail') {
                    console.log('문자 전송 성공');
                    alert('문자 전송 성공! 인증번호를 입력해 주세요.');
                    confirmNumber = response;
                } else {
                    alert('문자 전송에 실패 했습니다.');
                }
            });

    }

});

$(document).on('click','#checkConfirmNumber',function () {

   var confirmNumberInput = $('#confirmNumber').val();

   if (confirmNumberInput === confirmNumber){
       alert('인증에 성공 했습니다! 하단의 변경 버튼을 꼭 눌러 주세요.');
       isPhoneNumberChanged = true;
   }else{
       console.log(confirmNumberInput+'/'+confirmNumber);
       alert('인증에 실패 했습니다. 다시 시도해 주세요.');
   }

});