$('#addTeacher').on('click', function(){
    $.get("/teacher/addPopup",  function(data) { //list의 버튼 클릭시 외부 url태그 가져오기
        $('#addPopup').addClass('popup');
        $('.popup').append(data);
    });
});

$('.modifyTeacherPopup').on('click', function(){
    var index = $(this).attr('value');

    if (index == "") {
        index = 0;
    }

    var url = '/teacher/modifyPopup/' + index;
    console.log(url);

    $.get(url,  function(data) { //list의 버튼 클릭시 외부 url태그 가져오기
        $('#addPopup').addClass('popup');
        $('.popup').append(data);
    });
});

$(document).on('click', '#submitModifyTeacher', function(){
    var teacherSeq = $('#teacherSeq').val();
    var preschoolClassSeq = $('#classList').val();
    var preschoolClassName = $('#classList option:checked').text()

    if(preschoolClassSeq == 'selected'){
        alert('학급을 선택해주세요.')
    } else {
        $.post('teacher/setClass',
            {
                teacherSeq : teacherSeq,
                preschoolClassSeq : preschoolClassSeq,
                preschoolClassName : preschoolClassName,
            },
            function(response, status){
                if (response == 200) {
                    alert('담당 학급이 변경되었습니다.');
                    location.reload();
                } else {
                    alert('담당 학급을 변경할 수 없습니다.');
                }
            })
    }
})


$('#delSelectedTeacher').on('click', function(){
    var array = new Array();

    var num = $('input[name="bodyCheckBox"]:checked').length;

    if(num != 0){
        var delAble = true;
        $('input:checkbox[name="bodyCheckBox"]:checked').each(function(index){
            array.push($(this).val());

            if($(this).parent().parent().children().eq(2).text() != 0){
                delAble = false;
                alert('담당 학급이 있는 선생님은 삭제할 수 없습니다.');
                return false;
            }

        });

        if(delAble){
            var confirmDel = confirm('선택한 선생님을 삭제하시겠습니까?');
        //
            if(confirmDel){
                $.post('teacher/deleteTeacher',
                    {
                        teacherSeqArr : array
                    },
                    function(response, status){
                        console.log(response);

                        if(response == 'success') {
                            alert('선택한 선생님을 삭제하였습니다.');
                            location.reload();
                        } else {
                            alert('선생님을 삭제할 수 없습니다.');
                        }
                    });
            }
        }
    } else {
        alert('선택된 선생님이 없습니다.')
    }
    
});

$('#approveTeacher').on('click', function(){
    var array = new Array();

    var num = $('input[name="bodyCheckBox"]:checked').length;

    if(num != 0){

        $('input:checkbox[name="bodyCheckBox"]:checked').each(function(index){
            array.push($(this).val());
        });

        var confirmDel = confirm('선택한 선생님을 승인하시겠습니까?');
            //
        if(confirmDel){
            $.post('teacher/approveTeacher',
                {
                    teacherSeqArr : array
                },
                function(response, status){
                console.log(response);

                if(response == 'success') {
                    alert('선택한 선생님을 승인하였습니다.');
                    location.reload();
                } else {
                    alert('선생님을 승인할 수 없습니다.');
                }
            });
        }
    } else {
        alert('선택된 선생님이 없습니다.')
    }

});

$(document).on('click', '#submitTeacher', function () {
    var teacherName = $('#teacherName').val();
    var teacherMail = $('#teacherMail').val();
    var teacherBD = $('#teacherBD').val();
    var teacherContact = $('#teacherContact').val();
    var preschoolSeq = $('#preschoolSeq').val();
    var preschoolName = $('#preschoolName').val();

    var regexName = /^[0-9a-zA-Z가-힝]*$/;
    var regexMail= /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    var regexBD =  /^([0-9][0-9]|20\d{2})(0[0-9]|1[0-2])(0[1-9]|[1-2][0-9]|3[0-1])$/;
    var regexContact =/(01[016789])([1-9]{1}[0-9]{2,3})([0-9]{4})$/;

    if(teacherName.length == 0) {
        alert('선생님 이름을 입력해주세요.');
        $('#teacherName').focus();
    } else if(teacherMail.length == 0) {
        alert('메일 주소를 입력해주세요.');
        $('#teacherMail').focus();
    } else if(teacherBD.length == 0) {
        alert('생년월일을 입력해주세요.');
        $('#teacherBD').focus();
    } else if(teacherContact.length == 0) {
        alert('휴대전화 번호를 입력해주세요');
        $('#teacherContact').focus();
    } else if(regexName.test(teacherName) === false) {
        alert('이름에는 특수문자가 포함될 수 없습니다.');
        $('#teacherName').focus();
    } else if(regexMail.test(teacherMail) === false) {
        alert('메일주소가 형식에 맞지 않습니다.');
        $('#teacherMail').focus();
    } else if(regexBD.test(teacherBD) === false) {
        alert('생년월일을 형식에 맞게 입력해주세요.');
        $('#teacherBD').focus();
    } else if(regexContact.test(teacherContact) === false) {
        alert('\'-\'을 제외한 숫자만 입력해주세요');
        $('#teacherContact').focus();
    } else {
        $.post('teacher/registerTeacher',
            {
                teacherName : teacherName,
                teacherMail : teacherMail,
                teacherBD : teacherBD,
                teacherContact : teacherContact,
                preschoolSeq : preschoolSeq,
                preschoolName : preschoolName,
            },
            function(response, status){
                if (response == 'success') {
                    alert('새로운 선생님이 등록되었습니다.');
                    location.reload();
                } else if(response == 'failed') {
                    alert('선생님을 등록할 수 없습니다.');
                } else if(response == 'duplicate') {
                    alert('이미 등록된 계정입니다.');
                }
            })
    }
})




$(document).on('click', '#submitTeacherExcel', function () {

    var preschoolClassSeq = $('#selectClassExcel').val();

    var form = $('#addTeacherExcelForm')[0];
    var data = new FormData(form);
    data.append('preschoolClassSeq', preschoolClassSeq);


    $.ajax({
        url: 'teacher/excelUpLoad',
        type: "post",
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        async: false,
        success: function (data) {

            console.log(data);

            $('.popup').children().remove();
            $('.popup').removeClass('popup');


            window.location.reload();
        },
        error: function (data) {
            console.log(data);
        }
    });

});



$('#addTeacherExcel').on('click', function(){
    $.get("/teacher/addPopupExcel",  function(data) { //list의 버튼 클릭시 외부 url태그 가져오기
        $('#addPopupExcel').addClass('popup');
        $('.popup').append(data);
    });
});


$('#sendTeacherSMS').on('click', function(){
    var array = new Array();

    var num = $('input[name="bodyCheckBox"]:checked').length;

    if(num != 0){

        $('input:checkbox[name="bodyCheckBox"]:checked').each(function(index){
            array.push($(this).val());
        });

        var confirmDel = confirm('선택한 선생님에게 안내 문자를 보내시겠습니까?');
        //
        if(confirmDel){
            $.post('teacher/sendSMS',
                {
                    teacherSeqArr : array
                },
                function(response, status){
                    console.log(response);

                    if(response == 'success') {
                        alert('안내 문자가 전송되었습니다.');
                        location.reload();
                    } else {
                        alert('안내 문자 전송에 실패 했습니다.');
                    }
                });
        }
    } else {
        alert('선택된 선생님이 없습니다.')
    }
});

