$('#addAdmin').on('click', function(){
    $.get("/institution/addPopup",  function(data) { //list의 버튼 클릭시 외부 url태그 가져오기
        $('#addPopup').addClass('popup');
        $('.popup').append(data);
    });
});

$(document).on('click', '#submitAdmin', function(){
    var teacherSeq = $('#addAdminTeacher').val();

    //학급이름 정규식 필요

    if (teacherSeq == 'selected'){
        alert('선생님을 선택해주세요.');
        $('#addAdminTeacher').focus();
    } else {
        $.post('institution/addSubAdmin',
            {
                teacherSeq : teacherSeq,
            },
            function(response, status){

                console.log(response);
                if (response == 'success') {
                    alert('부관리자 등록이 완료되었습니다.');
                    location.reload();
                } else if(response == 'failed') {
                    alert('부관리자로 등록할 수 없습니다.');
                    $('#addAdminTeacher').focus();
                }
            }

        );
    }
});

$('#delSelectedAdmin').on('click', function(){
    var array = new Array();

    var num = $('input[name="bodyCheckBox"]:checked').length;

    if(num != 0){
        $('input:checkbox[name="bodyCheckBox"]:checked').each(function(index){
            array.push($(this).val());

        });


        var confirmDel = confirm('선택한 관리자를 삭제하시겠습니까?');

        if(confirmDel){
            $.post('institution/deleteAdmin',
                {
                    userSeqArr : array
                },
                function(response, status){
                console.log(response);

                if(response == 'success') {
                    alert('선택한 관리자를 삭제하였습니다.');
                    location.reload();
                } else {
                    alert('관리자를 삭제할 수 없습니다.');
                }
            });
        }

    } else {
        alert('선택된 관리자가 없습니다.')
    }

})