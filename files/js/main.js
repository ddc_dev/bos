$(document).ready(function(){
    $('#rangeType').change();
    var _today = new Date();
    setPickerDate(_today);
})

$('#sort').on('click', function(){
    $.get("/main/sortPopup",  function(data) { //list의 버튼 클릭시 외부 url태그 가져오기
        $('#sortPopup').addClass('popup');
        $('.popup').append(data);
    });
});

$('#rangeType').on('change', function(){

    var currentDate = new Date($('#dailyPicker').val());
    setPickerDate(currentDate)

    if ($(this).val() == 'daily') {

        $('#dailyPicker').datepicker({
            showMonthAfterYear:true //년도 먼저 나오고, 뒤에 월 표시
            ,changeYear: true //콤보박스에서 년 선택 가능
            ,changeMonth: true //콤보박스에서 월 선택 가능
            ,yearSuffix: "년" //달력의 년도 부분 뒤에 붙는 텍스트
            ,monthNamesShort: ['1','2','3','4','5','6','7','8','9','10','11','12'] //달력의 월 부분 텍스트
            ,monthNames: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'] //달력의 월 부분 Tooltip 텍스트
            ,dayNamesMin: ['일','월','화','수','목','금','토'] //달력의 요일 부분 텍스트
            ,dayNames: ['일요일','월요일','화요일','수요일','목요일','금요일','토요일'] //달력의 요일 부분 Tooltip 텍스트
            ,onSelect: function(dateText){
                var dailyPick = stringToDate(dateText);
                setPickerDate(dailyPick);
                // $('#datepicker').html(formattingDate(test));
                // $('#datepickerInput').val(test);
            }
        });

    } else if($(this).val() == 'weekly') {

        $('#weeklyPicker').datepicker({
            showMonthAfterYear:true //년도 먼저 나오고, 뒤에 월 표시
            ,changeYear: true //콤보박스에서 년 선택 가능
            ,changeMonth: true //콤보박스에서 월 선택 가능
            ,yearSuffix: "년" //달력의 년도 부분 뒤에 붙는 텍스트
            ,monthNamesShort: ['1','2','3','4','5','6','7','8','9','10','11','12'] //달력의 월 부분 텍스트
            ,monthNames: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'] //달력의 월 부분 Tooltip 텍스트
            ,dayNamesMin: ['일','월','화','수','목','금','토'] //달력의 요일 부분 텍스트
            ,dayNames: ['일요일','월요일','화요일','수요일','목요일','금요일','토요일'] //달력의 요일 부분 Tooltip 텍스트
            ,onSelect: function(dateText){
                var weeklyPick = stringToDate(dateText);
                setPickerDate(weeklyPick);
            }
        });

        convertToWeekPicker($('#weeklyPicker'));
    } else {

        $('#monthlyPicker').MonthPicker({
            Button: false

            ,OnAfterChooseMonth: function(dateText){
                var monthlyPick = stringToDate(dateText);
                setPickerDate(monthlyPick);
            }
        });
    }
})

$('#datepicker').on('click', function(){

    if($('#rangeType').val() == 'daily'){
        $('#dailyPicker').datepicker('show');
    } else if($('#rangeType').val() == 'weekly'){
        $('#weeklyPicker').datepicker('show');
    } else {
        $('#monthlyPicker').click();
    }
});


function stringToDate(string){
    var result = new Date(string);
    return result;
}

function formattingDate(date){

    var result;

    if($('#rangeType').val() == 'daily') {
        result = date.getFullYear() + '. ' + (date.getMonth()+1) + '. ' + date.getDate();
    } else if($('#rangeType').val() == 'weekly'){
        // var date = $(this).datepicker('getDate');
        var startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay());
        var endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 6);
        result = startDate.getFullYear() + '. ' + (startDate.getMonth()+1) + '. ' + startDate.getDate() + ' ~ ' + (endDate.getMonth()+1) + '. ' + endDate.getDate();
    } else {
        result = date.getFullYear() + '. ' + (date.getMonth()+1);
    }

    return result;
}

function setPickerDate(date){
    $('#datepicker').html(formattingDate(date));
    $('#dailyPicker').val(date);
    $('#weeklyPicker').val(date);
    $('#monthlyPicker').val(date);
}

$(document).on('click', '#sortAge li', function(){
    $(this).addClass('selected').siblings().removeClass('selected');
});

$(document).on('click', '#sortGender li', function(){
    $(this).addClass('selected').siblings().removeClass('selected');
});

$(document).on('click', '#sortBMI li', function(){
    $(this).addClass('selected').siblings().removeClass('selected');
});

$(document).on('click', '.cancelPopup', function(){
    $('.popup').children().remove();
    $('.popup').removeClass('popup');
});

$(document).on('click', '#popupSort', function(){
    console.log($('#sortAge').find('.selected').attr('value'));
    console.log($('#sortGender').find('.selected').attr('value'));
    console.log($('#sortBMI').find('.selected').attr('value'));
});

$('#dateBefore').click(function(){
    console.log('before');
    var currentDate = new Date($('#dailyPicker').val());

    var rangeType = $('#rangeType').val()

    if (rangeType == 'daily'){
        var newDate = currentDate.setDate(currentDate.getDate() - 1, 1);
        var weeklyPick = stringToDate(newDate);
        setPickerDate(weeklyPick);
    } else if(rangeType == 'weekly'){
        var newDate = currentDate.setDate(currentDate.getDate() - 7, 1);
        var weeklyPick = stringToDate(newDate);
        setPickerDate(weeklyPick);
    } else {
        var newDate = currentDate.setMonth(currentDate.getMonth() - 1, 1);
        var weeklyPick = stringToDate(newDate);
        setPickerDate(weeklyPick);
    }

})

$('#dateAfter').click(function(){
    console.log('after');
    var currentDate = new Date($('#dailyPicker').val());

    var rangeType = $('#rangeType').val()

    console.log(rangeType);

    if (rangeType == 'daily'){
        var newDate = currentDate.setDate(currentDate.getDate() + 1, 1);
        var weeklyPick = stringToDate(newDate);
        setPickerDate(weeklyPick);
    } else if(rangeType == 'weekly'){
        var newDate = currentDate.setDate(currentDate.getDate() + 7, 1);
        var weeklyPick = stringToDate(newDate);
        setPickerDate(weeklyPick);
    } else {
        var newDate = currentDate.setMonth(currentDate.getMonth() + 1, 1);
        var weeklyPick = stringToDate(newDate);
        setPickerDate(weeklyPick);
    }
});

