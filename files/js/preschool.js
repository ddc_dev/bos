$('#preschoolVillage').on('change', function(){
    var villageCode = $(this).val();

    $.post('preschool/getPreschoolListByVillage',
        {
            villageCode : villageCode
        },
        function(response, status){
            console.log(response);

            var list = JSON.parse(response);

            $('.box').children().remove();

            var headLine = '<ul>\n' +
                '                <li class="head">유치원 이름</li>\n' +
                '                <li class="head">학급 수</li>\n' +
                '                <li class="head">선생님 수</li>\n' +
                '                <li class="head">원생 수</li>\n' +
                '                <li class="head">기기 수</li>\n' +
                '                <li class="head">등록일</li>\n' +
                '                <li class="head">관리자</li>\n' +
                '            </ul>'

            $('.box').append(headLine);

            var bodyLine = ''
            for(var i = 0; i < list.length; i ++){
                bodyLine += '<ul>\n' +
                    '                    <li>'+list[i].preschool_name+'</li>\n' +
                    '                    <li>'+list[i].class_count+'</li>\n' +
                    '                    <li>'+list[i].teacher_count+'</li>\n' +
                    '                    <li></li>\n' +
                    '                    <li></li>\n' +
                    '                    <li></li>\n' +
                    '                    <li>'+list[i].user_mail+'</li>\n' +
                    '                </ul>'
            }
            $('.box').append(bodyLine);

            var pagination = '<div class="pagination">\n' +
                '                <a href="#">이전페이지</a>&nbsp;&nbsp;&nbsp;1 . 2 . 3 . 4 ... 12&nbsp;&nbsp;&nbsp;<a href="#">다음페이지</a>\n' +
                '            </div>'
            $('.box').append(pagination);
        });
})