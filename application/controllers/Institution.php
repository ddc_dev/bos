<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Institution extends Base_Controller {
    function __construct()
    {
        parent::__construct();

        $this->load->model('Teacher_Model');
        $this->load->model('Admin_Model');
        $this->lang->load(array('common_lang'), isset($_SESSION['language']) ? $_SESSION['language'] : 'en');
    }

    function index(){
        $this->_head();
        $this->_side();
        $data['adminList'] = $this->Admin_Model->getAdminList($_SESSION['instType'], $_SESSION['instSeq']);
        $this->load->view('manage/institution', $data);
        $this->_bottom();
    }

    function addPopup(){
        $preschoolSeq = $_SESSION['preschoolSeq'];
        $data['teacherList'] = $this->Teacher_Model->getNotAdminTeacherList($preschoolSeq);
        $this->load->view('/manage/institution_add_popup', $data);
    }

    function addSubAdmin(){
        $teacherInfo = $this->input->post();

        $result = $this->Admin_Model->addSubAdmin($teacherInfo);

        echo $result;

    }
    
    function deleteAdmin(){
        $delArr = $this->input->post();

        $result = $this->Admin_Model->deleteAdmin($delArr);

        echo $result;
    }

}