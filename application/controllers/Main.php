<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Main extends Base_Controller
{
    function __construct()
    {
        parent::__construct();
        
        $this->load->database();
        $this->load->model('District_Model');
        $this->load->model('Notice_Model');
        $params = array('api_key' => 'UP-1558938888-2966', 'password' => 'ka42292');
        $this->load->library('message', $params);
        $this->lang->load(array('common_lang'), isset($_SESSION['language']) ? $_SESSION['language'] : 'en');
    }

    function index() {
        $this->_head();
        $this->_side();
//        $data['province'] = $this->District_Model->getProvince();
        $data['noticeList'] = $this->Notice_Model->getNoticeList('', 0, 0, 3);
        $this->load->view('main', $data);
        $this->_bottom();
    }

    function province(){
        $data = $this->District_Model->getProvince();
        return $data;
    }

    function city(){
        $code = $this->input->post('province_code', TRUE);
        $data = $this->District_Model->getCity($code);

        echo json_encode($data);
    }

    function village(){
        $code = $this->input->post('city_code', TRUE);
        $data = $this->District_Model->getVillage($code);

        echo json_encode($data);
    }

    function test(){
        $this->load->view('test');
    }

    function sortPopup(){
        $this->load->view('sort_popup');
    }

    function profilePopup(){
        $this->load->view('common/profilePopup');
    }

    function center(){
        $this->_head();

        $this->load->view('centerinfo');
        $this->_bottom();
    }

}