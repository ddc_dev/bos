<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Cert extends Base_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->lang->load(array('common_lang'), isset($_SESSION['language']) ? $_SESSION['language'] : 'en');
    }

    function index()
    {

        $this->load->view('cert');


    }

}