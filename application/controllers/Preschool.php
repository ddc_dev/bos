<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Preschool extends Base_Controller {
    function __construct()
    {
        parent::__construct();

        $this->load->model('Preschool_Model');
        $this->load->model('District_Model');
        $this->lang->load(array('common_lang'), isset($_SESSION['language']) ? $_SESSION['language'] : 'en');
    }

    function index(){
        $this->_head();
        $this->_side();
        $city = $_SESSION['officeCity'];
        $data['preschoolList'] = $this->Preschool_Model->getPreschoolList($city);
        $data['villageList'] = $this->District_Model->getVillage($_SESSION['officeCity']);
        $this->load->view('manage/preschool', $data);
        $this->_bottom();
    }

    function getPreschoolListByVillage(){
        $villageCode = $this->input->post();

        $result = $this->Preschool_Model->getPreschoolListByVillage($villageCode);

        echo json_encode($result);

    }
    
}