<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notice extends Base_Controller {
    function __construct() {
        parent::__construct();

        $this->load->database();
        $this->load->model('Notice_Model');
        $this->load->helper(array('form', 'url'));
        $this->load->helper('download');
        $this->load->library('pagination');
        $this->lang->load(array('common_lang'), isset($_SESSION['language']) ? $_SESSION['language'] : 'en');
    }


    function index() {
        $this->_head();
        $this->_side();
        $this->lists(0);
        $this->_bottom();
    }

//    function _remap($method) {
//        var_dump($method);
//
//        if (method_exists($this, $method)) {
//
//            if($method == 'lists'|| $method == 'content'|| count($this->uri->segments) < 2){
//                $this->_head();
//                $this->_side();
//                $this -> {"{$method}"}();
//                $this->_bottom();
//            } else {
//                $this -> {"{$method}"}();
//            }
//
//        }
//
//    }

    function all(){
        $this->_head();
        $this->_side();
        $this->lists(0);
        $this->_bottom();
    }

    function general(){
        $this->_head();
        $this->_side();
        $this->lists(1);
        $this->_bottom();
    }

    function monthlyMenu(){
        $this->_head();
        $this->_side();
        $this->lists(2);
        $this->_bottom();
    }

    function recipe(){
        $this->_head();
        $this->_side();
        $this->lists(3);
        $this->_bottom();
    }

    function telegram(){
        $this->_head();
        $this->_side();
        $this->lists(4);
        $this->_bottom();
    }

    function lists($type){
        // 페이지 네이션 설정

        $typeText = '';
        switch ($type) {
            case 0:
                $typeText = 'all';
                break;
            case 1:
                $typeText = 'general';
                break;
            case 2:
                $typeText = 'monthlyMenu';
                break;
            case 3:
                $typeText = 'recipe';
                break;
            case 4:
                $typeText = 'telegram';
                break;
        }

        $config['base_url'] = '/notice/'.$typeText;
        // 페이징 주소
        $config['total_rows'] = $this->Notice_Model->getNoticeList('count', $type, null, null);
        // 게시물 전체 개수
        $config['per_page'] = 10;
        // 한 페이지에 표시할 게시물 수
        $config['uri_segment'] = 3;
        // 페이지 번호가 위치한 세그먼트

        $config['full_tag_open'] = '<div class="pagination">';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['prev_link'] = '이전 페이지';
        $config['prev_tag_open'] = '&nbsp;&nbsp;&nbsp;';
        $config['prev_tag_close'] = '&nbsp;&nbsp;&nbsp;';
        $config['next_link'] = '다음 페이지';
        $config['next_tag_open'] = '&nbsp;&nbsp;&nbsp;';
        $config['next_tag_close'] = '&nbsp;&nbsp;&nbsp;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;';
        $config['cur_tag_close'] = '&nbsp;&nbsp;&nbsp;';
        $config['num_tag_open'] = '&nbsp;&nbsp;&nbsp;';
        $config['num_tag_close'] = '&nbsp;&nbsp;&nbsp;';
        $config['full_tag_close'] = '</div>';

        // 페이지네이션 초기화
        $this->pagination->initialize($config);
        // 페이지 링크를 생성하여 view에서 사용하 변수에 할당
        $data['pagination'] = $this -> pagination -> create_links();


        // 게시물 목록을 불러오기 위한 offset, limit 값 가져오기
        $page = $this->uri->segment(3, 1);


        if ($page > 1) {
            $start = (($page / $config['per_page'])) * $config['per_page'];
        } else {
            $start = ($page - 1) * $config['per_page'];
        }

        $limit = $config['per_page'];

        $data['totalRow'] = $config['total_rows'];
        $data['noticeList'] = $this->Notice_Model->getNoticeList('', $type, $start, $limit);
        $data['fixedList'] = $this->Notice_Model->getFixedNoticeList($type);
        $this->load->view('notice/notice_list', $data);
    }

    function registerNotice(){
        $noticeInfo = $this->input->post();

        if(isset($_FILES['noticeFile'])){

            $date = date('Y-m-d');
            $timestamp = time();

            if(!file_exists('./uploads/notice/'.$date)){
                $old = umask(0);
                mkdir('./uploads/notice/'.$date, 0777);
                umask($old);
            }

            $ext = explode('.', $_FILES['noticeFile']['name']); // 파일 확장자

            $filename = $_SESSION['userSeq'].'_'.$timestamp.'.'.array_pop($ext);
            
            $config['upload_path'] = './uploads/notice/'.$date;
            $config['allowed_types'] = 'zip|xls|xlsx|gif|jpg|png';
            $config['file_name'] = $filename;
            $config['max_size']	= '2048';
            $config['encrypt_name'] = false;

            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('noticeFile'))
            {
                $error = array('error' => $this->upload->display_errors());
//                var_dump($error);
                $data['status'] = 'failed';
                $data['message'] = '업로드할 수 있는 형식의 확장자가 아닙니다.';
                echo json_encode($data);
            }
            else
            {
                $fileInfo = array('upload_data' => $this->upload->data());
                $result = $this->Notice_Model->registerNotice($noticeInfo, $fileInfo);
                echo json_encode($result);
                
            }
        } else {
            $fileInfo = array();
            $result = $this->Notice_Model->registerNotice($noticeInfo, $fileInfo);
            echo json_encode($result);
        }

    }

    function modifyNotice(){
        $noticeInfo = $this->input->post();

        if(isset($_FILES['noticeFile'])){

            $date = date('Y-m-d');
            $timestamp = time();

            if(!file_exists('./uploads/notice/'.$date)){
                $old = umask(0);
                mkdir('./uploads/notice/'.$date, 0777);
                umask($old);
            }

            $ext = explode('.', $_FILES['noticeFile']['name']); // 파일 확장자

            $filename = $_SESSION['userSeq'].'_'.$timestamp.'.'.array_pop($ext);

            $config['upload_path'] = './uploads/notice/'.$date;
            $config['allowed_types'] = 'zip|xls|xlsx|gif|jpg|png';
            $config['file_name'] = $filename;
            $config['max_size']	= '2048';
            $config['encrypt_name'] = false;

            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('noticeFile'))
            {
                $error = array('error' => $this->upload->display_errors());
//                var_dump($error);
                $data['status'] = 'failed';
                $data['message'] = '업로드할 수 있는 형식의 확장자가 아닙니다.';
                echo json_encode($data);
            }
            else
            {
                $fileInfo = array('upload_data' => $this->upload->data());
                $result = $this->Notice_Model->modifyNotice($noticeInfo, $fileInfo);

                echo json_encode($result);

            }
        } else {
            $fileInfo = array();
            $result = $this->Notice_Model->modifyNotice($noticeInfo, $fileInfo);

            echo json_encode($result);
        }

    }
    
    function deleteNotice(){
        $noticeInfo = $this->input->post();

//        var_dump($noticeInfo);

        $result = $this->Notice_Model->deleteNotice($noticeInfo);

        echo $result;
    }

    function content(){
        $param = $this->uri->segments[3];
        $data['noticeContent'] = $this->Notice_Model->getNoticeDetail($param);
        $this->_head();
        $this->_side();
        $this->load->view('notice/notice_detail', $data);
        $this->_bottom();
    }

    function download(){

        $path = $this->uri->segments[3];
        $fileName= $this->uri->segments[4];
        $originName = $this->uri->segments[5];

        $file = file_get_contents('uploads/notice/'.$path.'/'.$fileName);

//        var_dump($file);
        force_download($originName, $file);
        
    }

    function register(){
        $this->_head();
        $this->_side();
        $this->load->view('notice/notice_edit');
        $this->_bottom();
    }

    function modify(){
        $param = $this->uri->segments[3];
        $data['noticeContent'] = $this->Notice_Model->getNoticeDetail($param);
        $this->_head();
        $this->_side();
        $this->load->view('notice/notice_modify', $data);
        $this->_bottom();
    }
}