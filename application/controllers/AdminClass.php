<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class AdminClass extends Base_Controller {
    function __construct()
    {
        parent::__construct();

        $this->load->model('Class_Model');
        $this->load->model('Teacher_Model');
        $this->lang->load(array('common_lang'), isset($_SESSION['language']) ? $_SESSION['language'] : 'en');
    }

    function index()
    {
        $this->_head();
        $this->_side();
        $preschoolSeq = $_SESSION['preschoolSeq'];
        $data['classList'] = $this->Class_Model->getClassList($preschoolSeq);
        $data['teacherList'] = $this->Teacher_Model->getClassTeacherList($preschoolSeq);
        $this->load->view('manage/class', $data);
        $this->_bottom();
    }

    function addPopup(){
        $preschoolSeq = $_SESSION['preschoolSeq'];
//        $data['teacherList'] = $this->Teacher_Model->getTeacherList($preschoolSeq);
        $this->load->view('/manage/class_add_popup');
    }
    
    function teacherList(){
        $preschoolSeq = $_SESSION['preschoolSeq'];
        $data = $this->Teacher_Model->getTeacherList($preschoolSeq);

        return $data;
    }
    
    function addClass(){
        $classInfo = $this->input->post();

        $result = $this->Class_Model->registerClass($classInfo, $_SESSION['preschoolSeq']);
        
        if ($result == 200) {
            echo 'success';
        } else if($result == 401){
            echo 'duplicate';
        }

    }

    function modifyPopup(){

        $preschoolSeq = $_SESSION['preschoolSeq'];
        $data['classList'] = $this->Class_Model->getClassList($preschoolSeq);
        $data['classListIndex'] = $this->uri->segments[3];
        $data['teacherList'] = $this->Teacher_Model->getNoClassTeacherList($preschoolSeq);
        $this->load->view('/manage/class_modify_popup', $data);

    }
    
    function modifyClass(){
        $classInfo = $this->input->post();

        $result = $this->Class_Model->updateClass($classInfo, $_SESSION['preschoolSeq']);

        if($result == 200) {
            echo 'success';
        } else if($result == 401) {
            echo 'duplicate';
        } else if($result == 404) {
            echo 'failed';
        }
    }

    function deleteClass(){
        $delArr = $this->input->post();

        $result = $this->Class_Model->deleteClass($delArr);
        
        echo $result;
    }
    
}