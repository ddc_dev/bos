<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Teacher extends Base_Controller {
    function __construct()
    {
        parent::__construct();

        $this->load->model('Class_Model');
        $this->load->model('Teacher_Model');
        $this->load->helper('download');
        $this->load->helper(array('form', 'url'));
        $this->load->library('message');
        $this->lang->load(array('common_lang'), isset($_SESSION['language']) ? $_SESSION['language'] : 'en');
    }

    function index()
    {
        $this->_head();
        $this->_side();
        $preschoolSeq = $_SESSION['preschoolSeq'];
        $data['teacherList'] = $this->Teacher_Model->getAllTeacherList($preschoolSeq);
        $this->load->view('manage/teacher', $data);
        $this->_bottom();
    }

    function addPopup(){
        $this->load->view('/manage/teacher_add_popup');
    }

    function modifyPopup(){
        $preschoolSeq = $_SESSION['preschoolSeq'];
        $data['classList'] = $this->Class_Model->getClassList($preschoolSeq);
        $data['teacherSeq'] = $this->uri->segments[3];
        $data['teacherInfo'] = $this->Teacher_Model->getTeacher($this->uri->segments[3]);
        $this->load->view('/manage/teacher_modify_popup', $data);
    }
//
    function setClass(){
        $classArr = $this->input->post();

        $result = $this->Teacher_Model->setTeacherClass($classArr);

        echo $result;
    }

    function approveTeacher(){
        $delArr = $this->input->post();

        $result = $this->Teacher_Model->updateTeacher($delArr);

        echo $result;
    }

    function registerTeacher(){
        $teacherInfo = $this->input->post();

//        var_dump($teacherInfo);
        $result = $this->Teacher_Model->registerTeacher($teacherInfo);

        echo $result;
    }

    function deleteTeacher(){
        $delArr = $this->input->post();

        $result = $this->Teacher_Model->deleteTeacher($delArr);

        echo $result;
    }

    function addPopupExcel(){
        $preschoolSeq = $_SESSION['preschoolSeq'];
        $data['classList'] = $this->Class_Model->getClassList($preschoolSeq);
        $this->load->view('/manage/teacher_add_excel_popup',$data);
    }


    function excelUpLoad()
    {

        $preschoolSeq = $_SESSION['preschoolSeq'];
        $preschoolName = $_SESSION['preschoolName'];

        $file_name = $preschoolSeq.'-teacher';


        $config['upload_path'] = './uploads/teacher';
        $config['allowed_types'] = 'xlsx';
        $config['max_size']	= '200';
        $config['file_name'] = $file_name;
        $config['encrypt_name'] = false;

        if (!file_exists('./uploads/teacher')) {        //연령 폴더 없으면 생성

            $old = umask(0);
            mkdir('./uploads/teacher', 0777);
            umask($old);

        }


        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload())
        {
            $error = array('error' => $this->upload->display_errors());
            echo $error;
        }
        else
        {

            echo $this->Teacher_Model->addTeacherByExcel($preschoolSeq,$preschoolName, $this->upload->data()['file_name']);

        }

    }

    public function createXLS() {

        $preschoolSeq = $_SESSION['preschoolSeq'];
        $objPHPExcel = new Spreadsheet();

        $activeSheet = $objPHPExcel->getActiveSheet();

        // set Header
        $activeSheet->SetCellValue('A1', '이름');
        $activeSheet->SetCellValue('B1', '이메일');
        $activeSheet->SetCellValue('C1', '담당학급');
        $activeSheet->SetCellValue('D1', '연락처');

        $activeSheet->getStyle('D:D')
            ->getNumberFormat()
            ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_TEXT);

        $activeSheet->getColumnDimension('A')->setWidth(15);
        $activeSheet->getColumnDimension('B')->setWidth(30);
        $activeSheet->getColumnDimension('C')->setWidth(15);
        $activeSheet->getColumnDimension('D')->setWidth(30);

        // set Row
        $rowCount = 2;
        $teacherInfo = $this->Teacher_Model->getAllTeacherList($preschoolSeq);

        foreach ($teacherInfo as $element) {
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element->user_name);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element->user_mail);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element->preschool_class_name);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element->user_contact);

            $rowCount++;
        }

        $date = date('Ymd');

        // create file name
        $fileName = '교사 명단_'.$date;
        $objWriter = new Xlsx($objPHPExcel);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'.$fileName.'.xlsx"');

        $objWriter->save('php://output');



    }

    function sendSMS()
    {
        $arr = $this->input->post();

        $result = $this->Teacher_Model->getTeacherContact($arr);

        if (sizeof($result) > 0) {

            try {

                $ch = $this->message->getHandle("/v1/send");

                foreach ($result as $teacher) {

                    $message = '[YummyMeal 접속 계정 안내] email : '.$teacher['user_mail'].'/ 비밀번호 : 생년월일';

                    $data = array(
                        "send_type" => "S", // 발송형태(R:예약,S:즉시)
                        "msg_type" => "S", // SMS : S, LMS : L, MMS : M
                        "to" => $teacher['user_contact'], // 수신자번호, ","으로 구분하여 100개까지 지정 가능하다.
                        "from" => "0317044398", // 발신자 번호, 발신자 번호는 사전등록된 번호여야 한다.
                        "msg" => $message, // 메시지 본문 내용
                    );

                    $this->message->setData($ch, $data);

                    $response = $this->message->sendPost($ch);
                    if ($response === FALSE) {
                        die(curl_error($ch));
                    }

                }
                // 결과는 사용자 시스템에 맞게 처리
                echo 'success';

            } catch (Exception $e) {
                echo $e->getMessage(); // get error message
            }

        } else {
            echo 'no_children';
        }
    }

}