<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class Children extends Base_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('Children_Model');
        $this->load->model('Class_Model');
        $this->load->helper(array('form', 'url'));
        $this->load->library('pagination');
        $this->load->library('message');
        $this->lang->load(array('common_lang'), isset($_SESSION['language']) ? $_SESSION['language'] : 'en');
    }

    function index()
    {
//        $this->_head();
//        $this->_side();
//        $preschoolSeq = $_SESSION['preschoolSeq'];
//        $data['childrenList'] = $this->Children_Model->getChildrenList($preschoolSeq, '', null, null);
//        $data['classList'] = $this->Class_Model->getClassList($preschoolSeq);
//        $this->load->view('manage/children', $data);
//        $this->_bottom();

        $this->lists();
    }


    function _remap($method)
    {
        if (method_exists($this, $method)) {

            if ($method == 'lists' || count($this->uri->segments) == 1) {
                $this->_head();
                $this->_side();
                $this->{"{$method}"}();
                $this->_bottom();
            } else {
                $this->{"{$method}"}();
            }

        }
    }

    function lists()
    {

        $preschoolSeq = $_SESSION['preschoolSeq'];
        $data['classList'] = $this->Class_Model->getClassList($preschoolSeq);

        // 페이지 네이션 설정
        $config['base_url'] = '/children/lists';
        // 페이징 주소
        $config['total_rows'] = $this->Children_Model->getChildrenList($preschoolSeq, 'count', null, null);
        // 게시물 전체 개수
        $config['per_page'] = 20;
        // 한 페이지에 표시할 게시물 수
        $config['uri_segment'] = 3;
        // 페이지 번호가 위치한 세그먼트

        $config['full_tag_open'] = '<div class="pagination">';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['prev_link'] = '이전 페이지';
        $config['prev_tag_open'] = '&nbsp;&nbsp;&nbsp;';
        $config['prev_tag_close'] = '&nbsp;&nbsp;&nbsp;';
        $config['next_link'] = '다음 페이지';
        $config['next_tag_open'] = '&nbsp;&nbsp;&nbsp;';
        $config['next_tag_close'] = '&nbsp;&nbsp;&nbsp;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;';
        $config['cur_tag_close'] = '&nbsp;&nbsp;&nbsp;';
        $config['num_tag_open'] = '&nbsp;&nbsp;&nbsp;';
        $config['num_tag_close'] = '&nbsp;&nbsp;&nbsp;';
        $config['full_tag_close'] = '</div>';

        // 페이지네이션 초기화
        $this->pagination->initialize($config);
        // 페이지 링크를 생성하여 view에서 사용하 변수에 할당
        $data['pagination'] = $this->pagination->create_links();


        // 게시물 목록을 불러오기 위한 offset, limit 값 가져오기
        $page = $this->uri->segment(3, 1);


        if ($page > 1) {
            $start = (($page / $config['per_page'])) * $config['per_page'];
        } else {
            $start = ($page - 1) * $config['per_page'];
        }

        $limit = $config['per_page'];


        $data['childrenList'] = $this->Children_Model->getChildrenList($preschoolSeq, '', $start, $limit);
        $this->load->view('manage/children', $data);
    }


    function getChildrenList()
    {

        $preschoolSeq = $_SESSION['preschoolSeq'];
        $result = $this->Children_Model->getChildrenList($preschoolSeq, '', null, null);

        echo json_encode($result);
    }

    function getChildrenListByClass()
    {
        $preschoolClassSeq = $this->input->post();

        $preschoolSeq = $_SESSION['preschoolSeq'];
        $result = $this->Children_Model->getChildrenListByClass($preschoolSeq, $preschoolClassSeq);

        echo json_encode($result);
    }

    function addPopup()
    {
        $preschoolSeq = $_SESSION['preschoolSeq'];
        $data['classList'] = $this->Class_Model->getClassList($preschoolSeq);
        $this->load->view('/manage/children_add_popup', $data);
    }

    function registerChild()
    {
        $childInfo = $this->input->post();

        $result = $this->Children_Model->registerChild($childInfo);

        echo $result;
    }

    function deleteChildren()
    {
        $delArr = $this->input->post();

        $result = $this->Children_Model->deleteChildren($delArr);

        echo $result;
    }


    /**
     * =========================================================================================================
     */

    function sendSMS()
    {
        $arr = $this->input->post();

        $result = $this->Children_Model->getGuardianContact($arr);

        if (sizeof($result) > 0) {

            try {

                $ch = $this->message->getHandle("/v1/send");

                foreach ($result as $child) {

                    $message = '안녕하세요 ' . $child['preschool_name'] . '입니다. Http://bos.datadrivencares.com/yummymeal 로 접속하여 어플을 설치하신 후 자녀 등록을 해주세요! 자녀분의 등록 코드는 [' . $child['activation_code'] . '] 입니다. 오늘도 즐거운 하루 보내세요~^^';


                    $data = array(
                        "send_type" => "S", // 발송형태(R:예약,S:즉시)
                        "msg_type" => "L", // SMS : S, LMS : L, MMS : M
                        "to" => $child['guardian_contact'], // 수신자번호, ","으로 구분하여 100개까지 지정 가능하다.
                        "from" => "0317044398", // 발신자 번호, 발신자 번호는 사전등록된 번호여야 한다.
                        "msg" => $message, // 메시지 본문 내용
                    );

                    $this->message->setData($ch, $data);

                    $response = $this->message->sendPost($ch);
                    if ($response === FALSE) {
                        die(curl_error($ch));
                    }

                }

                echo $this->Children_Model->updateChildren($result);
//                echo $response;

            } catch (Exception $e) {
                echo $e->getMessage(); // get error message
            }

        } else {
            echo 'no_children';
        }
    }

    function addPopupExcel()
    {

        $this->load->view('/manage/children_add_excel_popup');
    }


    function excelUpLoad()
    {

        $preschoolSeq = $_SESSION['preschoolSeq'];
        $preschoolName = $_SESSION['preschoolName'];

        $file_name = $preschoolSeq . '-children';


        $config['upload_path'] = './uploads/children';
        $config['allowed_types'] = 'xlsx|jpg|png';
        $config['max_size'] = '200';
        $config['file_name'] = $file_name;
        $config['encrypt_name'] = false;

        if (!file_exists('./uploads/children')) {        //연령 폴더 없으면 생성

            $old = umask(0);
            mkdir('./uploads/children', 0777);
            umask($old);

        }


        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {
            $error = array('error' => $this->upload->display_errors());
            echo $error;
        } else {

            echo $this->Children_Model->addChildrenByExcel($preschoolSeq, $preschoolName, $this->upload->data()['file_name']);

        }

    }


    public
    function createXLS()
    {

        $preschoolSeq = $_SESSION['preschoolSeq'];
        $preschoolClassSeq = $this->input->get('class');

        $objPHPExcel = new Spreadsheet();

        $activeSheet = $objPHPExcel->getActiveSheet();

        // set Header
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', '이름');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', '반');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', '학부모 이메일');
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', '학부모 연락처');

        $activeSheet->getColumnDimension('A')->setWidth(10);
        $activeSheet->getColumnDimension('B')->setWidth(15);
        $activeSheet->getColumnDimension('C')->setWidth(30);
        $activeSheet->getColumnDimension('D')->setWidth(30);

        $objPHPExcel->getActiveSheet()->getStyle('D:D')
            ->getNumberFormat()
            ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_TEXT);


        // set Row
        $rowCount = 2;
        if ($preschoolClassSeq == 'all') {
            $childrenInfo = $this->Children_Model->getChildrenList($preschoolSeq, null,null,null);
        } else {
            $childrenInfo = $this->Children_Model->getChildrenListByClass($preschoolSeq, $preschoolClassSeq);
        }


        foreach ($childrenInfo as $element) {
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element->bos_name == null ? $element->child_name : $element->bos_name);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element->preschool_class_name);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element->user_mail);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element->guardian_contact);

            $rowCount++;
        }

        $date = date('Ymd');

        // create file name
        $fileName = '원아 명단_' . $date;
        $objWriter = new Xlsx($objPHPExcel);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="' . $fileName . '.xlsx"');

        $objWriter->save('php://output');


    }


    function submitChildren()
    {
        $submitArr = $this->input->post();

        $result = $this->Children_Model->submitChildren($submitArr);

        echo $result;
    }


}