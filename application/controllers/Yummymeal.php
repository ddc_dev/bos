<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Yummymeal extends Base_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper('download');
        $this->lang->load(array('common_lang'), isset($_SESSION['language']) ? $_SESSION['language'] : 'en');
    }

    function index()
    {
        $this->_head();
        $this->load->view('yummymeal');
        $this->_bottom();
    }

    function teacher(){


        $this->load->view('teacher_down');


    }


    function teacher_down(){
        $data = file_get_contents('uploads/'); // Read the file's contents
        $name = 'yummymeal_teacher.apk';

        force_download($name, $data);
    }

    function parent(){

        $data = file_get_contents('uploads/'); // Read the file's contents
        $name = 'yummymeal.apk';

        force_download($name, $data);

        $this->load->view('teacher_download');

    }
}