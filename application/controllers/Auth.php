<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Auth extends Base_Controller {
    function __construct() {
        parent::__construct();
        
        $this->load->model('Auth_model');
        $this->load->model('District_Model');
        $this->load->library('message');
        $this->lang->load(array('common_lang'), isset($_SESSION['language'])?$_SESSION['language']:'en');
    }

    function index() {

        if(isset($_SESSION['userSeq'])){
            $this->load->helper('url');
            redirect('/main', 'refresh');
        } else {
            $this->session->sess_destroy();
            $this->_head();
            $this->load->view('intro');
            $this->_bottom();
        }

    }

    function agreement(){
        $this->load->view('/register/agreement');
    }

    function formInst(){
        $data['province'] = $this->District_Model->getProvince();
        $this->load->view('/register/formInst', $data);
    }

    function formAdmin(){
        $this->load->view('/register/formAdmin');
    }

    function register(){
        $instInfo = $this->input->post();
        
        $instResult = $this->Auth_model->registerInst($instInfo);

        if($instResult['status'] == 'success'){
            $adminResult = $this->Auth_model->signUpAdmin($instInfo, $instResult['instSeq']);

            if($adminResult == 201){
                $result['msg'] = '정상적으로 등록되었습니다. 로그인 후 서비스를 이용해주세요.';
                $result['url'] = '/auth';
            } else {
                $result['msg'] = '이미 등록된 사용자입니다.';
            }

        } else {

            if($instResult['msg'] == 'duplication') {
                $result['msg'] = '이미 등록된 기관이 존재합니다.';
            }
        }

        echo json_encode($result);

    }

    function signIn(){
        $userInfo = $this->input->post();

        $signInResult = $this->Auth_model->signInAdmin($userInfo);

        if($signInResult['statusCode'] == 200){
            if($signInResult['role'] == 2){

                if($signInResult['instInfo'][0]->inst_type == 1){
                    $sessionData = array(
                        'userSeq' => $signInResult['instInfo'][0]->user_seq,
                        'role' => $signInResult['role'],
                        'instType' => $signInResult['instInfo'][0]->inst_type,
                        'instSeq' => $signInResult['instInfo'][0]->inst_seq,
                        'adminType'=> $signInResult['instInfo'][0]->admin_type,
                        'officeSeq' => $signInResult['instInfo'][0]->office_seq,
                        'officeName' => $signInResult['instInfo'][0]->office_name,
                        'officeProvince' => $signInResult['instInfo'][0]->province_code,
                        'officeCity' => $signInResult['instInfo'][0]->city_code,
                    );
                } else if($signInResult['instInfo'][0]->inst_type == 2){
                    $sessionData = array(
                        'userSeq' => $signInResult['instInfo'][0]->user_seq,
                        'role' => $signInResult['role'],
                        'instType' => $signInResult['instInfo'][0]->inst_type,
                        'instSeq' => $signInResult['instInfo'][0]->inst_seq,
                        'adminType'=> $signInResult['instInfo'][0]->admin_type,
                        'preschoolSeq' => $signInResult['instInfo'][0]->preschool_seq,
                        'preschoolName' => $signInResult['instInfo'][0]->preschool_name,
                        'preschoolProvince' => $signInResult['instInfo'][0]->province_code,
                        'preschoolCity' => $signInResult['instInfo'][0]->city_code,
                        'preschoolVillage' => $signInResult['instInfo'][0]->village_code,
                    );
                }
            } else if($signInResult['role'] == 4){
                $sessionData = array(
                    'userSeq' => $signInResult['instInfo'][0]->user_seq,
                    'role' => $signInResult['role'],
                    'instType' => 2,
                    'instSeq' => $signInResult['instInfo'][0]->preschool_seq,
                    'adminType'=> 0,
                    'preschoolSeq' => $signInResult['instInfo'][0]->preschool_seq,
                    'preschoolName' => $signInResult['instInfo'][0]->preschool_name,
                    'preschoolProvince' => $signInResult['instInfo'][0]->province_code,
                    'preschoolCity' => $signInResult['instInfo'][0]->city_code,
                    'preschoolVillage' => $signInResult['instInfo'][0]->village_code,
                );
            }

            foreach($signInResult['userInfo'] as $key => $val){
                $sessionData[$key] = $val;
            }


            $this->session->set_userdata($sessionData);
        }

        echo json_encode($signInResult);
    }

    function login_action() {
        $currentIP = $_SERVER['REMOTE_ADDR'];
        $data['id'] = $this->input->post('id');
        $data['password'] = $this->input->post('password');
        $result = $this->Auth_model->login($data);
        $auth_permission = "";

        $result->status = 'Y';
        $result->seq = $result->user_seq;

        if ($result) {
            if (empty($result->ip)) {//ip상관없이 로그인
                if ($result->status == 'Y') {
                    $permission = $this->Auth_model->get_permission($result->seq);
                    foreach ($permission as $val) {
                        $auth_permission .= $val->permission . "|";
                    }
                    $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
                    $menu_list = $this->Auth_model->get_menu_list($result->seq);
                    
                    if (!$this->cache->get('menu_list')) {
                        $this->cache->save('menu_list', $menu_list);
                    }
                    
                    //세션에 추가적인 정보를 담을때 DB ci_session 테이블에 컬럼을 추가해준다
                    $newdata = array(
                        'seq' => $result->seq,
                        'id' => $result->id,
                        'name' => $result->name,
                        'logged_in' => TRUE,
                        'language' => 'en',
                        'permission' => $auth_permission
                    );
                    
                    $this->session->set_userdata($newdata);
                    Alert2url($result->name . ' is signed in.', '/admin/main');
                    exit;
                    
                } else {
                    Alert2url('Currently inactive.', '/auth');
                    exit;
                }
                
            } else {
                if (strpos($result->ip, $currentIP) !== false) {
                    if ($result->status == 'Y') {
                        $permission = $this->Auth_model->get_permission($result->seq);
                        foreach ($permission as $val) {
                            $auth_permission .= $val->permission . "|";
                        }
                        $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
                        $menu_list = $this->Auth_model->get_menu_list($result->seq);
                        
                        if (!$this->cache->get('menu_list')) {
                            $this->cache->save('menu_list', $menu_list);
                        }
                        
                        //세션에 추가적인 정보를 담을때 DB ci_session 테이블에 컬럼을 추가해준다
                        $newdata = array(
                            'seq' => $result->seq,
                            'id' => $result->id,
                            'name' => $result->name,
                            'logged_in' => TRUE,
                            'language' => 'en',
                            'permission' => $auth_permission
                        );
                        
                        $this->session->set_userdata($newdata);
                        Alert2url($result->name . ' is signed in.', '/dashboard/dashboard');
                        exit;
                        
                    } else {
                        Alert2url('Currently inactive.', '/auth');
                        exit;
                    }
                    
                } else {
                    Alert2url('Please check IP.', '/auth');
                    exit;
                }
            }
            
        } else {
            Alert2url('Please check your ID and password.', '/auth');
            exit;
        }
        
    }

    
    function logout() {
        $this -> session -> sess_destroy();
        Alert2url('로그아웃 되었습니다.', '/auth');
        exit;        
    }
    
    function language_action() {
        $lang = $_GET['lang'];
        $_SESSION['language'] = $lang;
    }

    function changeProfile(){

        $password = $this->input->post('password',TRUE);
        $phoneNumber = $this->input->post('phoneNumber',TRUE);

        $userSeq = $_SESSION['userSeq'];

        if ($password==null){
            $result = $this->Auth_model->changePhoneNumber($userSeq,$phoneNumber);
        }elseif ($phoneNumber==null){
            $result = $this->Auth_model->changePassword($userSeq,$password);
        }else{
            $result = $this->Auth_model->changePasswordAndPhoneNumber($userSeq,$password,$phoneNumber);
        }


        if ($result){
            echo 'success';
        }else{
            echo 'fail';
        }

    }

    function confirmContact()
    {

        $phoneNumber = $this->input->post('phoneNumber', TRUE);

        $rand_num = sprintf('%06d',rand(000000,999999));

        try {

            $ch = $this->message->getHandle("/v1/send");

            $data = array(
                "send_type" => "S", // 발송형태(R:예약,S:즉시)
                "msg_type" => "S", // SMS : S, LMS : L, MMS : M
                "to" => $phoneNumber, // 수신자번호, ","으로 구분하여 100개까지 지정 가능하다.
                "from" => "0317044398", // 발신자 번호, 발신자 번호는 사전등록된 번호여야 한다.
                "msg" => '[YummyMeal] 인증번호는 ['.$rand_num.'] 입니다.', // 메시지 본문 내용
            );

            $this->message->setData($ch, $data);

            $response = $this->message->sendPost($ch);
            if ($response === FALSE) {
                die(curl_error($ch));
            }

            // 결과는 사용자 시스템에 맞게 처리
            echo $rand_num;

        } catch (Exception $e) {
            echo 'fail';
        }

    }
}
