<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data extends Base_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->load->model('Data_Model');
        $this->lang->load(array('common_lang'), isset($_SESSION['language']) ? $_SESSION['language'] : 'en');
    }


    function index()
    {


        $this->load->view('test');

    }

    function get()
    {
        $province = $this->input->post('province', TRUE);

        $city = $this->input->post('city', TRUE);
        $city_arr = array();
        if ($city) array_push($city_arr, $city);

        $village = $this->input->post('village', TRUE);
        $village_arr = array();
        if ($village) array_push($village_arr, $village);

        $preschool = $this->input->post('preschool', TRUE);
        $preschool_arr = array();
        if ($preschool) array_push($preschool_arr, $preschool);

        $class = $this->input->post('class', TRUE);
        $class_arr = array();
        if ($class) array_push($class_arr, $class);

        $child = $this->input->post('child', TRUE);
        $child_arr = array();
        if ($child) array_push($child_arr, $child);


        $type = $this->input->post('type', TRUE);
        $action_time = $this->input->post('action_time', TRUE);


        /**
         *  no sorting option select from class action tables
         */

        if (!$province) {
            $result = $this->Data_Model->select_data_all($type, $action_time);                                          // 전체 데이터
        } else if (sizeof($city_arr) == 0) {
            $result = $this->Data_Model->select_data_from_province($province, $type, $action_time);                     // 시도별
        } else if (sizeof($village_arr) == 0) {
            $result = $this->Data_Model->select_data_from_city($city_arr, $type, $action_time);                         // 시군구별
        } else if (sizeof($preschool_arr) == 0) {
            $result = $this->Data_Model->select_data_from_village($village_arr, $type, $action_time);                   // 동별
        } else if (sizeof($class_arr) == 0) {
            $result = $this->Data_Model->select_data_from_preschool($preschool_arr, $type, $action_time);               // 유치원별
        } else if (sizeof($child_arr) == 0) {
            $result = $this->Data_Model->select_data_from_class($class_arr, $type, $action_time);                       // 클래스별
        } else{
            $result = $this->Data_Model->select_data_from_child($child_arr, $type, $action_time);                       // 아동별
        }


        echo $result;
    }


    function option()
    {
        $province = $this->input->post('province', TRUE);

        $city = $this->input->post('city', TRUE);
        $city_arr = array();
        if ($city) array_push($city_arr, $city);

        $village = $this->input->post('village', TRUE);
        $village_arr = array();
        if ($village) array_push($village_arr, $village);

        $preschool = $this->input->post('preschool', TRUE);
        $preschool_arr = array();
        if ($preschool) array_push($preschool_arr, $preschool);

        $class = $this->input->post('class', TRUE);
        $class_arr = array();
        if ($class) array_push($class_arr, $class);

        $child = $this->input->post('child', TRUE);
        $child_arr = array();
        if ($child) array_push($child_arr, $child);


        $term_type = $this->input->post('term_type', TRUE);
        $age_category = $this->input->post('age', TRUE);
        $gender_category = $this->input->post('gender', TRUE);
        $obesity = $this->input->post('obesity', TRUE);
        $action_time = $this->input->post('action_time', TRUE);


        /**
         * with sorting option select from child action tables
         */

        if (!$province) {
            $result = $this->Data_Model
                ->select_data_all_with_option($term_type, $action_time,$age_category,$gender_category,$obesity);                                          // 전체 데이터
        } else if (sizeof($city_arr) == 0) {
            $result = $this->Data_Model
                ->select_data_from_province_with_option($province, $term_type, $action_time,$age_category,$gender_category,$obesity);                     // 시도별
        } else if (sizeof($village_arr) == 0) {
            $result = $this->Data_Model
                ->select_data_from_city_with_option($city_arr, $term_type, $action_time,$age_category,$gender_category,$obesity);                         // 시군구별
        } else if (sizeof($preschool_arr) == 0) {
            $result = $this->Data_Model
                ->select_data_from_village_with_option($village_arr, $term_type, $action_time,$age_category,$gender_category,$obesity);                   // 동별
        } else if (sizeof($class_arr) == 0) {
            $result = $this->Data_Model
                ->select_data_from_preschool_with_option($preschool_arr, $term_type, $action_time,$age_category,$gender_category,$obesity);               // 유치원별
        } else if (sizeof($child_arr) == 0) {
            $result = $this->Data_Model
                ->select_data_from_class_with_option($class_arr, $term_type, $action_time,$age_category,$gender_category,$obesity);                       // 클래스별
        } else{
           $result = null;                   // 아동별
        }


        echo $result;

    }

}