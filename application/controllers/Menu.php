<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class Menu extends Base_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->load->model('Menu_Model');
        $this->load->helper(array('form', 'url'));
        $this->load->helper('download');
        $this->lang->load(array('common_lang'), isset($_SESSION['language']) ? $_SESSION['language'] : 'en');
    }

    function index() {
        $this->_head();
        $this->_side();

        $user_type = $_SESSION['instType'];

        if ($user_type==1){
            $this->load->view('meals/menu_admin');
        }else{
            $preschool_seq = $_SESSION['preschoolSeq'];
            $data['age_list'] = $this->Menu_Model->call_age_list($preschool_seq);
            $this->load->view('meals/menu',$data);
        }

        $this->_bottom();
    }

    function get_type(){

        $preschool_seq = $_SESSION['preschoolSeq'];
        $age = $this->input->get('age', TRUE);
        $year = $this->input->get('year', TRUE);
        $month = $this->input->get('month', TRUE);


        $result = $this->Menu_Model->call_type_list($age,$preschool_seq, $year, $month);

        echo json_encode($result);

    }

    function get(){

        $preschool_seq = $_SESSION['preschoolSeq'];
        $age = $this->input->post('age', TRUE);
        $year = $this->input->post('year', TRUE);
        $month = $this->input->post('month', TRUE);
        $type = $this->input->post('type', TRUE);
//
//
//        $preschool_seq = 1;
//        $class_seq = 1;
//        $year = 2019;
//        $month = 7;
//        $type = 'momma';


        $result =$this->Menu_Model->call_meal_list($preschool_seq,$age,$year,$month,$type);
        echo json_encode($result, JSON_UNESCAPED_UNICODE);


    }



    function show_popup(){

        $this->load->view('meals/add_file_popup');
    }

    function show_note(){

        $memo = $this->input->post('memo', TRUE);

        $data['memo'] = $memo;

        $this->load->view('meals/edit_note_popup',$data);
    }

    function show_salt(){

        $salt = $this->input->post('salt', TRUE);

        $data['salt'] = $salt;

        $this->load->view('meals/edit_salt_popup',$data);
    }


    function do_upload()
    {

        $preschool_seq = $_SESSION['preschoolSeq'];
        $preschool_name = $_SESSION['preschoolName'];

        $check = $this->input->post('check', TRUE);
        $month = $this->input->post('month', TRUE);
        $arr = explode('/', $month);



        $year = $arr[0];
        $month = $arr[1];

        $age = $this->input->post('age', TRUE);
        $type = $this->input->post('type', TRUE);


        $isExist = $this->Menu_Model->isMealsExist($preschool_seq, $age, $year, $month, $type);


        if ($isExist&&($check=='false')){
            echo 'exist';
        } else{

            if (!file_exists('./uploads/meals')) {        //식단 폴더 없으면 생성

                $old = umask(0);
                mkdir('./uploads/meals', 0777);
                umask($old);

            }

            if (!file_exists('./uploads/meals/' . $preschool_seq)) {        //유치원 폴더 없으면 생성

                $old = umask(0);
                mkdir('./uploads/meals/' . $preschool_seq, 0777);
                umask($old);

            }

            $config['upload_path'] = './uploads/meals/'.$preschool_seq;
            $config['allowed_types'] = 'xlsx|jpg|png';
            $config['max_size']	= '200';
            $config['encrypt_name'] = false;

            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload())
            {
                $error = array('error' => $this->upload->display_errors());
                echo $error;
            }
            else
            {
                $data = array('upload_data' => $this->upload->data());

                echo $this->Menu_Model->upload_menu($data['upload_data']['orig_name'],$preschool_seq,$preschool_name,$age,$year, $month, $type);

            }

        }



    }

    function uploadFile(){




    }


    function updateSalt(){
        $preschoolSeq = $_SESSION['preschoolSeq'];
        $userName = $_SESSION['user_name'];
        $age = $this->input->post('age', TRUE);
        $type = $this->input->post('type', TRUE);
        $salt = $this->input->post('salt', TRUE);
        $menuDate = $this->input->post('menuDate', TRUE);

        echo $this->Menu_Model->updateSalt($preschoolSeq,$userName,$type,$age,$menuDate,floatval($salt));
    }

    function updateNote(){
        $preschoolSeq = $_SESSION['preschoolSeq'];
        $age = $this->input->post('age', TRUE);
        $type = $this->input->post('type', TRUE);
        $note = $this->input->post('note', TRUE);
        $menuDate = $this->input->post('menuDate', TRUE);

        echo $this->Menu_Model->updateNote($preschoolSeq,$type,$age,$menuDate,$note);
    }

    public function downloadMenu() {

        $preschoolSeq = $_SESSION['preschoolSeq'];
        $age = $this->input->get('age', TRUE);
        $year = $this->input->get('year', TRUE);
        $month = $this->input->get('month', TRUE);
        $type = $this->input->get('type', TRUE);
//

        $isExist = $this->Menu_Model->isMealsExist($preschoolSeq, $age, $year, $month, $type);

        if ($isExist!='false'){

            $filePath = $isExist[0]->meals_file_path;
            $splitPath = explode('/', $filePath);

            $data = file_get_contents($filePath); // Read the file's contents
            $name = $splitPath[3];

            force_download($name, $data);

        }else{
            echo false;
        }
    }

    function downloadMenuAdmin(){

        $type = $this->input->get('type', TRUE);
        $date = $this->input->get('date', TRUE);

        $objPHPExcel = new Spreadsheet();

        $activeSheet = $objPHPExcel->getActiveSheet();

        // set Header
        $activeSheet->SetCellValue('A1', '날짜');
        $activeSheet->SetCellValue('B1', '기관명');
        $activeSheet->SetCellValue('C1', '측정자');
        $activeSheet->SetCellValue('D1', '메뉴명');
        $activeSheet->SetCellValue('E1', '염도');
        $activeSheet->SetCellValue('F1', '비고');


        $activeSheet->getColumnDimension('A')->setWidth(15);
        $activeSheet->getColumnDimension('B')->setWidth(15);
        $activeSheet->getColumnDimension('C')->setWidth(10);
        $activeSheet->getColumnDimension('D')->setWidth(50);
        $activeSheet->getColumnDimension('E')->setWidth(10);
        $activeSheet->getColumnDimension('F')->setWidth(50);

        // set Row
        $rowCount = 2;

        $menuInfo =  $this->Menu_Model->getMenuInfoForAdmin($type,$date);


        foreach ($menuInfo as $element) {
            $activeSheet->SetCellValue('A' . $rowCount, $element->menu_date);
            $activeSheet->SetCellValue('B' . $rowCount, $element->preschool_name);
            $activeSheet->SetCellValue('C' . $rowCount, $element->salt_writer);
            $activeSheet->SetCellValue('D' . $rowCount, $element->menu_soup);
            $activeSheet->SetCellValue('E' . $rowCount, $element->menu_salt);
            $activeSheet->SetCellValue('F' . $rowCount, $element->menu_note);

            $rowCount++;
        }

        $date = date('Ymd');

        // create file name
        $fileName = '염도 측정 내역서_' . $date;
        $objWriter = new Xlsx($objPHPExcel);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="' . $fileName . '.xlsx"');
//
        $objWriter->save('php://output');


    }
}