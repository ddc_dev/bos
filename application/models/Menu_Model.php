<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_Model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function call_meal_list($preschool_seq, $age, $year, $month, $type)
    {

        $firstDayString = $year . '-' . $month . '-01';
        $lastDay = date("Y-m-t", strtotime($firstDayString));


        $data = array(
            'preschool_seq' => $preschool_seq,
            'menu_age' => $age,
            'menu_type' => $type,
        );
        $this->db->where('menu_date >=', $firstDayString);
        $this->db->where('menu_date <=', $lastDay);
        $this->db->where($data);

        $query = $this->db->get('meals_menu');

        return $query->result();

    }

    function upload_menu($file_name, $preschool_seq, $preschool_name, $age, $year, $month, $type)
    {

        $inputFileType = 'Xlsx';
        $inputFileName = 'uploads/meals/' . $preschool_seq . '/' . $file_name;
        /**  Create a new Reader of the type defined in $inputFileType  **/
        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
        /**  Load $inputFileName to a Spreadsheet Object  **/

        $info = array(
            'preschool_seq' => $preschool_seq,
            'meals_type' => $type,
            'meals_age' => $age
            );

        $this->db->delete('meals_menu', $info);  // Produces: // DELETE FROM mytable  // WHERE id = $id


        $spreadsheet = $reader->load($inputFileName);
        $worksheet = $spreadsheet->getActiveSheet();
        $rows = $worksheet->toArray();

        $firstDayString = $year . '-' . $month . '-01';

        $menuDate = $date = new DateTime($firstDayString);

        $lastDay = date("t", strtotime($firstDayString));
        $lastDay = (int)$lastDay;

        $firstDayWeekNumber = date("w", strtotime("2019-07-01"));
        $firstDayWeekNumber = (int)$firstDayWeekNumber;


        if (($type == '0') || ($type == '1') || $type == '2') {
            $lineCount = 9;
        } else if ($type == '3') {
            $lineCount = 7;
        } else {
            $lineCount = 6;
        }

        $soup = null;
        $kcal = null;
        $protein = null;
        $count = 1;
        $rowCount = 1;

        $date = date('Y-m-d H:i:s');



        while ($count <= $lastDay) {

            for ($j = $firstDayWeekNumber; $j < 7; $j++) {

                if ($count > $lastDay)
                    break;

                if ($j != 0) {


                    if ($type == '0' || $type == '1' || $type == '2') {

                        $divideString = explode('/', $rows[$rowCount + 7][$j]);

                        $kcal = $divideString[0];
                        $protein = $divideString[1];
                        $soup = $rows[$rowCount + 2][$j];


                        $data = array(
                            'preschool_seq' => $preschool_seq,
                            'preschool_name' => $preschool_name,
                            'menu_age' => $age,
                            'menu_type' => $type,
                            'menu_date' => $menuDate->format("Y-m-d"),
                            'menu0' => $rows[$rowCount][$j],
                            'menu1' => $rows[$rowCount+1][$j],
                            'menu2' => $rows[$rowCount+2][$j],
                            'menu3' => $rows[$rowCount+3][$j],
                            'menu4' => $rows[$rowCount+4][$j],
                            'menu5' => $rows[$rowCount+5][$j],
                            'menu6' => $rows[$rowCount+6][$j],
                            'menu_soup' => $soup,
                            'menu_kcal' => $kcal,
                            'menu_protein' => $protein,
                            'menu_created_time' => $date,
                            'menu_updated_time' => $date
                        );
                        $this->db->insert('meals_menu', $data);


                    } elseif ($type == '3') {

                        if ($j != 6) {

                            $divideString = explode('/', $rows[$rowCount + 5][$j]);

                            $kcal = $divideString[0];
                            $protein = $divideString[1];
                            $soup = $rows[$rowCount + 1][$j];


                            $data = array(
                                'preschool_seq' => $preschool_seq,
                                'preschool_name' => $preschool_name,
                                'menu_age' => $age,
                                'menu_type' => $type,
                                'menu_date' => $menuDate->format("Y-m-d"),
                                'menu0' => $rows[$rowCount][$j],
                                'menu1' => $rows[$rowCount+1][$j],
                                'menu2' => $rows[$rowCount+2][$j],
                                'menu3' => $rows[$rowCount+3][$j],
                                'menu4' => $rows[$rowCount+4][$j],
                                'menu_soup' => $soup,
                                'menu_kcal' => $kcal,
                                'menu_protein' => $protein,
                                'menu_created_time' => $date,
                                'menu_updated_time' => $date
                            );
                            $this->db->insert('meals_menu', $data);

                        }


                    } else if ($type == '4') {

                        $data = array(
                            'preschool_seq' => $preschool_seq,
                            'preschool_name' => $preschool_name,
                            'menu_age' => $age,
                            'menu_type' => $type,
                            'menu_date' => $menuDate->format("Y-m-d"),
                            'menu0' => $rows[$rowCount][$j],
                            'menu1' => $rows[$rowCount+1][$j],
                            'menu2' => $rows[$rowCount+2][$j],
                            'menu3' => $rows[$rowCount+3][$j],
                            'menu4' => $rows[$rowCount+4][$j],
                            'menu_created_time' => $date,
                            'menu_updated_time' => $date
                        );
                        $this->db->insert('meals_menu', $data);

                    }






                }

                $menuDate->add(new DateInterval('P1D'));
                $count = $count + 1;
            }

            $firstDayWeekNumber = 0;
            $rowCount = $rowCount + $lineCount;


        }

        $data = array(
            'preschool_seq' => $preschool_seq,
            'meals_year' => $year,
            'meals_month' => $month,
            'meals_age' => $age,
            'meals_type' => $type
        );

        $this->db->where($data);
        $query = $this->db->get('meals');

        $result= $query->result();

        if (sizeof($result)>0) {              //업데이트

            $id = $result[0]->meals_seq;

            $data = array(
                'meals_file_path' => $inputFileName,
                'meals_updated_time' => $date
            );

            $this->db->where('meals_seq', $id);


            return $this->db->update('meals', $data);


        } else {            //새로 삽입


            $data = array(
                'preschool_seq' => $preschool_seq,
                'meals_year' => $year,
                'meals_month' => $month,
                'meals_age' => $age,
                'meals_type' => $type,
                'meals_file_path' => $inputFileName,
                'meals_created_time' => $date,
                'meals_updated_time' => $date
            );

            return $this->db->insert('meals', $data);
        }



    }


    function isMealsExist($preschool_seq, $age, $year, $month, $type){


        $data = array(
            'preschool_seq' => $preschool_seq,
            'meals_year' => $year,
            'meals_month' => $month,
            'meals_age' => $age,
            'meals_type' => $type
        );

        $this->db->where($data);
        $query = $this->db->get('meals');

        $result= $query->result();

        if (sizeof($result)>0){
            return $result;
        }else{
            return false;
        }

    }


    function updateSalt($preschool_seq, $user_name, $type, $age, $menuDate, $salt)
    {


        $data = array(
            'preschool_seq' => $preschool_seq,
            'menu_type' => $type,
            'menu_date' => $menuDate
        );

        $update = array(
            'menu_salt' => $salt,
            'salt_writer' => $user_name,
        );

        $this->db->where($data);
        $this->db->update('meals_menu', $update);

        return $menuDate;

    }




    function updateNote($preschool_seq, $type, $age, $menuDate, $note)
    {

        $data = array(
            'preschool_seq' => $preschool_seq,
            'menu_type' => $type,
            'menu_age' => $age,
            'menu_date' => $menuDate
        );

        $update = array(
            'menu_note' => $note,
        );

        $this->db->where($data);
        $this->db->update('meals_menu', $update);

        return $menuDate;

    }


    function getMemoAndSalt($preschool_seq, $year, $month, $type, $age)
    {

        $stmt = "SELECT meal_index, salt, memo FROM memo WHERE preschool_seq = ? AND meal_year = ? AND meal_month = ? AND meal_type = ? AND age = ?";
        $query = $this->db->query($stmt, array($preschool_seq, $year, $month, $type, $age));

        return $query->result_array();

    }

    function call_age_list($preschool_seq)
    {

        $stmt = "SELECT meals_age FROM meals WHERE preschool_seq = ? GROUP BY meals_age ";
        $query = $this->db->query($stmt, array($preschool_seq));

        return $query->result();

    }

    function call_type_list($age, $preschool_seq, $year, $month)
    {


        $stmt = "SELECT meals_type FROM meals WHERE preschool_seq = ? AND meals_age = ? AND meals_year = ? AND meals_month = ? ";
        $query = $this->db->query($stmt, array($preschool_seq, $age, $year, $month));

        return $query->result();
    }

    function getMenuInfoForAdmin($type,$date){

        if ($type =='day'){

            $stmt = "SELECT menu_date, menu_soup, menu_salt, menu_note FROM meals_menu WHERE menu_date = ? AND menu_soup IS NOT NULL AND menu_type != 3 GROUP BY preschool_seq,menu_date,menu_soup ";
            $query = $this->db->query($stmt, array($date));

        }else{
            $stmt = "SELECT * FROM meals_menu WHERE menu_soup IS NOT NULL AND menu_type != 3 AND menu_date BETWEEN ? AND ? GROUP BY preschool_seq,menu_date,menu_soup";

            $endDate = date("Y-m-t",strtotime($date));

            $query = $this->db->query($stmt,array($date,$endDate));
        }


        return $query->result();
    }
}