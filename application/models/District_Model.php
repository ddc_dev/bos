<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class District_Model extends CI_Model {
    function __construct()
    {
        parent::__construct();
    }

    function getProvince()
    {
        $getProvinceStmt = "SELECT province_code, province_name FROM district WHERE isnull(city_code) AND isnull(village_code)";
        return $this->db->query($getProvinceStmt)->result();
    }

    function getCity($code){

        if($code == "3611000000") {
            $getCityStmt = "SELECT city_code, city_name FROM district WHERE province_code = ? AND city_code IS NOT NULL";
        } else {
            $getCityStmt = "SELECT city_code, city_name FROM district WHERE province_code = ? AND city_code IS NOT NULL AND isnull(village_code)";
        }

        return $this->db->query($getCityStmt, array($code))->result();

    }

    function getVillage($code){
        $getVillageStmt = "SELECT village_code, village_name FROM district WHERE city_code = ? AND village_code IS NOT NULL";
        
        return $this->db->query($getVillageStmt, array($code))->result();
    }

}