<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Children_Model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function getChildrenList($preschoolSeq, $type, $offset, $limit)
    {

        $queryLimit= '';

        if(isset($offset)) {
            $queryLimit = ' LIMIT '.$offset.', '.$limit;
        }

        $queryGetChildrenList = 'SELECT * FROM child 
                                    WHERE preschool_seq = ?
                                    ORDER BY child_seq DESC
                                    '.$queryLimit;

        $executeGetChildrenList = $this->db->query($queryGetChildrenList, array($preschoolSeq));

        if($type == 'count'){
            $result = $executeGetChildrenList->num_rows();
        } else {
            $result = $executeGetChildrenList->result();
        }

        return $result;
    }

    function getChildrenListByClass($preschoolSeq, $preschoolClassSeq)
    {

        $queryGetChildrenList = 'SELECT * FROM child
                                    WHERE preschool_seq = ? AND preschool_class_seq = ?';

        $executeGetChildrenList = $this->db->query($queryGetChildrenList, array($preschoolSeq, $preschoolClassSeq));

        return $executeGetChildrenList->result();
    }

    function registerChild($array)
    {

        $childName = $array['childName'];
        $guardianContact = $array['guardianContact'];
        $preschoolSeq = $array['preschoolSeq'];
        $preschoolClassSeq = $array['preschoolClassSeq'];
        $preschoolClassName = $array['preschoolClassName'];
        $childGender = $array['childGender'];
        $birthdayYear = $array['birthdayYear'];
        $birthdayMonth = $array['birthdayMonth'];
        $birthdayDay = $array['birthdayDay'];
        $rand_num = sprintf('%06d',rand(000000,999999));

        if ($childGender == 0) {
            $childGender = null;
        }

        if ($birthdayYear != 0) {
            $birthday = $birthdayYear . '-' . $birthdayMonth . '-' . ($birthdayDay + 1);
        } else {
            $birthday = null;
        }

        $queryInsertChild = 'INSERT INTO child
                                (preschool_seq, preschool_class_seq, preschool_class_name, bos_name, child_gender, child_birthday, child_create_time, activation_code, activated, guardian_contact)
                                VALUES 
                                (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
        $date = date('Y-m-d H:i:s');

        $this->db->query($queryInsertChild, array($preschoolSeq, $preschoolClassSeq, $preschoolClassName, $childName, $childGender, $birthday, $date, $rand_num, 2, $guardianContact));


        $childSeq = $this->db->insert_id();


        $queryInsertChild = 'INSERT INTO bio_info
                                (child_seq, child_height, child_weight, child_bmi, bio_info_created_time)
                                VALUES 
                                (?, ?, ?, ?, ?)';

        $this->db->query($queryInsertChild, array($childSeq, 0, 0, 0, $date));


        $queryInsertChild = 'INSERT INTO child_image
                                (child_seq, child_img_path, child_img_updated_time)
                                VALUES 
                                (?, ?, ?)';

        $this->db->query($queryInsertChild, array($childSeq, "NULL", $date));


        if ($this->db->affected_rows() > 0) {
            return 'success';
        } else {
            return 'failed';
        }
    }


    function deleteChildren($array)
    {
        $delArray = $array['childSeqArr'];

        $queryUpdateChildren = 'UPDATE child
                                SET preschool_seq = null, preschool_class_seq = null, activated = 5
                                WHERE child_seq IN ?';

        $this->db->query($queryUpdateChildren, array($delArray));

        if ($this->db->affected_rows() > 0) {
            return 'success';
        } else {
            return 'failed';
        }
    }

    function addChildrenByExcel($preschoolSeq, $preschoolName, $fileName)
    {


        $inputFileType = 'Xlsx';
        $inputFileName = 'uploads/children/' . $fileName;
        /**  Create a new Reader of the type defined in $inputFileType  **/
        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
        /**  Load $inputFileName to a Spreadsheet Object  **/


        $spreadsheet = $reader->load($inputFileName);
        $worksheet = $spreadsheet->getActiveSheet();
        $rows = $worksheet->toArray();

        $count = 1;

        $data = array();

        while ($rows[$count][0] && $rows[$count][1] && $rows[$count][2] && $rows[$count][3]) {


            $name = $rows[$count][0];
            $phone = $rows[$count][1];
            $gender = $rows[$count][2] == "남자" ? 1 : 2;
            $birth = $rows[$count][3];

            $rand_num = sprintf('%06d',rand(000000,999999));


            $childData = array(
                'preschool_seq' => $preschoolSeq,
                'preschool_name' => $preschoolName,
                'bos_name' => $name,
                'child_gender' => $gender,
                'child_birthday' => $birth,
                'guardian_contact' =>$phone,
                'child_create_time' => $preschoolSeq,
                'activation_code' => $rand_num,
                'activated' => 2
            );

            $result =  $this->db->insert('child',$childData);

            if ($result){
                $childSeq = $this->db->insert_id();


                $queryInsertChild = 'INSERT INTO bio_info
                                (child_seq, child_height, child_weight, child_bmi, bio_info_created_time)
                                VALUES 
                                (?, ?, ?, ?, ?)';
                $date = date('Y-m-d H:i:s');

                $this->db->query($queryInsertChild, array($childSeq, 0, 0, 0, $date));

                $queryInsertChild = 'INSERT INTO child_image
                                (child_seq, child_img_path, child_img_updated_time)
                                VALUES 
                                (?, ?, ?)';

                $this->db->query($queryInsertChild, array($childSeq, "NULL", $date));

                $count = $count + 1;
            }else{
                break;
            }

        }



        if ($result){

            unlink($inputFileName);

            return true;
        }

        return false;
    }

    function getGuardianContact($arr)
    {
        $delArray = $arr['childSeqArr'];

        $queryUpdateChildren = 'SELECT a.*,b.preschool_name FROM child as a 
                                LEFT JOIN preschool as b
                                ON a.preschool_seq = b.preschool_seq
                                WHERE child_seq IN ?';

        $query = $this->db->query($queryUpdateChildren, array($delArray));

        return $query->result_array();

    }

    function submitChildren($array)
    {
        $submitArray = $array['childSeqArr'];


        //앱에서 신청했지만 승인이 되지 않은 = 1 만 승인된 상태인 4로 바꿈
        $queryUpdateChildren = 'UPDATE child
                                SET activated = 4
                                WHERE child_seq IN ? AND activated = 1';

        $this->db->query($queryUpdateChildren, array($submitArray));

        if ($this->db->affected_rows() > 0) {

            $selectTokenQuery = 'SELECT child_seq, child_name, firebase_token FROM child WHERE child_seq IN ? ';
            $result = $this->db->query($selectTokenQuery, array($submitArray));

            $result = $result->result();

            $headers = array(
                'Authorization: key=AAAAiBOzYpQ:APA91bGhkTucQya3tPBwc2e6KKqoUj1UmAF8uIlQ0O9yKBIaJtgBRWToAJ94gG_U0vaD7K0iGOILL9uSDSZe7gZvFKUBnIGGTeKrkiYZeWdqeo6iqSljBIrDeyjjW9i2WkWKaGGbpscK',
                'Content-Type: application/json'
            );

            foreach ($result as $child){

                $notification = array(
                    'body' => $child->child_name.' 아동의 기관 등록 승인이 완료되었습니다.',

            );
                $data = array(
                    'seq' => $child->child_seq
                );

                $fields = array(
                    'to' => $child->firebase_token,
                    'priority' => 'high',
                    'content_available' => true,
                    'notification' => $notification,
                    'data' => $data
                );


                $url = 'https://fcm.googleapis.com/fcm/send';


                $ch = curl_init();
                curl_setopt( $ch,CURLOPT_URL,$url);
                curl_setopt( $ch,CURLOPT_POST,true);
                curl_setopt( $ch,CURLOPT_HTTPHEADER,$headers);
                curl_setopt( $ch,CURLOPT_RETURNTRANSFER,true);
                curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER,false);
                curl_setopt( $ch,CURLOPT_POSTFIELDS,json_encode($fields));
                curl_exec($ch);

                $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

                curl_close($ch);
            }

            if ($httpcode==200){
                return 'success';
            }else{
                return 'failed';
            }

        } else {
            return 'failed';
        }
    }

    function updateChildren($array)
    {

        $seqArr = array_column($array, 'child_seq');


            
            $queryUpdateChildren = 'UPDATE child
                                SET activated = 3
                                WHERE child_seq = ? AND activated = 2';

            $this->db->query($queryUpdateChildren, array($seqArr));

            if ($this->db->affected_rows() > 0) {
                return'success';
            } else {
                return 'failed';
            }



    }


}