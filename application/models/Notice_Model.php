<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notice_Model extends CI_Model {
    function __construct() {
        parent::__construct();
    }

    function getNoticeList($type, $noticeType, $offset, $limit){

        $queryLimit= '';

        if(isset($offset)) {
            $queryLimit = ' LIMIT '.$offset.', '.$limit;
        }

        $addQuery = '';
        if($noticeType != 'all') {
            $addQuery = 'WHERE notice_type = '.$noticeType;
        }

        $queryGetNoticeList = 'SELECT * FROM notice '.$addQuery.' ORDER BY notice_seq DESC '.$queryLimit;

        $executeGetNoticeList = $this->db->query($queryGetNoticeList);

        if($type == 'count'){
            $result = $executeGetNoticeList->num_rows();
        } else {
            $result = $executeGetNoticeList->result();
        }
        
        return $result;
    }
    
    function getFixedNoticeList($noticeType) {
        $addQuery = '';
        $limit = 0;
        if($noticeType != 'all') {
            $addQuery = 'AND notice_type = '.$noticeType;
            $limit = 3;
        } else {
            $limit = 5;
        }
        
        $queryGetFixedNotice = 'SELECT * FROM notice WHERE fix = true '.$addQuery.' ORDER BY notice_seq DESC LIMIT '.$limit;

        $executeGetFixedNotice = $this->db->query($queryGetFixedNotice);

        return $executeGetFixedNotice->result();
    }

    function getNoticeDetail($param){
        $queryGetNoticeDetail = 'SELECT * FROM notice WHERE notice_seq = ?';

        $executeGetNoticeDetail = $this->db->query($queryGetNoticeDetail, array($param));

        return $executeGetNoticeDetail->result();
    }
    
    function registerNotice($array, $fileInfo){
        $userSeq = $array['userSeq'];
        $userName = $array['userName'];
        $instType = $array['instType'];
        $instSeq = $array['instSeq'];
        $noticeType = $array['noticeType'];
        $noticeFix = $array['noticeFix'];
        $noticeTitle = $array['noticeTitle'];
        $noticeContent = $array['noticeContent'];
        
        $date = date('Y-m-d H:i:s');

        if(count($fileInfo) > 0){
            $fileFolder = date('Y-m-d');
            $pathArray = array('origin'=>$fileInfo['upload_data']['client_name'], 'path'=>$fileFolder.'/'.$fileInfo['upload_data']['file_name']);
            $filePathArray = json_encode($pathArray);
        } else {
            $filePathArray = null;
        }
        
        $queryInsertNotice = 'INSERT INTO notice
                                (user_seq, user_name, inst_type, inst_seq, notice_type, title, content, notice_created_time, file_list, fix)
                                VALUES
                                (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
                              ';

        $this->db->query($queryInsertNotice, array($userSeq, $userName, $instType, $instSeq, $noticeType, $noticeTitle, $noticeContent, $date, $filePathArray, $noticeFix));

        if($this->db->affected_rows() > 0){
            $data['status'] = 'success';
            $data['noticeSeq'] = $this->db->insert_id();
        } else {
            $data['status'] = 'failed';
        }

        return $data;
    }
    
    function modifyNotice($array, $fileInfo){
        $noticeSeq = $array['noticeSeq'];
        $noticeFix = $array['noticeFix'];
        $noticeTitle = $array['noticeTitle'];
        $noticeContent = $array['noticeContent'];

        $date = date('Y-m-d H:i:s');

        if(count($fileInfo) > 0){
            $fileFolder = date('Y-m-d');
            $pathArray = array('origin'=>$fileInfo['upload_data']['client_name'], 'path'=>$fileFolder.'/'.$fileInfo['upload_data']['file_name']);
            $filePathArray = json_encode($pathArray);

            $queryInsertNotice = 'UPDATE notice SET
                                title = ?, content = ?, file_list = ?, fix = ?, notice_updated_time = ?
                                WHERE notice_seq = ?
                              ';

            $this->db->query($queryInsertNotice, array($noticeTitle, $noticeContent, $filePathArray, $noticeFix, $date, $noticeSeq));

        } else {
            $queryInsertNotice = 'UPDATE notice SET
                                title = ?, content = ?, fix = ?, notice_updated_time = ?
                                WHERE notice_seq = ?
                              ';

            $this->db->query($queryInsertNotice, array($noticeTitle, $noticeContent, $noticeFix, $date, $noticeSeq));

        }


        if($this->db->affected_rows() > 0){
            $data['status'] = 'success';
        } else {
            $data['status'] = 'failed';
        }

        return $data;
    }

    function deleteNotice($arr){
        $userSeq = $arr['userSeq'];
        $noticeSeq = $arr['noticeSeq'];


        $queryDeleteNotice = 'DELETE FROM notice WHERE notice_seq = ? AND user_seq = ?';

        $this->db->query($queryDeleteNotice, array($noticeSeq, $userSeq));

        if($this->db->affected_rows() > 0){
            return 'success';
        } else {
            return 'failed';
        }
    }
}