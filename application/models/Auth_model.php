<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Auth_model extends CI_Model {
    function __construct() {
        parent::__construct();
    }

    function registerInst($data){

        $instType = $data['instType'];

        $instName = $data['instName'];
        $instProvince = $data['instProvince'];
        $instProvinceName = $data['instProvinceName'];
        $instCity = $data['instCity'];
        $instCityName = $data['instCityName'];
        $instVillage = $data['instVillage'];
        $instVillageName = $data['instVillageName'];
        $instContact = $data['instContact'];

        if($this->checkInstExist($instType, $instName, $instProvince, $instCity, $instVillage)){
            if($instType == 'office'){

                $queryInsertOffice = "INSERT INTO public_office (office_name, province_code, province_name, city_code, city_name, office_contact) VALUES (?, ?, ?, ?, ?, ?)";
                $queryExecute = $this->db->query($queryInsertOffice, array($instName, $instProvince, $instProvinceName, $instCity, $instCityName, $instContact));

            } else if($instType == 'preschool'){

                $queryInsertPreschool = "INSERT INTO preschool
                                            (preschool_name, preschool_contact, province_code, province_name, city_code, city_name, village_code, village_name)
                                            VALUES
                                            (?, ?, ?, ?, ?, ?, ?, ?)";

                $queryExecute = $this->db->query($queryInsertPreschool, array($instName, $instContact, $instProvince, $instProvinceName, $instCity, $instCityName, $instVillage, $instVillageName));

            }
            $result['status'] = 'success';
            $result['instSeq'] = $this->db->insert_id();
            return $result;
            //기관 생성 완료
        } else {
            $result['status'] = 'failed';
            $result['msg'] = 'duplication';
            return $result;
        }
    }

    function signUpAdmin($data, $instSeq){

        //instType에 따라 기관 관리자인지 선생님인지 결정
        //api호출 -> userSeq 반환

        $url = 'https://www.datadrivencares.com:3000/api/users';
        $data = array('userMail' => $data['adminMail'],
                    'userPassword' => $data['adminPW'],
                    'userName' => $data['adminName'],
                    'userContact' => $data['adminContact'],
                    'role' => 'admin',
                    'oauth2Provider' => 'D',
                    'instType' => $data['instType'],
                    'instSeq' => $instSeq,
                    'instName' => $data['instName'],
        );


        $curl = curl_init();
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HEADER, true);

        $result = curl_exec($curl);
        
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);

        return $httpcode;

    }

    function signInAdmin($userInfo){
        $userMail = $userInfo['userMail'];
        $userPassword = $userInfo['userPassword'];

        $queryGetUser = 'SELECT user_seq, user_password, user_mail, user_name, role_seq FROM user WHERE user_mail = ?';

        $queryExecute = $this->db->query($queryGetUser, array($userMail));

        if ($queryExecute->num_rows() == 1) {

            $userRow = $queryExecute->result();
            $userSeq = $userRow[0]->user_seq;
            $role = $userRow[0]->role_seq;

            if(password_verify($userPassword, $userRow[0]->user_password)){

                $userInfo = array(
                    'user_seq' => $userSeq,
                    'user_mail' => $userRow[0]->user_mail,
                    'user_name' => $userRow[0]->user_name,
                );

                $data['userInfo'] = $userInfo;

                if($role == 2){
                    $queryGetAdmin =
                        'SELECT * FROM admin as a
                            LEFT JOIN preschool as b
                            ON a.admin_seq = b.admin_seq
                            LEFT JOIN public_office as c
                            ON a. admin_seq = c.admin_seq
                            WHERE a.user_seq = ?';

                    $queryAdminExecute = $this->db->query($queryGetAdmin, array($userSeq))->result();
                    $data['statusCode'] = 200;
                    $data['role'] = 2;
                    $data['instInfo'] = $queryAdminExecute;

                } else if($role == 4) {
                    $queryGetAdmin =
                        'SELECT * FROM teacher as a
                            LEFT JOIN preschool as b
                            ON a.preschool_seq = b.preschool_seq
                            WHERE user_seq = ?';

                    $queryAdminExecute = $this->db->query($queryGetAdmin, array($userSeq))->result();
                    $data['statusCode'] = 200;
                    $data['role'] = 4;
                    $data['instInfo'] = $queryAdminExecute;
                }

            } else {
                $data['statusCode'] = 401;
                $data['message'] = 'invalid password';
            }

        } else {
                $data['statusCode'] = 404;
                $data['message'] = 'user not found';
        }

        return $data;
    }

    function checkInstExist($instType, $instName, $instProvince, $instCity, $instVillage){

        if($instType == 'office'){
            $queryGetOfficeCount = "SELECT * FROM public_office WHERE office_name = ? AND province_code = ? AND city_code = ?";

            $rowCount = $this->db->query($queryGetOfficeCount, array($instName, $instProvince, $instCity))->row();
        } else if($instType == 'preschool') {
            $queryGetPreschoolCount = "SELECT * FROM preschool WHERE preschool_name = ? AND province_code= ? AND city_code= ? AND village_code = ?";

            $rowCount = $this->db->query($queryGetPreschoolCount, array($instName, $instProvince, $instCity, $instVillage))->row();
        }

        if(!isset($rowCount)){
            $result = true;
        } else {
            $result = false;
        }

        return $result;
    }

    function login($data) {
//        $sql = "SELECT seq, id, name, status, ip FROM admin WHERE id='".$data['id']."' AND password=password('".$data['password']."')";
//        $query = $this->db->query($sql);
//        if ($query->num_rows() > 0) {
//            return $query->row();
//        } else {
//            return FALSE;
//        }

        $url = 'https://www.datadrivencares.com:3000/api/auth';
        $data = array('userMail' => $data['id'], 'userPassword' => $data['password'], 'oauth2Provider' => 'D');


        $curl = curl_init();
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($curl);

        curl_close($curl);

        return json_decode($result);
    }
    
    function get_menu_list($seq) {
        $sql = "SELECT * FROM menu_list a, menu_permission b WHERE a.seq=b.seq_menu_list AND b.seq_admin=".$seq." AND a.show_flag='Y' ORDER BY a.order ASC";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }
    
    function get_permission($seq) {
        $sql = "SELECT CASE WHEN b.permission='W' THEN a.write WHEN b.permission='R' THEN a.read END permission FROM menu_list a, menu_permission b WHERE a.seq=b.seq_menu_list AND b.seq_admin=".$seq;
        $query = $this->db->query($sql);
        
        return $query->result();        
    }


    function changePhoneNumber($userSeq,$phoneNumber){


        $this->db->where('user_seq', $userSeq);
        $this->db->set('user_contact', $phoneNumber);
        return $this->db->update('user');

    }

    function changePassword($userSeq,$password){

        $cost = ['cost' => 11];
        $password = password_hash($password, PASSWORD_BCRYPT, $cost);

        $this->db->where('user_seq', $userSeq);
        $this->db->set('user_password', $password);
        return $this->db->update('user');



    }

    function changePasswordAndPhoneNumber($userSeq,$password,$phoneNumber){

        $cost = ['cost' => 11];
        $password = password_hash($password, PASSWORD_BCRYPT, $cost);


        $this->db->where('user_seq', $userSeq);
        $this->db->set('user_contact', $phoneNumber);
        $this->db->set('user_password', $password);
        return $this->db->update('user');

    }
}

