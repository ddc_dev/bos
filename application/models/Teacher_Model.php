<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Teacher_Model extends CI_Model {
    function __construct()
    {
        parent::__construct();
    }

    function getAllTeacherList($preschoolSeq){
        $queryGetAllTeacherList = 'SELECT * FROM teacher
                                    WHERE preschool_seq = ? AND activated != 0';

        $executeGetTeacherList = $this->db->query($queryGetAllTeacherList, array($preschoolSeq));

        return $executeGetTeacherList->result();
    }

    function getNotAdminTeacherList($preschoolSeq){
        $queryGetAllTeacherList = 'SELECT a.teacher_seq, b.user_name FROM teacher as a
                                    LEFT JOIN user as b on a.user_seq = b.user_seq
                                    LEFT JOIN admin as c on a.user_seq = c.user_seq
                                    WHERE a.preschool_seq = ? AND a.activated != 0 AND c.admin_seq IS NULL';

        $executeGetTeacherList = $this->db->query($queryGetAllTeacherList, array($preschoolSeq));

        return $executeGetTeacherList->result();
    }

    function getClassTeacherList($preschoolSeq) {
        $queryGetClassTeacher = 'SELECT * FROM teacher WHERE preschool_seq = ? AND preschool_class_seq IS NOT NULL';

        $excuteGetClassTeacherList = $this->db->query($queryGetClassTeacher, array($preschoolSeq));

        return $excuteGetClassTeacherList->result();
    }

    //반이 지정안된 선생님들 용
    function getNoClassTeacherList($preschoolSeq){

        $queryGetTeacherList = 'SELECT * FROM teacher WHERE preschool_seq = ? AND ISNULL(preschool_class_seq) AND activated != 0';
        
        $executeGetTeacherList = $this->db->query($queryGetTeacherList, array($preschoolSeq));

        return $executeGetTeacherList->result();
    }

    function registerTeacher($array){
        
        $teacherName = $array['teacherName'];
        $teacherMail = $array['teacherMail'];
        $teacherBD = $array['teacherBD'];
        $teacherContact = $array['teacherContact'];
        $preschoolSeq = $array['preschoolSeq'];
        $preschoolName = $array['preschoolName'];

        $cost = ['cost' => 11];
        $password = password_hash($teacherBD, PASSWORD_BCRYPT, $cost);

        if($this->checkUserExist($teacherMail)){
            $queryInsertUser = 'INSERT INTO user
                            (user_mail, user_password, user_name, user_created_time, role_seq)
                            VALUES
                            (?, ?, ?, ?, ?)';

            $date = date('Y-m-d H:i:s');

            $queryInsertTeacher = 'INSERT INTO teacher
                                    (user_seq, user_mail, user_name, user_contact, preschool_seq, preschool_name, activated, teacher_approved_time)
                                    VALUES 
                                    (?, ?, ?, ?, ?, ?, ?, ?)';

            $this->db->trans_start();
            $this->db->query($queryInsertUser, array($teacherMail, $password, $teacherName, $date, 4));
            $this->db->query($queryInsertTeacher, array($this->db->insert_id(), $teacherMail, $teacherName, $teacherContact, $preschoolSeq, $preschoolName, 2, $date));
            $this->db->trans_complete();

            if($this->db->trans_status() === true){
                return 'success';
            } else {
                return 'failed';
            }

        } else {
            //이미 가입된 유저
            return 'duplicate';
        }
    }

    function setTeacherClass($array){
        $teacherSeq = $array['teacherSeq'];
        $preschoolClassSeq = $array['preschoolClassSeq'];
        $preschoolClassName = $array['preschoolClassName'];

        $queryUpdateClass = 'UPDATE teacher SET preschool_class_seq = ?, preschool_class_name = ? WHERE teacher_seq = ?';

        $result = $this->db->query($queryUpdateClass, array($preschoolClassSeq, $preschoolClassName, $teacherSeq));

        if($this->db->affected_rows() > 0) {
            return 200;
        } else {
            return 404;
        }
    }

    function updateTeacher($array){
        $teacherArray = $array['teacherSeqArr'];

        $queryUpdateTeacher = 'UPDATE teacher
                                SET activated = 2
                                WHERE teacher_seq IN ?';

        $result = $this->db->query($queryUpdateTeacher, array($teacherArray));

        if($this->db->affected_rows() > 0) {

            $selectTokenQuery = 'SELECT teacher_seq, firebase_token FROM teacher WHERE teacher_seq IN ? ';
            $result = $this->db->query($selectTokenQuery, array($teacherArray));

            $result = $result->result();

            $headers = array(
                'Authorization: key=AAAAiBOzYpQ:APA91bGhkTucQya3tPBwc2e6KKqoUj1UmAF8uIlQ0O9yKBIaJtgBRWToAJ94gG_U0vaD7K0iGOILL9uSDSZe7gZvFKUBnIGGTeKrkiYZeWdqeo6iqSljBIrDeyjjW9i2WkWKaGGbpscK',
                'Content-Type: application/json'
            );



            foreach ($result as $child){

                $data = array(
                    'body' => '기관 등록 승인이 완료되었습니다.'
                );

                $fields = array(
                    'to' => $child->firebase_token,
                    'priority' => 'high',
                    'content_available' => true,
                    'notification' => $data
                );


                $url = 'https://fcm.googleapis.com/fcm/send';


                $ch = curl_init();
                curl_setopt( $ch,CURLOPT_URL,$url);
                curl_setopt( $ch,CURLOPT_POST,true);
                curl_setopt( $ch,CURLOPT_HTTPHEADER,$headers);
                curl_setopt( $ch,CURLOPT_RETURNTRANSFER,true);
                curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER,false);
                curl_setopt( $ch,CURLOPT_POSTFIELDS,json_encode($fields));
                curl_exec($ch);

                $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

                curl_close($ch);
            }

            if ($httpcode==200){
                return 'success';
            }else{
                return $httpcode;
            }



        } else {
            return 'failed';
        }
    }

    function deleteTeacher($array){
        $delArray = $array['teacherSeqArr'];

        $queryUpdateTeacher = 'UPDATE teacher
                                SET activated = 3
                                WHERE teacher_seq IN ?';
        
        $result = $this->db->query($queryUpdateTeacher, array($delArray));

        if($this->db->affected_rows() > 0) {
            return 'success';
        } else {
            return 'failed';
        }
    }

    function checkUserExist($userMail) {
        $queryGetUserCount = 'SELECT * FROM user WHERE user_mail = ?';

        $count = $this->db->query($queryGetUserCount, array($userMail))->row();

        if(!isset($count)){
            return true;
        } else {
            return false;
        }
    }


    function addTeacherByExcel($preschoolSeq, $preschoolName, $fileName)
    {


        $inputFileType = 'Xlsx';
        $inputFileName = 'uploads/teacher/' . $fileName;
        /**  Create a new Reader of the type defined in $inputFileType  **/
        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
        /**  Load $inputFileName to a Spreadsheet Object  **/


        $spreadsheet = $reader->load($inputFileName);
        $worksheet = $spreadsheet->getActiveSheet();
        $rows = $worksheet->toArray();

        $count = 1;

        $data = array();

        while ($count<sizeof($rows)) {


            $name = $rows[$count][0];
            $birth = $rows[$count][1];
            $mail = $rows[$count][2];
            $phone = $rows[$count][3];
            $cost = ['cost' => 11];
            $password = password_hash($birth, PASSWORD_BCRYPT, $cost);

            if($this->checkUserExist($mail)) {
                $queryInsertUser = 'INSERT INTO user
                            (user_mail, user_password, user_name, user_created_time, role_seq)
                            VALUES
                            (?, ?, ?, ?, ?)';

                $date = date('Y-m-d H:i:s');

                $queryInsertTeacher = 'INSERT INTO teacher
                                    (user_seq, user_mail, user_name, preschool_seq, preschool_name, activated, teacher_approved_time, user_contact)
                                    VALUES 
                                    (?, ?, ?, ?, ?, ?, ?, ?)';

                $this->db->trans_start();
                $this->db->query($queryInsertUser, array($mail, $password, $name, $date, 4));
                $this->db->query($queryInsertTeacher, array($this->db->insert_id(), $mail, $name, $preschoolSeq, $preschoolName, 2, $date, $phone));
                $this->db->trans_complete();
            }
                $count = $count + 1;
        }


        if ($this->db->trans_status() === true){

            unlink($inputFileName);

            return true;
        }

        return false;
    }


    function getTeacher($teacherSeq)
    {

        $queryGetTeacher = 'SELECT * FROM teacher WHERE teacher_seq = ?';

        $query = $this->db->query($queryGetTeacher, array($teacherSeq));

        return $query->result();

    }
}