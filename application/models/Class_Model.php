<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Class_Model extends CI_Model {
    function __construct()
    {
        parent::__construct();
    }

    function getClassList($preschoolSeq){

        $queryGetClassList = 'SELECT d.*, count(e.child_seq) as child_count FROM preschool_class as d
                                LEFT JOIN child as e
                                ON e.preschool_class_seq = d.preschool_class_seq
                                WHERE d.preschool_seq = ?
                                GROUP BY d.preschool_class_seq';

        $executeGetClassList = $this->db->query($queryGetClassList, array($preschoolSeq));
        return $executeGetClassList->result();
    }

    function registerClass($classInfo, $preschoolSeq){

        $preschoolClassName = $classInfo['className'];
//        $teacherSeq = $classInfo['teacherSeq'];

        if ($this->checkClassExist($preschoolSeq, $preschoolClassName)){
            $queryInsertClass = 'INSERT INTO preschool_class
                                (preschool_seq, preschool_class_name, preschool_class_created_time)
                                VALUES
                                (?, ?, ?)';

            $date = date('Y-m-d H:i:s');

            $executeInsertClass = $this->db->query($queryInsertClass, array($preschoolSeq, $preschoolClassName, $date));

//            $preschoolClassSeq = $this->db->insert_id();

//            $queryUpdateTeacher = 'UPDATE teacher SET preschool_class_seq = ? WHERE teacher_seq = ?';
//
//            $executeUpdateTeacher = $this->db->query($queryUpdateTeacher, array($preschoolClassSeq, $classInfo['teacherSeq']));

            $result = 200;
        } else {
            $result = 401;
        }

        return $result;
    }

    function updateClass($data, $preschoolSeq){

        $preschoolClassSeq = $data['classSeq'];
        $preschoolClassName = $data['className'];
        $teacherSeq = $data['teacherSeq'];

        $queryUpdateClass = 'UPDATE preschool_class SET preschool_class_name = ?, preschool_class_updated_time = ? WHERE preschool_class_seq = ?';
        $date = date('Y-m-d H:i:s');
        $executeUpdateClass = $this->db->query($queryUpdateClass, array($preschoolClassName, $date, $preschoolClassSeq));

        if($teacherSeq != 0) {
            $queryUpdateTeacher = 'UPDATE teacher
                                    SET preschool_class_seq = ?, preschool_class_name = ?
                                    WHERE teacher_seq = ?';

            $executeUpdateTeacher = $this->db->query($queryUpdateTeacher, array($preschoolClassSeq, $preschoolClassName, $teacherSeq));
        }

        if($this->db->affected_rows() > 0) {
            return 200;
        } else {
            return 404;
        }

    }

    function deleteClass($array){

        $delArray = $array['classSeqArr'];

        $queryUpdateTeacher = 'UPDATE teacher AS a
                                LEFT JOIN preschool_class AS b
                                ON b.teacher_seq = a.teacher_seq
                                SET a.preschool_class_seq = null
                                WHERE a.preschool_class_seq IN ?';

        $queryDeletePreschoolClass = 'DELETE FROM preschool_class WHERE preschool_class_seq IN ?';

        $this->db->trans_start();
        $this->db->query($queryUpdateTeacher, array($delArray));
        $this->db->query($queryDeletePreschoolClass, array($delArray));
        $this->db->trans_complete();

        if($this->db->trans_status() === true){
            return 'success';
        } else {
            return 'failed';
        }

    }


    function checkClassExist($preschoolSeq, $preschoolClassName){
        $queryGetClassCount = "SELECT count(*) as count FROM preschool_class WHERE preschool_seq = ? AND preschool_class_name = ?";

        $executeGetClassCount = $this->db->query($queryGetClassCount, array($preschoolSeq, $preschoolClassName))->result();

//        var_dump($executeGetClassCount);

        if($executeGetClassCount[0]->count == 0) {
            return true;
        } else {
            return false;
        }
    }
}