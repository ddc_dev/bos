<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_Model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function select_data_all($term_type, $action_time)
    {
       return $this->select_data_from_class(null, $term_type, $action_time);
    }

    function select_data_all_with_option($term_type, $action_time, $age_category, $gender_category, $obesity)
    {
        return $this->select_data_from_child(null, $term_type, $action_time, $age_category, $gender_category, $obesity);
    }

    function select_data_from_province($province, $term_type, $action_time)
    {
        $stmt = "SELECT preschool_seq FROM preschool WHERE preschool_address_depth1 = " . $province;
        $result = $this->db->query($stmt)->result();

        return $this->select_data_from_preschool(array_column($result, 'preschool_seq'), $term_type, $action_time);
    }

    function select_data_from_province_with_option($province, $term_type, $action_time, $age_category, $gender_category, $obesity)
    {
        $stmt = "SELECT preschool_seq FROM preschool WHERE preschool_address_depth1 = " . $province;
        $result = $this->db->query($stmt)->result();

        return $this->select_data_from_preschool_with_option(array_column($result, 'preschool_seq'), $term_type, $action_time, $age_category, $gender_category, $obesity);

    }

    function select_data_from_city($city, $term_type, $action_time)
    {
        $in_list = empty($city) ? 'NULL' : "'" . join("','", $city) . "'";
        $stmt = "SELECT preschool_seq FROM preschool WHERE preschool_address_depth2 IN (" . $in_list . ")";
        $result = $this->db->query($stmt)->result();

        return $this->select_data_from_preschool(array_column($result, 'preschool_seq'), $term_type, $action_time);
    }

    function select_data_from_city_with_option($city_arr, $term_type, $action_time, $age_category, $gender_category, $obesity)
    {
        $in_list = empty($city_arr) ? 'NULL' : "'" . join("','", $city_arr) . "'";
        $stmt = "SELECT preschool_seq FROM preschool WHERE preschool_address_depth2 IN (" . $in_list . ")";
        $result = $this->db->query($stmt)->result();

        return $this->select_data_from_preschool_with_option(array_column($result, 'preschool_seq'), $term_type, $action_time, $age_category, $gender_category, $obesity);

    }

    function select_data_from_village($village, $term_type, $action_time)
    {
        $in_list = empty($village) ? 'NULL' : "'" . join("','", $village) . "'";
        $stmt = "SELECT preschool_seq FROM preschool WHERE preschool_address_depth3 IN (" . $in_list . ")";
        $result = $this->db->query($stmt)->result();

        return $this->select_data_from_preschool(array_column($result, 'preschool_seq'), $term_type, $action_time);
    }

    function select_data_from_village_with_option($village_arr, $term_type, $action_time, $age_category, $gender_category, $obesity)
    {
        $in_list = empty($village_arr) ? 'NULL' : "'" . join("','", $village_arr) . "'";
        $stmt = "SELECT preschool_seq FROM preschool WHERE preschool_address_depth3 IN (" . $in_list . ")";
        $result = $this->db->query($stmt)->result();

        return $this->select_data_from_preschool_with_option(array_column($result, 'preschool_seq'), $term_type, $action_time, $age_category, $gender_category, $obesity);

    }

    function select_data_from_preschool($preschool, $term_type, $action_time)
    {


        $in_list = empty($preschool) ? 'NULL' : "'" . join("','", $preschool) . "'";
        $stmt = "SELECT preschool_class_seq FROM preschool_class WHERE preschool_seq IN (" . $in_list . ")";
        $result = $this->db->query($stmt)->result();

        return $this->select_data_from_class(array_column($result, 'preschool_class_seq'), $term_type, $action_time);
    }

    function select_data_from_preschool_with_option($preschool_arr, $term_type, $action_time, $age_category, $gender_category, $obesity)
    {

        $age_query = "";
        $gender_query = "";
        $obesity_query = "";

        if ($age_category!=0){
            $age_start_date = null;
            $age_end_date = null;

            $age_query = " AND child_birthday BETWEEN ".$age_start_date." AND ".$age_end_date;
        }

        if ($gender_query!=0){
            $gender_query = " AND child_gender = ".$gender_category;
        }

        if ($obesity!=-1){
            $obesity_query = " AND child_obesity = ".$obesity;
        }

        $in_list = empty($preschool_arr) ? 'NULL' : "'" . join("','", $preschool_arr) . "'";
        $stmt = "SELECT child_seq FROM child WHERE preschool_seq IN (" . $in_list . ")".$age_query.$gender_query.$obesity_query;
        $result = $this->db->query($stmt)->result();


        if ($result){
            return $this->select_data_from_child(array_column($result, 'child_seq'), $term_type, $action_time);
        }else return null;



    }

    function select_data_from_class($class, $term_type, $action_time)
    {

        $action_time_column = 'action_date';

        switch ($term_type) {
            case 'day':
                $action_end_time = date("Y-m-d", strtotime("+1 days", strtotime($action_time)));
                $table_name = 'class_act_hour';
                $action_time_column = 'action_time';
                break;
            case 'week':
                $action_end_time = date("Y-m-d", strtotime("+7 days", strtotime($action_time)));
                $table_name = 'class_act_day';
                break;
            case 'month':
                $action_end_time = date("Y-m-t", strtotime($action_time));
                $table_name = 'class_act_day';
                break;
            case 'year':
                $action_end_time = date("Y-m-d", strtotime("+1 years", strtotime($action_time)));
                $table_name = 'class_act_month';
                break;
            default:
                return null;
        }

        if ($class) {
            $in_list = empty($class) ? 'NULL' : "'" . join("','", $class) . "'";
            $stmt = "SELECT * FROM " . $table_name . " WHERE preschool_class_seq IN (" . $in_list . ") AND " . $action_time_column . " BETWEEN ? AND ? ORDER BY " . $action_time_column;
        }else{
            $stmt = "SELECT * FROM " . $table_name . " WHERE " . $action_time_column . " BETWEEN ? AND ? ORDER BY " . $action_time_column;

        }
        $query = $this->db->query($stmt, array($action_time, $action_end_time));


        $act1 = 'act1';
        $act2 = 'act2';
        $act3 = 'act3';


        if ($query) {

            $query_result_arr = $query->result_array();


            $result_arr = array();
            $time_term = "+1 hour";
            $format = "Y-m-d H:00:00";
            $last_count = 24;
            $start_action_time = $action_time;
            switch ($term_type) {
                case 'day':
                    $time_term = "+1 hour";
                    $format = "Y-m-d H:00:00";
                    $last_count = 24;
                    $start_action_time = $start_action_time . " 00:00:00";
                    break;
                case 'week':
                    $time_term = "+1 days";
                    $format = "Y-m-d";
                    $last_count = 7;
                    break;
                case 'month':
                    $time_term = "+1 days";
                    $format = "Y-m-d";
                    $last_count = date("t", strtotime($action_time));
                    break;
                case 'year':
                    $time_term = "+1 month";
                    $format = "Y-m-01";
                    $last_count = 12;
                    break;
            }

            $index = 0;
            $result = array();

            $result['act1'] = 0;
            $result['act2'] = 0;
            $result['act3'] = 0;
            $result['action_time'] = $start_action_time;
            $result['count'] = 0;
            foreach ($query_result_arr as $query_data) {

                while (($result['action_time'] != $query_data[$action_time_column]) && ($index < $last_count)) {

                    if ($result['count'] > 1) {

                        $result['act1'] = $result['act1'] / $result['count'];
                        $result['act2'] = $result['act2'] / $result['count'];
                        $result['act3'] = $result['act3'] / $result['count'];
                    }
                    array_push($result_arr, $result);

                    $next_action_time = date($format, strtotime($time_term, strtotime($result['action_time'])));
                    $index = $index + 1;


                    $result['act1'] = 0;
                    $result['act2'] = 0;
                    $result['act3'] = 0;
                    $result['action_time'] = $next_action_time;
                    $result['count'] = 0;

                }

                $result['act1'] = $result['act1'] + $query_data[$act1];
                $result['act2'] = $result['act2'] + $query_data[$act2];
                $result['act3'] = $result['act3'] + $query_data[$act3];
                $result['count'] = $result['count'] + 1;

            }
            array_push($result_arr, $result);

            //데이터 없을 경우 뒷부분 0으로 채워주기
            for ($i = sizeof($result_arr); $i < $last_count; $i++) {

                $next_action_time = date($format, strtotime($time_term, strtotime($result['action_time'])));

                $result['act1'] = 0;
                $result['act2'] = 0;
                $result['act3'] = 0;
                $result['action_time'] = $next_action_time;

                array_push($result_arr, $result);
            }

//            $data = [];

//            $data['act1'] = ['150','29'];
//            $data['act2'] = ['150','29'];
//            $data['act3'] = ['150','29'];

//            $data['act1'] = json_encode(array_column($result_arr,act1),JSON_NUMERIC_CHECK);
//            $data['act2'] = json_encode(array_column($result_arr,act2),JSON_NUMERIC_CHECK);
//            $data['act3'] = json_encode(array_column($result_arr,act3),JSON_NUMERIC_CHECK);
//
            $data['act1'] = array_column($result_arr, 'act1');
            $data['act2'] = array_column($result_arr, 'act2');
            $data['act3'] = array_column($result_arr, 'act3');


            return json_encode($data, JSON_NUMERIC_CHECK);
        } else {
            return null;
        }
//
//        $data['act1'] = json_encode($table_name, JSON_NUMERIC_CHECK);
//        $data['act2'] = json_encode($class, JSON_NUMERIC_CHECK);
//        $data['act3'] = json_encode($action_end_time, JSON_NUMERIC_CHECK);


    }

    function select_data_from_class_with_option($class_arr, $term_type, $action_time, $age_category, $gender_category, $obesity)
    {

        $age_query = "";
        $gender_query = "";
        $obesity_query = "";

        if ($age_category!=0){
            $age_start_date = null;
            $age_end_date = null;

            $age_query = " AND child_birthday BETWEEN ".$age_start_date." AND ".$age_end_date;
        }

        if ($gender_query!=0){
            $gender_query = " AND child_gender = ".$gender_category;
        }

        if ($obesity!=-1){
            $obesity_query = " AND child_obesity = ".$obesity;
        }

        $in_list = empty($class_arr) ? 'NULL' : "'" . join("','", $class_arr) . "'";
        $stmt = "SELECT child_seq FROM child WHERE preschool_class_seq IN (" . $in_list . ")".$age_query.$gender_query.$obesity_query;
        $result = $this->db->query($stmt)->result();

        if ($result){
            return $this->select_data_from_child(array_column($result, 'child_seq'), $term_type, $action_time);
        }else return null;

    }

    function select_data_from_child($child, $term_type, $action_time)
    {

        $action_time_column = 'action_date';

        switch ($term_type) {
            case 'day':
                $action_end_time = date("Y-m-d", strtotime("+1 days", strtotime($action_time)));
                $table_name = 'act_hour';
                $action_time_column = 'action_time';
                break;
            case 'week':
                $action_end_time = date("Y-m-d", strtotime("+7 days", strtotime($action_time)));
                $table_name = 'act_day';
                break;
            case 'month':
                $action_end_time = date("Y-m-t", strtotime($action_time));
                $table_name = 'act_day';
                break;
            case 'year':
                $action_end_time = date("Y-m-d", strtotime("+1 years", strtotime($action_time)));
                $table_name = 'act_month';
                break;
            default:
                return null;
        }

        if($child) {
            $in_list = empty($child) ? 'NULL' : "'" . join("','", $child) . "'";
            $stmt = "SELECT * FROM " . $table_name . " WHERE preschool_class_seq IN (" . $in_list . ") AND " . $action_time_column . " BETWEEN ? AND ? ORDER BY " . $action_time_column;
        }else{
            $stmt = "SELECT * FROM " . $table_name . " WHERE " . $action_time_column . " BETWEEN ? AND ? ORDER BY " . $action_time_column;
        }
        $query = $this->db->query($stmt, array($action_time, $action_end_time));


        $act1 = 'act1';
        $act2 = 'act2';
        $act3 = 'act3';


        if ($query) {

            $query_result_arr = $query->result_array();


            $result_arr = array();
            $time_term = "+1 hour";
            $format = "Y-m-d H:00:00";
            $last_count = 24;
            $start_action_time = $action_time;
            switch ($term_type) {
                case 'day':
                    $time_term = "+1 hour";
                    $format = "Y-m-d H:00:00";
                    $last_count = 24;
                    $start_action_time = $start_action_time . " 00:00:00";
                    break;
                case 'week':
                    $time_term = "+1 days";
                    $format = "Y-m-d";
                    $last_count = 7;
                    break;
                case 'month':
                    $time_term = "+1 days";
                    $format = "Y-m-d";
                    $last_count = date("t", strtotime($action_time));
                    break;
                case 'year':
                    $time_term = "+1 month";
                    $format = "Y-m-01";
                    $last_count = 12;
                    break;
            }

            $index = 0;
            $result = array();

            $result['act1'] = 0;
            $result['act2'] = 0;
            $result['act3'] = 0;
            $result['action_time'] = $start_action_time;
            $result['count'] = 0;
            foreach ($query_result_arr as $query_data) {

                while (($result['action_time'] != $query_data[$action_time_column]) && ($index < $last_count)) {

                    if ($result['count'] > 1) {

                        $result['act1'] = $result['act1'] / $result['count'];
                        $result['act2'] = $result['act2'] / $result['count'];
                        $result['act3'] = $result['act3'] / $result['count'];
                    }
                    array_push($result_arr, $result);

                    $next_action_time = date($format, strtotime($time_term, strtotime($result['action_time'])));
                    $index = $index + 1;


                    $result['act1'] = 0;
                    $result['act2'] = 0;
                    $result['act3'] = 0;
                    $result['action_time'] = $next_action_time;
                    $result['count'] = 0;

                }

                $result['act1'] = $result['act1'] + $query_data[$act1];
                $result['act2'] = $result['act2'] + $query_data[$act2];
                $result['act3'] = $result['act3'] + $query_data[$act3];
                $result['count'] = $result['count'] + 1;

            }
            array_push($result_arr, $result);

            //데이터 없을 경우 뒷부분 0으로 채워주기
            for ($i = sizeof($result_arr); $i < $last_count; $i++) {

                $next_action_time = date($format, strtotime($time_term, strtotime($result['action_time'])));

                $result['act1'] = 0;
                $result['act2'] = 0;
                $result['act3'] = 0;
                $result['action_time'] = $next_action_time;

                array_push($result_arr, $result);
            }

//            $data = [];

//            $data['act1'] = ['150','29'];
//            $data['act2'] = ['150','29'];
//            $data['act3'] = ['150','29'];

//            $data['act1'] = json_encode(array_column($result_arr,act1),JSON_NUMERIC_CHECK);
//            $data['act2'] = json_encode(array_column($result_arr,act2),JSON_NUMERIC_CHECK);
//            $data['act3'] = json_encode(array_column($result_arr,act3),JSON_NUMERIC_CHECK);
//
            $data['act1'] = array_column($result_arr, 'act1');
            $data['act2'] = array_column($result_arr, 'act2');
            $data['act3'] = array_column($result_arr, 'act3');


            return json_encode($data, JSON_NUMERIC_CHECK);
        } else {
            return null;
        }
//
//        $data['act1'] = json_encode($table_name, JSON_NUMERIC_CHECK);
//        $data['act2'] = json_encode($class, JSON_NUMERIC_CHECK);
//        $data['act3'] = json_encode($action_end_time, JSON_NUMERIC_CHECK);


    }

}