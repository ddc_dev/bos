<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Preschool_Model extends CI_Model {
    function __construct()
    {
        parent::__construct();
    }

    function getPreschoolList($city){

        $queryGetPreschoolList = 'SELECT a.*, count(b.preschool_class_seq) as class_count, count(d.child_seq) as child_count, c.user_name, c.user_mail, c.user_contact
                                    FROM
                                        (SELECT e.*, count(teacher_seq) as teacher_count FROM preschool as e
                                            LEFT JOIN teacher as f
                                            ON e.preschool_seq = f.preschool_seq
                                            GROUP BY e.preschool_seq
                                        ) a
                                    LEFT JOIN preschool_class as b
                                    ON b.preschool_seq = a.preschool_seq
                                    LEFT JOIN child as d
                                    ON d.preschool_seq = a.preschool_seq
                                    LEFT JOIN admin as c
                                    ON a.admin_seq = c.admin_seq
                                    WHERE a.city_code = ?
                                    GROUP BY a.preschool_seq';

        $executeGetPreschoolList = $this->db->query($queryGetPreschoolList, array($city));


        return $executeGetPreschoolList->result();
    }

    function getPreschoolListByVillage($villageCode){
        
        $queryGetPreschoolList = 'SELECT a.*, count(preschool_class_seq) as class_count, count(d.child_seq) as child_count, c.user_name, c.user_mail, c.user_contact
                                    FROM
                                        (SELECT e.*, count(teacher_seq) as teacher_count FROM preschool as e
                                            LEFT JOIN teacher as f
                                            ON e.preschool_seq = f.preschool_seq
                                            GROUP BY e.preschool_seq
                                        ) a
                                    LEFT JOIN preschool_class as b
                                    ON b.preschool_seq = a.preschool_seq
                                    LEFT JOIN child as d
                                    ON d.preschool_seq = a.preschool_seq
                                    LEFT JOIN admin as c
                                    ON a.admin_seq = c.admin_seq
                                    WHERE a.village_code = ? OR a.city_code = ?
                                    GROUP BY a.preschool_seq';

        $executeGetPreschoolList = $this->db->query($queryGetPreschoolList, array($villageCode, $villageCode));

        return $executeGetPreschoolList->result();
    }

    function registerTeacher(){

    }

    function updateTeacher(){

    }

    function deleteTeacher(){

    }
 
}