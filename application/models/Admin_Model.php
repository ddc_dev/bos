<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Admin_model extends CI_Model {
    function __construct() {
        parent::__construct();
    }

    function getAdminList($instType, $instSeq){

        $queryGetAdminList = 'SELECT * FROM admin WHERE inst_type = ? AND inst_seq = ?';

        $executeGetAdminList = $this->db->query($queryGetAdminList, array($instType, $instSeq))->result();

        return $executeGetAdminList;
    }
    
    function addSubAdmin($array){

        $teacherSeq = $array['teacherSeq'];

        $queryInsertSubAdmin = 'INSERT INTO admin (user_seq, user_contact, user_name, user_mail, inst_type, inst_seq, inst_name, admin_type)
                                    SELECT user_seq, user_contact, user_name, user_mail, 2 as inst_type, preschool_seq, preschool_name, false as approved
                                    FROM teacher
                                    WHERE teacher_seq = ?';

        $excuteInsertSubAdmin = $this->db->query($queryInsertSubAdmin, array($teacherSeq));

        if($this->db->affected_rows() > 0) {
            return 'success';
        } else {
            return 'failed';
        }
    }

    function deleteAdmin($array){
        $delArray = $array['userSeqArr'];

        $queryDeleteAdmin = 'DELETE FROM admin WHERE user_seq IN ?';

        $result = $this->db->query($queryDeleteAdmin, array($delArray));

        if($this->db->affected_rows() > 0) {
            return 'success';
        } else {
            return 'failed';
        }
    }

}