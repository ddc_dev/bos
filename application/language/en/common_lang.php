<?php
/**
 * Created by PhpStorm.
 * User: yoon
 * Date: 2018. 4. 5.
 * Time: PM 9:46
 */

$lang['confirm'] = "영어_생성되었습니다.";
$lang['delete'] = "영어_삭제되었습니다.";
$lang['update'] = "영어_업데이트 되었습니다.";

//menu
$lang['booking'] = "Booking";
$lang['booking_schedule'] = "Booking Schedule";
$lang['booking_list'] = "Booking List";
$lang['inventory'] = "Inventory";
$lang['inventory_adj'] = "Inventory adjustment";
$lang['property'] = "Property";
$lang['property(new)'] = "Property(NEW)";
$lang['owner'] = "Owner";
$lang['owner_report'] = "Owner Report";
$lang['revenue'] = "Revenue";
$lang['revenue_report'] = "Revenue Report";
$lang['statistics'] = "Statistics";
$lang['invoice'] = "Invoice";
$lang['admin'] = "Administrator";
$lang['admin_list'] = "Administrator List";

//----------property
$lang['property_new'] = "Create New property";
$lang['property_name'] = "Property Name";
$lang['property_type'] = "Property Type";
$lang['property_owner'] = "Owner";
$lang['property_plan'] = "Plan";
$lang['property_meter'] = "Square Meter(㎡)";
$lang['property_address'] = "Address";
$lang['property_postcode'] = "Postal Code";
$lang['property_telephone'] = "Telephone";
$lang['property_charge'] = "Charge Type";
$lang['property_ota_fee'] = "OTA Fee";
$lang['property_ota_hint'] = "If it is 'Not included', the amount is calculated excluding the OTA fee..";


