<?php
/**
 * Created by PhpStorm.
 * User: yoon
 * Date: 2018. 4. 5.
 * Time: PM 9:46
 */

$lang['confirm'] = "일본어_생성되었습니다.";
$lang['delete'] = "일본어_삭제되었습니다.";
$lang['update'] = "일본어_업데이트 되었습니다.";

$lang['booking'] = "予約";
$lang['booking_schedule'] = "予約スケジュール";
$lang['booking_list'] = "予約リスト";
$lang['inventory'] = "在庫";
$lang['inventory_adj'] = "在庫状況";
$lang['property'] = "物件";
$lang['property(new)'] = "物件(NEW)";
$lang['owner'] = "オーナー";
$lang['owner_report'] = "オーナーレポート";
$lang['revenue'] = "売上";
$lang['revenue_report'] = "売上レポート";
$lang['statistics'] = "通計";
$lang['invoice'] = "請求書";
$lang['admin'] = "管理者";
$lang['admin_list'] = "管理者リスト";

//----------property
$lang['property_new'] = "新物件の登録";
$lang['property_name'] = "プロパティ名称";
$lang['property_type'] = "運営形態";
$lang['property_owner'] = "オーナ";
$lang['property_plan'] = "運営プラン";
$lang['property_meter'] = "平米数(㎡)";
$lang['property_address'] = "Address";
$lang['property_postcode'] = "郵便番号";
$lang['property_telephone'] = "Telephone";
$lang['property_charge'] = "請求タイプ";
$lang['property_ota_fee'] = "OTA費用";
$lang['property_ota_hint'] = "’含まない’を選択するとOTA費用は自動で計算しません。";

