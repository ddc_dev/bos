<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Permission {
    function check() {
        $CI =& get_instance();
        $CI->load->library('session');
        $CI->load->helper('url');
                
        // $CI->allow 는 배열로, 추후 로그인 없이 사용 가능한 것들을 저장할때 사용합니다.
        // 가령 로그인 화면이나 문의화면 같이 비로그인으로 열어줄 것들을 넣어주게 됩니다.
        if (isset($CI->allow) && (is_array($CI->allow) === false OR in_array($CI->router->method, $CI->allow) === false)) {
            if (!$CI->session->userdata('id')) {            	
            	Alert2url('Please login', '/auth');
            }            

            //현재 페이지
            $current_page = uri_string();
            $current_page = $CI->uri->segment(1)."/".$CI->uri->segment(2);
            
            //내 권한 페이지 세션에서 가져오기            
            $permission = $CI->session->userdata("permission");            
            if(substr($permission , -1) == "|") $permission = substr($permission , 0, -1);
            
            // 배열로 만든다
            $arr_permission = explode("|",$permission);
            //권한 체크 하지 않는 페이지 하드코딩
            array_push($arr_permission , "dashboard/dashboard","auth/login" , "auth/login_action" , "auth/logout", "auth/language_action");

            //in_array로 체크
            if (!in_array($current_page , $arr_permission)) {
                Alert("You do not have permission. Please contact your administrator.");
            } 
        }
    }
}
?>