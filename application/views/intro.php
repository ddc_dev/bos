<div class="wrapper displayBlock" style="width: 100%;">
<!--콘텐츠-->
<div class="mainContentContainer" style="margin: 0 auto; max-width: 1100px;">
    <!--헤드텍스트-->
    <div class="headText displayBlock" style="text-align: center;">
        <img src="/files/img/logo.svg" style="width: 200px; height: 200px;"/>
    </div>
    <!--//헤드텍스트-->
    <!--데이터-->
    <div class="dateContainer">
        <!--row 1-->
        <ul>
            <!--1-->
            <div class="box loginSection box-1">
                <ul>
                    <li>
                        <input id="inputAdminMail" type="text" class="textBox" placeholder="메일주소를 입력해주세요."/>
                    </li>
                    <li>
                        <input id="inputAdminPW" type="password" class="textBox" placeholder="비밀번호를 입력해주세요."/>
                    </li>
                </ul>
                <!--비번찾기-->
                    <div class="findPasswordSection">
                    <a href="#">아이디 찾기</a>  / <a href="#">비밀번호 찾기</a>
                </div>
                <!--//비번찾기-->
                <!--버튼-->
                <div class="buttonBox">
                    <a href="#">
                        <div id="signIn" class="right">
                            로그인
                        </div>
                    </a>
                    <!--<a href="#">-->
                        <!--<div id="registerInst" class="right">-->
                            <!--기관 등록-->
                        <!--</div>-->
                    <!--</a>-->
                </div>
                <!--//버튼-->
                <div class="findPasswordSection" id="registerInst">
                  <a style="text-decoration: underline; font-weight: normal;">기관등록을 하지 않으셨나요?</a>
                </div>
            </div>
            <!--//1-->
            <!--2-->
        </ul>
        <!--//row 1-->
    </div>
    <!--//데이터-->
</div>
<!--//콘텐츠-->
</div>
<div id="registerPopup"></div>
