<div class="mainContentContainer">
    <!--헤드텍스트-->
    <div class="headText">
        <div class="left">
            기관 정보
            <a href="#">
                <img src="/files/css/images/setting_button.svg" style="margin-left: 15px;"/>
            </a>
        </div>
    </div>
    <!--//헤드텍스트-->
    <!--데이터-->
    <div class="centerInfoContainer">
        <div class="box box-1">
            <!--head-->
            <ul>
                <li class="head">
                    시도
                </li>
                <li class="head">
                    구군
                </li>
                <li class="head">
                    기관명
                </li>
                <li class="head">
                    구분
                </li>
                <li class="head">
                    기관장명
                </li>
                <li class="head">
                    아이디
                </li>
                <li class="head">
                    원생수
                </li>
                <li class="head">
                    기기수
                </li>
                <li class="head">
                    등록일
                </li>
                <li class="head">
                    인증일
                </li>
                <li class="head">
                    상태
                </li>
                <li class="head">
                    서비스
                </li>
            </ul>
            <!--head-->
            <!--1-->
            <ul>
                <li>
                    서울시
                </li>
                <li>
                    송파구
                </li>
                <li>
                    기관명
                </li>
                <li>
                    유치원 이름
                </li>
                <li>
                    이름
                </li>
                <li>
                    abcd1234
                </li>
                <li>
                    14
                </li>
                <li>
                    14
                </li>
                <li>
                    2019.03.01
                </li>
                <li>
                    2019.03.01
                </li>
                <li>
                    완료
                </li>
                <li>
                    -
                </li>
            </ul>
            <!--//1-->
            <!--2-->
            <ul>
                <li>
                    서울시
                </li>
                <li>
                    송파구
                </li>
                <li>
                    기관명
                </li>
                <li>
                    유치원 이름
                </li>
                <li>
                    이름
                </li>
                <li>
                    abcd1234
                </li>
                <li>
                    14
                </li>
                <li>
                    14
                </li>
                <li>
                    2019.03.01
                </li>
                <li>
                    2019.03.01
                </li>
                <li>
                    완료
                </li>
                <li>
                    -
                </li>
            </ul>
            <!--//2-->
            <!--3-->
            <ul>
                <li>
                    서울시
                </li>
                <li>
                    송파구
                </li>
                <li>
                    기관명
                </li>
                <li>
                    유치원 이름
                </li>
                <li>
                    이름
                </li>
                <li>
                    abcd1234
                </li>
                <li>
                    14
                </li>
                <li>
                    14
                </li>
                <li>
                    2019.03.01
                </li>
                <li>
                    2019.03.01
                </li>
                <li>
                    완료
                </li>
                <li>
                    -
                </li>
            </ul>
            <!--//3-->
            <!--4-->
            <ul>
                <li>
                    서울시
                </li>
                <li>
                    송파구
                </li>
                <li>
                    기관명
                </li>
                <li>
                    유치원 이름
                </li>
                <li>
                    이름
                </li>
                <li>
                    abcd1234
                </li>
                <li>
                    14
                </li>
                <li>
                    14
                </li>
                <li>
                    2019.03.01
                </li>
                <li>
                    2019.03.01
                </li>
                <li>
                    요청
                </li>
                <li>
                    -
                </li>
            </ul>
            <!--//4-->
            <!--5-->
            <ul>
                <li>
                    서울시
                </li>
                <li>
                    송파구
                </li>
                <li>
                    기관명
                </li>
                <li>
                    유치원 이름
                </li>
                <li>
                    이름
                </li>
                <li>
                    abcd1234
                </li>
                <li>
                    14
                </li>
                <li>
                    14
                </li>
                <li>
                    2019.03.01
                </li>
                <li>
                    2019.03.01
                </li>
                <li>
                    완료
                </li>
                <li>
                    -
                </li>
            </ul>
            <!--//5-->
            <!--페이지-->
            <div class="pagination">
                <a href="#">이전페이지</a>&nbsp;&nbsp;&nbsp;1 . 2 . 3 . 4 ... 12&nbsp;&nbsp;&nbsp;<a href="#">다음페이지</a>
            </div>
            <!--//페이지-->
        </div>
    </div>
    <!--//데이터-->
</div>
<!--//콘텐츠-->
<!--팝업-->
<div class="popup">
    <div class="box">
        <!--1-->
        <div class="optionList">
            <h1>기관 정보 step.2</h1>
        </div>
        <!--//1-->
        <!--2-->
        <div class="optionList marginTop30">
            <div class="">
                <input type="text" class="textBox marginTop15" placeholder="아이디 (영문 숫자 조합 10자리 이내)">
                <input type="text" class="textBox borderTopNone" placeholder="비밀번호 (영문, 숫자, 조합 6자리 이상)">
                <input type="text" class="textBox borderTopNone" placeholder="이메일 주소 입력">
                <!--승인 여부-->
                <div class="optionList marginTop30">
                    <h1>승인 여부</h1>
                    <div class="checkBoxSection">
                        <input class="checkBox" type="radio">&nbsp;&nbsp;승인 확인&nbsp;&nbsp;&nbsp;&nbsp;
                        <input class="checkBox" type="radio">&nbsp;&nbsp;승인 거절
                    </div>
                    <div class="divider"></div>
                    <div class="checkBoxSection">
                        <select class="textBox marginTop15">
                            <option>서류 미비</option>
                        </select>
                    </div>
                    <div class="divider"></div>
                    <div class="checkBoxSection marginTop15">
                        2017.10.30 승인 됨 / 2017.07.30 등록 됨
                    </div>
                </div>
                <!--//승인여부-->
            </div>
        </div>
        <!--//2-->
        <!--버튼-->
        <div class="buttonBox marginTop0">
            <!--왼쪽 버튼-->
            <a href="#">
                <div class="left">
                    서비스 정지
                </div>
            </a>
            <!--//왼쪽 버튼-->
            <!--왼쪽 버튼-->
            <a href="#">
                <div class="left">
                    닫기
                </div>
            </a>
            <!--//왼쪽 버튼-->
            <!--오른쪽 버튼-->
            <a href="#">
                <div class="right">
                    등록
                </div>
            </a>
            <!--//오른쪽 버튼-->
        </div>
        <!--//버튼-->
    </div>
</div>
<!--//팝업-->