<div class="box">
    <!--1-->
    <div class="optionList">
        <h1>디디씨 서비스 이용 약관 동의</h1>
    </div>
    <!--//1-->
    <!--2-->
    <div class="optionList marginTop30">
        <h1>1. 디디씨 서비스 이용에 관한 사항</h1>
        <div class="">
            <textarea class="agreementTextBox">
당사 및 당사 업무 수탁자가 제공하는 아이디엘 관련 제반 서비스의 이용과 관련하여 회사와 사용자간의 권리, 의무 및 책임사항, 기타 필요한 사항을 규정하고자 합니다. 이에 대하여 동의하십니까?
            </textarea>
            <div class="checkBoxSection">
                <input class="checkBox" name="agreeCheck" type="checkbox"> 아이디엘 서비스 이용 약관 사항에 동의합니다.
            </div>
        </div>
    </div>
    <!--//2-->
    <!--divider-->
    <div class="divider"></div>
    <!--//divider-->
    <!--3-->
    <div class="optionList marginTop30">
        <h1>개인(신용)정보 처리 동의서 [기관정보관리, 서비스이용]</h1>
    </div>
    <!--//3-->
    <!--4-->
    <div class="optionList marginTop30">
        <h1>1. 개인(신용)정보의 수집·이용에 관한 사항</h1>
        <div class="">
                    <textarea class="agreementTextBox">
당사 및 당사 업무 수탁자가 제공하는 아이디엘 관련 제반 서비스의 이용과 관련하여 회사와 사용자간의 권리, 의무 및 책임사항, 기타 필요한 사항을 규정하고자 합니다. 이에 대하여 동의하십니까?
                    </textarea>
            <div class="checkBoxSection">
                <input class="checkBox" name="agreeCheck" type="checkbox"> 개인(신용)정보의 수집·이용에 관한 사항에 동의합니다.
            </div>
        </div>
    </div>
    <!--//4-->
    <!--5-->
    <div class="optionList marginTop30 marginBottom15">
        <h1>2. 개인(신용)정보의 조회에 관한 사항</h1>
        <div class="">
                    <textarea class="agreementTextBox">
당사 및 당사 업무 수탁자가 제공하는 아이디엘 관련 제반 서비스의 이용과 관련하여 회사와 사용자간의 권리, 의무 및 책임사항, 기타 필요한 사항을 규정하고자 합니다. 이에 대하여 동의하십니까?
                    </textarea>
            <div class="checkBoxSection">
                <input class="checkBox" name="agreeCheck" type="checkbox"> 개인(신용)정보의 조회에 관한 사항에 동의합니다.
            </div>
        </div>
    </div>
    <!--//5-->
    <!--버튼-->
    <div class="buttonBox marginTop0">
        <!--왼쪽 버튼-->
        <a href="#">
            <div class="left cancelPopup">
                닫기
            </div>
        </a>
        <!--//왼쪽 버튼-->
        <!--오른쪽 버튼-->
        <a href="#">
            <div id="agreeAll" class="right">
                전체 동의
            </div>
        </a>
        <!--//오른쪽 버튼-->
    </div>
    <!--//버튼-->
</div>