<div class="box">
    <!--1-->
    <div class="optionList">
        <h1>기관 등록</h1>
    </div>
    <!--//1-->
    <!--2-->
    <div class="optionList marginTop30">
        <h1>기관 유형</h1>
        <div class="">
            <select id="instType" class="textBox marginTop15">
                <option value="select">유형</option>
                <option value="office">기관</option>
                <option value="preschool">유치원 / 어린이집</option>
            </select>
            <input id="instName" type="text" class="textBox marginTop15" placeholder="기관명(○○센터, ○○○유치원)">
            <input id="instContact" type="text" class="textBox marginTop15" placeholder="기관 전화번호 ('-' 제외)">
        </div>
    </div>
    <div class="divider"></div>
    <div class="optionList marginTop30">
        <h1>지역</h1>
        <div class="">
            <select id="province" class="textBox marginTop15">
                <option value="selected">시/도</option>
                <?php foreach ((array)$province as $list) { ?>
                    <option value="<?=$list->province_code;?>"><?=$list->province_name;?></option>
                <?php } ?>
            </select>
            <select id="city" class="textBox marginTop15">
                <option value="select">시/군/구</option>
            </select>
            <select id="village" class="textBox marginTop15">
                <option value="select">읍/면/동</option>
            </select>
        </div>
    </div>
    <!--//2-->
    <!--버튼-->
    <div class="buttonBox marginTop15">
        <!--왼쪽 버튼-->
        <a href="#">
            <div class="left" id="cancelFormInst">
                이전
            </div>
        </a>
        <!--//왼쪽 버튼-->
        <!--오른쪽 버튼-->
        <a href="#">
            <div id="toStep2" class="right">
                다음
            </div>
        </a>
        <!--//오른쪽 버튼-->
    </div>
    <!--//버튼-->
</div>