<div class="box">
    <!--1-->
    <div class="optionList">
        <h1>관리자 등록</h1>
    </div>
    <!--//1-->
    <!--2-->
    <div class="optionList marginTop15">
        <div class="">
            <input id="adminName" type="text" class="textBox marginTop15" placeholder="관리자 이름">
            <input id="adminMail" type="text" class="textBox borderTopNone" placeholder="관리자 이메일">
            <input id="adminPW" type="password" class="textBox borderTopNone" placeholder="비밀번호">
            <input id="adminPWCheck" type="password" class="textBox borderTopNone" placeholder="비밀번호 확인">
            <input id="adminContact" type="text" class="textBox borderTopNone" placeholder="관리자 휴대폰 번호(숫자만 입력)">
            <a href="#">
                <div id="SendSMSForRegister" class="buttonFull marginTop15">인증번호 전송</div>
            </a>
            <input style="display: inline-block;" id="adminContactCertificate" type="text"
                   class="textBox marginTop15 width50" placeholder="인증번호 6자리">
            <input style="display: inline-block; width: 15%" id="confirmTimer" type="text" class="textBox marginTop15" readonly>


            <a href="#">
                <div id="confirmContactNumber" class="buttonFull marginTop15">확인</div>
            </a>
        </div>
    </div>
    <!--//2-->
    <!--버튼-->
    <div class="buttonBox marginTop30">
        <!--왼쪽 버튼-->
        <a href="#">
            <div class="left" id="cancelFormAdmin">
                이전
            </div>
        </a>
        <!--//왼쪽 버튼-->
        <!--오른쪽 버튼-->
        <a href="#">
            <div id="toStep3" class="right">
                완료
            </div>
        </a>
        <!--//오른쪽 버튼-->
    </div>
    <!--//버튼-->
</div>