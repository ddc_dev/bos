<!--콘텐츠-->
<div class="mainContentContainer">
    <!--헤드텍스트-->
    <div class="headText">
        <div class="left displayBlock">
            <div class="displayFlex">
                <?php
                $noticeType = '';
                if(!isset($this->uri->segments[2])) {
                    $noticeType = 'all';
                } else {
                    $noticeType = $this->uri->segments[2];
                }

                if ($noticeType == 'all') {
                    echo '공지사항';
                } else if($noticeType == 'general') {
                    echo '공지사항';
                } else if($noticeType == 'monthlyMenu') {
                    echo '월간 식단';
                } else if($noticeType == 'recipe') {
                    echo '표준 레시피';
                } else if($noticeType == 'telegram') {
                    echo '가정통신문';
                }
                ?>
            </div>
        </div>
        <div class="right">
            <div class="childrenListSortingBox displayFlex alignItemsCenter marginTop15">
                <!--right-->
                <div>
                    <!--유치원명-->
<!--                    <input type="text" class="textBox" placeholder="검색"/>-->
                    <!--//유치원명-->
                </div>
                <!--//right-->
            </div>
        </div>
    </div>
    <!--//헤드텍스트-->
    <!--데이터-->
    <div class="centerInfoContainer">
        <div class="box box-1">
            <div class="displayFlex marginBottom20">
                <div class="width30 displayFlex alignItemsCenter">
                </div>
                <!--버튼-->
                <div class="buttonBox width70">
                    <a href="register">
                        <?php
                        if($_SESSION['instType'] == 1) {
                            echo ' <div class="right">공지사항 등록</div>';
                        }
                        ?>

                    </a>
                </div>
                <!--//버튼-->
            </div>
            <!--head-->
            <ul>
                <li class="head width10 flexBasisAuto">번호</li>
                <li class="head width10 flexBasisAuto">분류</li>
                <li class="head width40 flexBasisAuto">제목</li>
                <li class="head width15 flexBasisAuto">작성자</li>
                <li class="head width15 flexBasisAuto">작성 날짜</li>
                <li class="head width10 flexBasisAuto">첨부</li>
            </ul>
            <!--head-->
            <?php
            if(!isset($this->uri->segments[3])) {
                $currentPage = 1;
            } else {
                $currentPage = $this->uri->segments[3];
            }

            foreach($fixedList as $fList){
                $datetime = explode(' ', $fList->notice_created_time);
                $date = $datetime[0];
                $time = $datetime[1];
                if ($date == Date('Y-m-d')) {
                    $mark_datetime = $time;
                } else {
                    $mark_datetime = $date;
                }
                switch ($fList->notice_type) {
                    case 1:
                        $type = '공지사항';
                        break;
                    case 2:
                        $type = '월간식단';
                        break;
                    case 3:
                        $type = '표준레시피';
                        break;
                    case 4:
                        $type = '가정통신문';
                        break;
                }
                ?>
                <ul class="fixedList">
                    <li class="width10 flexBasisAuto"><span><i class="fas fa-info-circle"></i></span></li>
                    <li class="width10 flexBasisAuto"><span><?=$type?></span></li>
                    <li class="width40 flexBasisAuto"><span><a href="/notice/content/<?=$fList->notice_seq?>"><?=$fList->title?></a></span></li>
                    <li class="width15 flexBasisAuto"><span><?=$fList->user_name?></span></li>
                    <li class="width15 flexBasisAuto"><span><?=$mark_datetime?></span></li>
                    <li class="width10 flexBasisAuto">
                        <?php
                        if(isset($fList->file_list)){
                            echo '<i class="fas fa-paperclip"></i>';
                        } else {
                            echo '-';
                        }
                        ?>
                    </li>
                </ul>
                <?php
            }

            $index = 0;
            if ($currentPage == 1) {
                $currentPage = 0;
            }
            foreach($noticeList as $list){
                $datetime = explode(' ', $list->notice_created_time);
                $date = $datetime[0];
                $time = $datetime[1];
                if ($date == Date('Y-m-d')) {
                    $mark_datetime = $time;
                } else {
                    $mark_datetime = $date;
                }
                switch ($list->notice_type) {
                    case 1:
                        $type = '공지사항';
                        break;
                    case 2:
                        $type = '월간식단';
                        break;
                    case 3:
                        $type = '표준레시피';
                        break;
                    case 4:
                        $type = '가정통신문';
                        break;
                }
                ?>
                <ul>
                    <li class="width10 flexBasisAuto"><span><?=$totalRow - (($currentPage)-$index)?></span></li>
                    <li class="width10 flexBasisAuto"><span><?=$type?></span></li>
                    <li class="width40 flexBasisAuto"><span><a href="/notice/content/<?=$list->notice_seq?>"><?=$list->title?></a></span></li>
                    <li class="width15 flexBasisAuto"><span><?=$list->user_name?></span></li>
                    <li class="width15 flexBasisAuto"><span><?=$mark_datetime?></span></li>
                    <li class="width10 flexBasisAuto">
                        <?php
                        if(isset($list->file_list)){
                            echo '<i class="fas fa-paperclip"></i>';
                        } else {
                            echo '-';
                        }
                        ?>
                    </li>
                </ul>
            <?php
                $index --;
            }

            echo $pagination;
            ?>
        </div>
    </div>
    <!--//데이터-->
</div>
<!--//콘텐츠-->

<!--//콘텐츠-->
<div id="addPopup"></div>