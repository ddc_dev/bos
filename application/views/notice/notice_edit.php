<div class="mainContentContainer">
    <!--헤드텍스트-->
    <div class="headText">
        <div class="left displayBlock">
            <div class="displayFlex">
                공지사항 등록
            </div>
        </div>
    </div>
    <!--//헤드텍스트-->
    <!--데이터-->
    <div class="centerInfoContainer">
        <div class="box box-1">
            <div class="displayFlex marginBottom20">
                <div class="width50 displayFlex alignItemsCenter marginRight30">
                    <select id="noticeType" class="textBox marginTop30">
                        <option value="0">분류 선택</option>
                        <option value="1">공지사항</option>
                        <option value="2">월간 식단</option>
                        <option value="3">표준 레시피</option>
                        <option value="4">가정 통신문</option>
                    </select>
                </div>
                <div class="width50 displayFlex alignItemsCenter">
                    <div class="registerCheckBoxSection">
                        <input class="registerCheckBox" name="fix" type="checkbox"> 상단고정
                    </div>
                </div>
            </div>
            <div class="displayFlex marginBottom20">
                <div class="width50 displayFlex alignItemsCenter">
                    <input type="text" id="noticeTitle" class="textBox marginTop15" placeholder="제목">
                </div>
            </div>
            <div class="summerNote">
                <textarea id="summernote"></textarea>
            </div>
            <div class="displayFlex alignItemsCenter registerButtonBox marginTop30">
                <div class="buttonAdd marginTop30">
                    <input name="noticeFile" type="file" id="noticeFile" class="inputFile" hidden>
                    <label for="noticeFile">파일 첨부</label>
                </div>
            </div>

            <div id="fileShow" class="marginTop30"></div>
            <div class="buttonCenter buttonBox marginTop30">
                <!--왼쪽 버튼-->
                <a href="#">
                    <div class="left" id="backNoticeList">
                        취소
                    </div>
                </a>
                <!--//왼쪽 버튼-->
                <!--오른쪽 버튼-->
                <a id="submitNotice">
                    <div class="right">
                        등록
                    </div>
                </a>
                <!--//오른쪽 버튼-->
                <input type="hidden" id="userSeq" value="<?=$_SESSION['user_seq']?>">
                <input type="hidden" id="userName" value="<?=$_SESSION['user_name']?>">
                <input type="hidden" id="instType" value="<?=$_SESSION['instType']?>">
                <input type="hidden" id="instSeq" value="<?=$_SESSION['instSeq']?>">
            </div>
        </div>
    </div>
    <!--//데이터-->
</div>