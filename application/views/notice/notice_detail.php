<div class="mainContentContainer">
    <!--헤드텍스트-->
    <div class="headText">
        <div class="left displayBlock">
            <div class="displayFlex">
                공지사항
            </div>
        </div>
    </div>
    <!--//헤드텍스트-->
    <!--데이터-->
    <div class="centerInfoContainer">
        <div class="box box-1">
            <div class="displayFlex marginBottom20">
                <div class="width50 displayFlex alignItemsCenter">
                    <h1 style="font-size: 18px; text-align: left">
                        <?=$noticeContent[0]->title?>
                    </h1>
                </div>
                <div class="buttonBox width70">
                    <?php
                    if($noticeContent[0]->user_seq == $_SESSION['userSeq']){
                        ?>
                        <a id="modifyNotice">
                            <div class="left">
                                수정
                            </div>
                        </a>
                        <a id="deleteNotice">
                            <div class="left">
                                삭제
                            </div>
                        </a>
                    <?php
                    }
                    ?>
                </div>
            </div>
            <div class="divider marginTop0 marginBottom20"></div>
            <div class="displayFlex">
                <div class="width50 alignItemsCenter">
                    <h6 style="font-size: 12px; text-align: left">
                        작성자 : <?=$noticeContent[0]->user_name?>
                    </h6>
                </div>
                <div class="width50 marginRight30 alignItemsCenter">
                    <h1 style="font-size: 12px; text-align: right">
                        작성일 : <?=$noticeContent[0]->notice_created_time?>
                    </h1>
                </div>
            </div>
            <div class="divider marginTop15"></div>
            <!--1-->
            <ul>
                <li class="justifyContentFlexStart fontSize16">
                    <div class="noticeContent">
                        <?=$noticeContent[0]->content?>
                    </div>
                </li>
            </ul>
            <!--//1-->
            <!--2-->
            <ul class="borderBottomNone marginBottom30 displayBlock justifyContentFlexStart">
                <?php
                if (isset($noticeContent[0]->file_list)) {
                    $fileInfo = json_decode($noticeContent[0]->file_list);
                    ?>
                    <li>
                        <a href="/notice/download/<?= $fileInfo->path . '/' . $fileInfo->origin ?>"><?= $fileInfo->origin ?></a>
                    </li>
                    <?php
                }
                ?>
            </ul>
            <!--//2-->
            <div class="buttonBox width100">
                <a id="backNoticeList">
                    <div class="left marginRight0">
                        목록
                    </div>
                </a>
            </div>
        </div>
    </div>
    <!--//데이터-->
</div>
<input type="hidden" id="noticeSeq" value="<?=$noticeContent[0]->notice_seq?>">
<input type="hidden" id="userSeq" value="<?=$_SESSION['userSeq']?>">
<div id="addPopup"></div>
<!--//콘텐츠-->