<div class="mainContentContainer">
    <!--헤드텍스트-->
    <div class="headText">
        <div class="left displayBlock">
            <div class="displayFlex">
                공지사항 등록
            </div>
        </div>
    </div>
    <!--//헤드텍스트-->
    <!--데이터-->
    <div class="centerInfoContainer">
        <div class="box box-1">
            <div class="displayFlex marginBottom20">
                <div class="width50 displayFlex alignItemsCenter marginRight30">
                    <select id="noticeType" class="textBox marginTop30" disabled>
                        <?php
                        switch ($noticeContent[0]->notice_type) {
                            case 1:
                                $typeText = "공지사항";
                            case 2:
                                $typeText = "월간 식단";
                            case 3:
                                $typeText = "표준 레시피";
                            case 4:
                                $typeText = "가정 통신문";
                        }

                        echo '<option>'.$typeText.'</option>';
                        ?>
                    </select>
                </div>
                <div class="width50 displayFlex alignItemsCenter">
                    <div class="registerCheckBoxSection">
                        <?php
                        if ($noticeContent[0]->fix == 1){
                            $checked = 'checked';
                        } else {
                            $checked = '';
                        }
                        ?>
                        <input class="registerCheckBox" name="fix" type="checkbox"<?=$checked?>> &nbsp;상단고정
                    </div>
                </div>
            </div>
            <div class="displayFlex marginBottom20">
                <div class="width50 displayFlex alignItemsCenter">
                    <input type="text" id="noticeTitle" class="textBox marginTop15" placeholder="제목" value="<?=$noticeContent[0]->title?>">
                </div>
            </div>
            <div class="summerNote">
                <textarea id="summernote"><?=$noticeContent[0]->content?></textarea>
            </div>
            <div class="displayFlex alignItemsCenter registerButtonBox marginTop30">
                <div class="buttonAdd marginTop30">
                    <input name="noticeFile" type="file" id="noticeFile" class="inputFile" hidden>
                    <label for="noticeFile">파일 첨부</label>
                </div>
            </div>

            <div id="fileShow" class="marginTop30">
                <?php
                if (isset($noticeContent[0]->file_list)) {
                    $fileInfo = json_decode($noticeContent[0]->file_list);
                    echo '<p>'.$fileInfo->origin.' &nbsp;&nbsp;&nbsp;&nbsp;<a class="delFile"><i class="fas fa-times"></i></a></p>';
                }
                ?>
            </div>
            <div class="buttonCenter buttonBox marginTop30">
                <!--왼쪽 버튼-->
                <a href="#">
                    <div class="left" id="backNoticeList">
                        취소
                    </div>
                </a>
                <!--//왼쪽 버튼-->
                <!--오른쪽 버튼-->
                <a id="submitModiefiedNotice">
                    <div class="right">
                        수정
                    </div>
                </a>
                <!--//오른쪽 버튼-->
                <input type="hidden" id="noticeSeq" value="<?=$noticeContent[0]->notice_seq?>">
            </div>
        </div>
    </div>
    <!--//데이터-->
</div>