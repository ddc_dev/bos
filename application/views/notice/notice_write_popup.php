<div class="box">
    <!--1-->
    <div class="optionList">
        <h1>공지사항 등록</h1>
    </div>
    <!--//1-->
    <!--2-->
    <div class="optionList marginTop15">
        <div class="">
            <div class="">
                <input type="text" id="noticeTitle" class="textBox marginTop15" placeholder="제목">
                <select id="noticeType" class="textBox marginTop30">
                    <option value="0">분류 선택</option>
                    <option value="1">공지사항</option>
                    <option value="2">월간 식단</option>
                    <option value="3">표준 레시피</option>
                    <option value="4">가정 통신문</option>
                </select>
                <div class="checkBoxSection">
                    <input class="checkBox" name="fix" type="checkbox"> 상단고정
                </div>
                <div id="summernote"></div>
<!--                <textarea name="ir1" id="ir1" rows="10" cols="100"></textarea>-->
            </div>
            <div class="buttonFull marginTop15 buttonPaddingVertical0">
                <input name="noticeFile" type="file" id="noticeFile" class="inputFile">
                <label for="noticeFile">파일 첨부</label>
            </div>
            <div id="fileShow" class="marginTop30">
            </div>
        </div>
    </div>
    <!--//2-->
    <!--버튼-->
    <div class="buttonBox marginTop30">
        <!--왼쪽 버튼-->
        <a href="#">
            <div class="left cancelPopup">
                닫기
            </div>
        </a>
        <!--//왼쪽 버튼-->
        <!--오른쪽 버튼-->
        <a id="submitNotice">
            <div class="right">
                등록
            </div>
        </a>
        <!--//오른쪽 버튼-->
        <input type="hidden" id="userSeq" value="<?=$_SESSION['user_seq']?>">
        <input type="hidden" id="userName" value="<?=$_SESSION['user_name']?>">
        <input type="hidden" id="instType" value="<?=$_SESSION['instType']?>">
        <input type="hidden" id="instSeq" value="<?=$_SESSION['instSeq']?>">
    </div>
    <!--//버튼-->
</div>

<script>
    $('#summernote').summernote({
        lang: 'ko-KR',
        tabsize: 1,
        height: 180,
        fontNames: ['Nanum Gothic', 'Gothic A1', 'Noto Serif KR', 'NanumMyeongjo', 'Stylish'],
        fontNamesIgnoreCheck: ['Nanum Gothic', 'Gothic A1', 'Noto Serif KR', 'NanumMyeongjo', 'Stylish'],
        fontSizes: ['12', '13', '14', '15', '16', '17', '18'],
        toolbar: [
            ['font', ['bold', 'italic', 'underline']],
            ['para', ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull']],
            ['fontname', ['fontname']],
            ['fontsize', ['fontsize']],
        ],
    });

    $('.note-editable').css('text-align', 'inherit');


    // var oEditors = [];
    // nhn.husky.EZCreator.createInIFrame({
    //     oAppRef: oEditors,
    //     elPlaceHolder: "ir1",
    //     sSkinURI: "/files/js/smart_editor/SmartEditor2Skin.html",
    //     fCreator: "createSEditor2"
    // });
</script>