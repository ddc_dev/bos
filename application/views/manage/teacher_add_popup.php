<div class="box">
    <!--1-->
    <div class="optionList">
        <h1>선생님 추가</h1>
    </div>
    <!--//1-->
    <!--2-->
    <div class="optionList marginTop30">
        <div class="">
            <input id="teacherName" type="text" class="textBox marginTop15" placeholder="선생님 이름">
            <input id="teacherMail" type="text" class="textBox marginTop15" placeholder="선생님 메일주소(ex: example@ddcares.com)">
            <input id="teacherBD" type="text" class="textBox marginTop15" placeholder="선생님 생년월일(ex: 150417)">
            <input id="teacherContact" type="text" class="textBox marginTop15" placeholder="선생님 휴대폰 번호 (숫자만 입력) (ex:01012341234)">
            <input id="preschoolSeq" type="hidden" value="<?=$_SESSION['preschoolSeq']?>">
        </div>
    </div>
    <div class="marginTop30">
        <p>초기 비밀번호는 생년월일로 설정됩니다.</p>
    </div>
    <!--//2-->
    <!--버튼-->
    <div class="buttonBox marginTop30">
        <!--왼쪽 버튼-->
        <a href="#">
            <div class="left cancelPopup">
                닫기
            </div>
        </a>
        <!--//왼쪽 버튼-->
        <!--오른쪽 버튼-->
        <a href="#">
            <div class="right" id="submitTeacher">
                등록
            </div>
        </a>
        <!--//오른쪽 버튼-->
    </div>
    <!--//버튼-->
    <input type="hidden" id="preschoolName" value="<?=$_SESSION['preschoolName']?>">
</div>