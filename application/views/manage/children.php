<!--콘텐츠-->
<div class="mainContentContainer">
    <!--헤드텍스트-->
    <div class="headText">
        <div class="left displayBlock">
            <div class="displayFlex">
                원아 관리
            </div>
        </div>
        <div class="right">
            <div class="childrenListSortingBox displayFlex alignItemsCenter marginTop15">
                <!--left-->
                <div class="left box-1">
                    <!--반-->
                    <select id="selectPreschoolClass" class="selectBox">
                        <option value="all">전체</option>
                        <?php
                        foreach($classList as $list){
                            echo '<option value="'.$list->preschool_class_seq.'">'.$list->preschool_class_name.'</option>';
                        }
                        ?>
                        <option value="0">소속없음</option>
                    </select>
                    <!--//반-->
                </div>
                <!--//left-->
            </div>
        </div>
    </div>
    <!--//헤드텍스트-->
    <!--데이터-->
    <div class="centerInfoContainer">
        <div class="box box-1">
            <div class="displayFlex marginBottom20">
                <div class="width30 displayFlex alignItemsCenter">
                    <h1 style="font-size: 18px; text-align: left">
                        원아 리스트
                    </h1>
                </div>
            </div>
            <div class="displayFlex marginBottom20">
                <!--버튼-->
                <div class="width100 buttonBox">
                    <a id="addChild">
                        <div class="left">
                            개별 등록
                        </div>
                    </a>
                    <a id="addExcel">
                        <div class="left">
                            일괄 등록
                        </div>
                    </a>
                    <a id="submitSelectedChild">
                        <div class="left">
                            선택 승인 요청
                        </div>
                    </a>
                    <a id="delSelectedChild">
                        <div class="left">
                            선택 삭제
                        </div>
                    </a>
                    <a id="downloadChildExcelLink">
                        <div id="downloadChildExcel" class="left">
                            엑셀 내보내기
                        </div>
                    </a>
                    <a id="sendSMS">
                        <div  class="left marginRight0">
                            문자 보내기
                        </div>
                    </a>
                </div>
                <!--//버튼-->
            </div>
            <!--head-->
            <ul>
                <li class="head width5"><input type="checkbox" name="headCheckBox" class="checkBox"/></li>
                <li class="head width10">이름</li>
                <li class="head width10">반</li>
                <li class="head width40">학부모 이메일</li>
                <li class="head width15">학부모 전화번호</li>
                <li class="head width10">접근 권한</li>
            </ul>
            <!--head-->
            <?php
            foreach($childrenList as $list){
                ?>
                <ul>
                    <li class="width5"><input type="checkbox"  value="<?=$list->child_seq?>" name="bodyCheckBox" class="checkBox"></li>
                    <li class="width10"><span><?=($list->bos_name)==null? $list->child_name : $list->bos_name?></span></li>
                    <li class="width10"><span><?=$list->preschool_class_name?></span></li>
                    <li class="width40"><span><?=$list->user_mail?></span></li>
                    <li class="width15"><span><?=splitContact($list->guardian_contact)?></span></li>
                    <?php
                    if($list->activated == 1){
                        echo '<li class="width10 pointColor3">승인 대기 중</li>';
                    } else if($list->activated == 4){
                        echo '<li class="width10 pointColor1">승인 완료</li>';
                    } else if($list->activated == 3){
                        echo '<li class="width10 pointColor3">연동 요청 중</li>';
                    }else if($list->activated == 2){
                        echo '<li class="width10 pointColor3">연동 요청 필요</li>';
                    }
                    ?>
                    </li>
                </ul>
            <?php
            }
            echo $pagination;
            ?>
            <!--페이지-->
<!--            <div class="pagination">-->
<!--                <a href="#">이전페이지</a>-->
<!--                <a>&nbsp;1&nbsp;</a>-->
<!--                <a>&nbsp;2&nbsp;</a>-->
<!--                <a>&nbsp;3&nbsp;</a>-->
<!--                <a>&nbsp;4&nbsp;</a>-->
<!--                <a>&nbsp;5&nbsp;</a>-->
<!--                <a href="#">다음페이지</a>-->
<!--            </div>-->
            <!--//페이지-->
        </div>
    </div>
    <!--//데이터-->
</div>
<!--//콘텐츠-->
<div id="addPopup"></div>
<div id="addPopupExcel"></div>