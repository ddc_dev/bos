<!--팝업-->
<div class="box">
    <!--1-->
    <div class="optionList">
        <h1>담당 학급 관리</h1>
    </div>
    <!--//1-->
    <!--2-->
    <div class="optionList marginTop30">
        <div class="">
            <select id="classList" class="textBox marginTop30">
                <option value="selected">학급 선택</option>
                <?php

//                echo '<option value="'.$classList[$classListIndex]->preschool_class_seq.'" selected>'.$classList[$classListIndex]->preschool_class_name.'</option>';
                foreach($classList as $list) {
                    if ($list->preschool_class_seq == $teacherInfo[0]->preschool_class_seq) {
                        echo '<option value="'.$list->preschool_class_seq.'" selected>'.$list->preschool_class_name.'</option>';
                    } else {
                        echo '<option value="'.$list->preschool_class_seq.'">' . $list->preschool_class_name . '</option>';
                    }
                }
                ?>
            </select>
        </div>
    </div>
    <!--//2-->
    <!--버튼-->
    <div class="buttonBox marginTop30">
        <!--왼쪽 버튼-->
        <a href="#">
            <div class="left cancelPopup">
                취소
            </div>
        </a>
        <!--//왼쪽 버튼-->
        <!--오른쪽 버튼-->
        <a id="submitModifyTeacher">
            <div class="right">
                등록
            </div>
        </a>
        <!--//오른쪽 버튼-->
    </div>
    <!--//버튼-->
    <input type="hidden" id="teacherSeq" value="<?=$teacherSeq?>">
</div>
<!--//팝업-->