<!--팝업-->
<div class="box">
    <!--1-->
    <div class="optionList">
        <h1>관리자 추가</h1>
    </div>
    <!--//1-->
    <!--2-->
    <div class="optionList marginTop30">
        <div class="">
            <select id="addAdminTeacher" class="textBox marginTop30">
                <option value="selected">선생님 선택</option>
                <?php
                foreach($teacherList as $list){
                    echo '<option value="'.$list->teacher_seq.'">'.$list->user_name.'</option>';
                }
                ?>
            </select>
        </div>
    </div>
    <!--//2-->
    <!--버튼-->
    <div class="buttonBox marginTop30">
        <!--왼쪽 버튼-->
        <a href="#">
            <div class="left cancelPopup">
                취소
            </div>
        </a>
        <!--//왼쪽 버튼-->
        <!--오른쪽 버튼-->
        <a>
            <div id="submitAdmin" class="right">
                추가
            </div>
        </a>
        <!--//오른쪽 버튼-->
    </div>
    <!--//버튼-->
</div>
<!--//팝업-->