<div class="box">
    <!--1-->
    <div class="optionList">
        <h1>원생 추가</h1>
    </div>
    <!--//1-->
    <!--2-->
    <div class="optionList marginTop30">
        <div class="">
            <input id="childName" type="text" class="textBox marginTop15" placeholder="원생 이름">
            <input id="guardianContact" type="text" class="textBox marginTop15" placeholder="학부모 휴대폰 번호 (숫자만 입력) (ex:01012341234)">
            <select id="selectClass" class="textBox marginTop30">
                <option value="selected">학급 선택</option>
                <?php
                foreach($classList as $list){
                    echo '<option value="'.$list->preschool_class_seq.'">'.$list->preschool_class_name.'</option>';
                }
                ?>
            </select>
            <select id="childGender" class="textBox marginTop30">
                <option value="0">성별</option>
                <option value="1">남</option>
                <option value="2">여</option>
            </select>
            <select id="birthdayYear" class="textBox marginTop30">
                <option value="0">태어난 연도</option>
                <?php
                for($i = 2019; $i > 1999; $i--){
                    echo '<option value="'.$i.'">'.$i.'년</option>';
                }
                ?>
            </select>
            <select id="birthdayMonth" class="textBox borderTopNone">
                <option value="0">태어난 월</option>
                <?php
                for($j = 1; $j < 13; $j++){
                    echo '<option value="'.$j.'">'.$j.'월</option>';
                }
                ?>
            </select>
            <select id="birthdayDay" class="textBox borderTopNone">
                <option value="selected">태어난 일</option>
            </select>
            <input id="preschoolSeq" type="hidden" value="<?=$_SESSION['preschoolSeq']?>">
        </div>
    </div>
    <!--//2-->
    <!--버튼-->
    <div class="buttonBox marginTop30">
        <!--왼쪽 버튼-->
        <a href="#">
            <div class="left cancelPopup">
                닫기
            </div>
        </a>
        <!--//왼쪽 버튼-->
        <!--오른쪽 버튼-->
        <a href="#">
            <div class="right" id="submitChild">
                등록
            </div>
        </a>
        <!--//오른쪽 버튼-->
    </div>
    <!--//버튼-->
</div>