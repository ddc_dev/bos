<div class="mainContentContainer">

    <!--헤드텍스트-->
    <div class="headText">
        <div class="left displayBlock">
            <div class="displayFlex">
                교사 관리
            </div>
        </div>
    </div>
    <!--//헤드텍스트-->
    <!--데이터-->
    <div class="centerInfoContainer">
        <div class="box box-1">
            <div class="displayFlex marginBottom20">
                <div class="width30 displayFlex alignItemsCenter">
                    <h1 style="font-size: 18px; text-align: left">
                        교사 리스트
                    </h1>
                </div>
            </div>
            <div class="displayFlex marginBottom20">
                <!--버튼-->
                <div class="buttonBox width100">
                    <a id="addTeacher">
                        <div class="left">
                            개별 등록
                        </div>
                    </a>
                    <a id="addTeacherExcel">
                        <div class="left">
                            일괄 등록
                        </div>
                    </a>
                    <a id="approveTeacher">
                        <div class="left">
                            선택 승인
                        </div>
                    </a>
                    <a id="delSelectedTeacher">
                        <div class="left">
                            선택 삭제
                        </div>
                    </a>
                    <a href="/teacher/createXLS">
                        <div id="downloadTeacherExcel" class="left">
                            엑셀 내보내기
                        </div>
                    </a>
                    <a id="sendTeacherSMS">
                        <div  class="left">
                            문자 보내기
                        </div>
                    </a>
                </div>
                <!--//버튼-->
            </div>
            <!--head-->
            <ul>
                <li class="head width5"><input type="checkbox" name="headCheckBox" class="checkBox"/></li>
                <li class="head width10">이름</li>
                <li class="head width10">담당 반</li>
                <li class="head width40">이메일</li>
                <li class="head width15">전화번호</li>
                <li class="head width10">승인</li>
                <li class="head width10">관리</li>
            </ul>
            <!--head-->
            <?php
            $index = 0;
            foreach ($teacherList as $list){
                ?>
                <ul>
                    <li class="width5"><input type="checkbox" value="<?=$list->teacher_seq?>" name="bodyCheckBox"  class="checkBox"/></li>
                    <li class="width10"><?=$list->user_name?></li>
                    <li class="width10"><?=$list->preschool_class_name?></li>
                    <li class="width40"><?=$list->user_mail?></li>
                    <li class="width15"><?=splitContact($list->user_contact)?></li>
                    <?php
                    if($list->activated == 1){
                        echo '<li class="width10 pointColor3">승인 대기</li>';
                    } else if($list->activated == 2){
                        echo '<li class="width10 pointColor1">승인 완료</li>';
                    }
                    ?>
                    <li class="width10"><a class="modifyTeacherPopup" value="<?=$list->teacher_seq?>">담당반 관리</a></li>
                </ul>
                <?php
                $index++;
            }
            ?>
        </div>
    </div>
    <!--//데이터-->
</div>

<div id="addPopup"></div>
<div id="addPopupExcel"></div>