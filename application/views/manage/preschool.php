<div class="mainContentContainer">
    <!--헤드텍스트-->
    <div class="headText">
        <div class="left">
            <?php

            if($_SESSION['instType'] == 1) {
                echo $_SESSION['officeName'];
            } else if($_SESSION['instType'] == 2) {
                echo $_SESSION['preschoolName'];
            }

            ?>
        </div>
        <div class="right">
            <div class="childrenListSortingBox displayFlex alignItemsCenter marginTop15">
                <!--동-->
                <select id="preschoolVillage" class="selectBox marginRight30">
                    <option value="<?=$_SESSION['officeCity']?>">동 전체</option>
                    <?php
                    foreach($villageList as $list) {
                        ?>
                        <option value="<?=$list->village_code?>"><?=$list->village_name?></option>
                        <?php
                    }
                    ?>
                </select>
                <!--//동-->
            </div>
        </div>
    </div>
    <!--//헤드텍스트-->
    <!--데이터-->
    <div class="centerInfoContainer">
        <div class="box box-1">
            <!--head-->
            <ul>
                <li class="head width10">보육기관명</li>
                <li class="head width5">반 수</li>
                <li class="head width5">교사 수</li>
                <li class="head width10">원아 수</li>
                <li class="head width10">관리자</li>
                <li class="head width40">관리자 메일</li>
                <li class="head width10">관리자 연락처</li>
                <li class="head width10">기관 연락처</li>
            </ul>
            <!--head-->
            <?php

            foreach ($preschoolList as $list) {

                ?>
                <ul>
                    <li class="width10"><?=$list->preschool_name?></li>
                    <li class="width5"><?=$list->class_count?></li>
                    <li class="width5"><?=$list->teacher_count?></li>
                    <li class="width10"><?=$list->child_count?></li>
                    <li class="width10"><?=$list->user_name?></li>
                    <li class="width40"><?=$list->user_mail?></li>
                    <li class="width10"><?=splitContact($list->user_contact)?></li>
                    <li class="width10"><?=splitContact($list->preschool_contact)?></li>
                </ul>
                
                <?php
            }
            ?>
            
        </div>
    </div>
    <!--//데이터-->
</div>
<!--//콘텐츠-->