<div class="mainContentContainer">
    <!--헤드텍스트-->
    <div class="headText">
        <div class="left displayBlock">
            <div class="displayFlex">
                <?php

                if($_SESSION['instType'] == 1) {
                    echo $_SESSION['officeName'];
                } else if($_SESSION['instType'] == 2) {
                    echo $_SESSION['preschoolName'];
                }

                ?>
            </div>
            <div class="detailBox">
                <!--반,아이디정보-->
<!--                <div class="sortingBox">-->
<!--                    <ul>-->
<!--                        <li>서울특별시 마포구</li>-->
<!--                        <li>02-2363-1005</li>-->
<!--                    </ul>-->
<!--                </div>-->
                <!--//반,아이디정보-->
            </div>

        </div>
    </div>
    <!--//헤드텍스트-->
    <!--데이터-->
    <div class="centerInfoContainer">
        <div class="box box-1">
            <div class="displayFlex marginBottom20">
                <div class="width30 displayFlex alignItemsCenter">
                    <h1 style="font-size: 18px; text-align: left">
                        관리자 리스트
                    </h1>
                </div>
                <?php
                if($_SESSION['instType'] == 2) {
                ?>
                <!--버튼-->
                <div class="buttonBox width70">
                    <a id="addAdmin">
                        <div class="left">
                            부관리자 추가
                        </div>
                    </a>
                    <a id="delSelectedAdmin">
                        <div class="left">
                            선택 삭제
                        </div>
                    </a>
                </div>
                <!--//버튼-->
                    <?php
                }
                ?>
            </div>
            <!--head-->
            <ul>
                <?php
                if($_SESSION['instType'] == 2) {
                ?>
                <li class="head width5"><input type="checkbox" name="headCheckBox" class="checkBox"/></li>
                    <?php
                }
                ?>
                <li class="head width15">이름</li>
                <li class="head width40">메일</li>
                <li class="head width15">휴대폰번호</li>
                <li class="head width15">인증일</li>
                <li class="head width10">등급</li>
            </ul>
            <!--head-->
            <?php
            foreach ($adminList as $list){
                ?>
                <ul>
                    <?php
                    if($_SESSION['instType'] == 2) {
                        if($list->admin_type == 1) {
                            ?>
                            <li class="width5"><input type="checkbox" class="checkBox" disabled/></li>
                            <?php
                        } else {
                            ?>
                            <li class="width5"><input type="checkbox" name="bodyCheckBox" class="checkBox" value="<?=$list->user_seq?>"/></li>
                    <?php
                        }
                    }
                    ?>
                    <li class="width15"><?=$list->user_name?></li>
                    <li class="width40"><?=$list->user_mail?></li>
                    <li class="width15"><?=splitContact($list->user_contact)?></li>
                    <li class="width15">&nbsp;</li>
                    <li<?=$list->admin_type==1?' class="width10 pointColor1"':' class="width10 pointColor3"'?>><?=$list->admin_type==1?"관리자":"부관리자"?></li>
                </ul>
                <?php
            }
            ?>

            <!--페이지-->
<!--            <div class="pagination">-->
<!--                <a href="#">이전페이지</a>&nbsp;&nbsp;&nbsp;1 . 2 . 3 . 4 ... 12&nbsp;&nbsp;&nbsp;<a href="#">다음페이지</a>-->
<!--            </div>-->
            <!--//페이지-->
        </div>
    </div>
    <!--//데이터-->
</div>
<div id="addPopup"></div>