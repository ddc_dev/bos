<div class="box">
    <!--1-->
    <div class="optionList">
        <h1>선생님 일괄 등록</h1>
    </div>
    <!--//1-->
    <!--2-->
    <form id="addTeacherExcelForm" enctype="multipart/form-data"
          method="post" accept-charset="utf-8">

        <div class="optionList marginTop30">
            <div class="">

                <style>
                    .buttonBox input[type="submit"] {
                        /* 파일 필드 숨기기 */
                        position: absolute;
                        width: 1px;
                        height: 1px;
                        padding: 0;
                        margin: -1px;
                        overflow: hidden;
                        clip: rect(0, 0, 0, 0);
                        border: 0;
                    }


                    /* named upload */
                    .upload-name {
                        width: 100%;
                        display: inline-block;
                        font-size: inherit;
                        font-family: inherit;
                        line-height: normal;
                        vertical-align: middle;
                        background-color: #f5f5f5;
                        border: 1px solid #ebebeb;
                        border-bottom-color: #e2e2e2;
                        border-radius: .25em;
                        -webkit-appearance: none; /* 네이티브 외형 감추기 */
                        -moz-appearance: none;
                        appearance: none;
                    }

                </style>
                <div class="divider"></div>

                <div class="buttonFull marginTop15 buttonPaddingVertical0">
                    <input name="userfile" type="file" id="file" class="inputFile">
                    <label for="file">엑셀파일 올리기</label>
                </div>
            </div>
        </div>
        <div id="fileList" class="excelFileList marginTop30">
        </div>
        <!--버튼-->
        <div class="buttonBox marginTop0">

            <!--왼쪽 버튼-->
            <a href="#">
                <div class="left cancelPopup">
                    닫기
                </div>
            </a>
            <!--//왼쪽 버튼-->
            <!--오른쪽 버튼-->
            <div id="submitTeacherExcel" class="right">
                <input type="submit" value="upload"/>
                <label for="submit">등록</label>
            </div>
            <!--//오른쪽 버튼-->
        </div>
    </form>
</div>
<script>

    $('#file').on('change', function () { // 값이 변경되면

        if (window.FileReader) { // modern browser
            var filename = $(this)[0].files[0].name;
            console.log(filename);

        } else { // old IE
            var filename = $(this).val().split('/').pop().split('\\').pop(); // 파일명만 추출
        } // 추출한 파일명 삽입
        var p = document.createElement('p');
        var name = document.createTextNode(filename);
        p.appendChild(name);
        $('#fileList').empty();
        $('#fileList').append(p);
    });
</script>