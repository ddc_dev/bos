<div class="mainContentContainer">
    <!--헤드텍스트-->
    <div class="headText">
        <div class="left displayBlock">
            <div class="displayFlex">
                <?php

                if($_SESSION['instType'] == 1) {
                    echo $_SESSION['officeName'];
                } else if($_SESSION['instType'] == 2) {
                    echo $_SESSION['preschoolName'];
                }

                ?>
            </div>
        </div>
    </div>
    <!--//헤드텍스트-->
    <!--데이터-->
    <div class="centerInfoContainer">
        <div class="box box-1">
            <div class="displayFlex marginBottom20">
                <div class="width30 displayFlex alignItemsCenter">
                    <h1 style="font-size: 18px; text-align: left">
                        반 리스트
                    </h1>
                </div>
                <!--버튼-->
                <div class="buttonBox width70">
                    <a id="addClass">
                        <div class="left marginRight30">
                            반 추가
                        </div>
                    </a>
                    <a id="delSelectedClass">
                        <div class="left marginRight0">
                            선택 삭제
                        </div>
                    </a>
                </div>
                <!--//버튼-->
            </div>
            <!--head-->
            <ul>
                <li class="head width13"><input type="checkbox" name="headCheckBox" class="checkBox"/></li>
                <li class="head width13">반 이름</li>
                <li class="head width13">담임</li>
                <li class="head width13">이메일</li>
                <li class="head width13">전화번호</li>
                <li class="head width13">원아 수</li>
                <li class="head width13">관리</li>
            </ul>
            <!--head-->
            <?php

            $index = 0;
            foreach ($classList as $list){
                $teacherName = "";
                $teacherMail = "";
                $teacherContact = "";
                foreach($teacherList as $tList){
                    if ($tList->preschool_class_seq == $list->preschool_class_seq) {
                        $teacherName .= $tList->user_name.'<br>';
                        $teacherMail .= $tList->user_mail.'<br>';
                        $teacherContact .= splitContact($tList->user_contact).'<br>';
                    }
                }
                ?>
                <ul>
                    <li class="width13"><input type="checkbox" value="<?=$list->preschool_class_seq?>" name="bodyCheckBox" class="checkBox"/></li>
                    <li class="width13"><?=$list->preschool_class_name?></li>
                    <li class="width13"><?=$teacherName?></li>
                    <li class="width13"><?=$teacherMail?></li>
                    <li class="width13"><?=$teacherContact?></li>
                    <li class="width13"><?=$list->child_count?></li>
                    <li class="width13"><a class="modifyPopup" value="<?=$index?>">관리</a></li>
                </ul>
                <?php
                $index++;
            }
            ?>
            <!--페이지-->
<!--            <div class="pagination">-->
<!--                <a href="#">이전페이지</a>&nbsp;&nbsp;&nbsp;1 . 2 . 3 . 4 ... 12&nbsp;&nbsp;&nbsp;<a href="#">다음페이지</a>-->
<!--            </div>-->
            <!--//페이지-->
        </div>
    </div>
    <!--//데이터-->
</div>
<!--//콘텐츠-->
<div id="addPopup"></div>