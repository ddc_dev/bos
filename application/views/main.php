<div class="mainContentContainer">
    <!--헤드텍스트-->
    <div class="headText">
        <div class="left displayBlock">
            <div class="displayFlex">
                <?php

                if($_SESSION['instType'] == 1) {
                    echo $_SESSION['officeName'];
                } else if($_SESSION['instType'] == 2) {
                    echo $_SESSION['preschoolName'];
                }

                ?>
            </div>
        </div>
    </div>
    <!--//헤드텍스트-->
    <!--식단, 공지사항-->
    <div class="centerInfoContainer">
        <!--row 1-->
        <ul class="borderBottomNone">
            <!--1-->
            <div class="box box-1">
                <!--head-->
                <div class="displayFlex marginBottom20">
                    <div class="width30 displayFlex alignItemsCenter">
                        <h1 style="font-size: 18px; text-align: left; margin: 0; font-weight: bold;">
                            공지사항
                        </h1>
                    </div>
                    <!--버튼-->
                    <div class="buttonBox width70">
                        <a href="notice">
                            더 보기
                        </a>
                    </div>
                    <!--//버튼-->
                </div>
                <!--//head-->
                <!--리스트-->
                <div>
                    <!--1-->
                    <?php
                    foreach($noticeList as $list){
                        $datetime = explode(' ', $list->notice_created_time);
                        $date = $datetime[0];
                        $time = $datetime[1];
                        if ($date == Date('Y-m-d')) {
                            $mark_datetime = $time;
                        } else {
                            $mark_datetime = $date;
                        }
                        switch ($list->notice_type) {
                            case 1:
                                $type = '공지사항';
                                break;
                            case 2:
                                $type = '월간식단';
                                break;
                            case 3:
                                $type = '표준레시피';
                                break;
                            case 4:
                                $type = '가정통신문';
                                break;
                        }
                        ?>
                        <ul>
                            <li class="width10 flexBasisAuto"><?=$type?></li>
                            <li class="width50 flexBasisAuto"><a href="/notice/content/<?=$list->notice_seq?>"><?=$list->title?></a></li>
                            <li class="width15 flexBasisAuto"><?=$list->user_name?></li>
                            <li class="width15 flexBasisAuto"><?=$mark_datetime?></li>
                        </ul>
                    <?php
                    }
                    ?>
                </div>
                <!--//리스트-->
            </div>
            <!--//1-->

        </ul>
        <!--//row 1-->
    </div>

</div>