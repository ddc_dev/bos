<div class="mainContentContainer">
    <!--헤드텍스트-->
    <div class="headText">
        <div class="left displayBlock">
            <div class="displayFlex">
                영양정보
            </div>
        </div>
    </div>
    <!--//헤드텍스트-->
    <!--데이터-->
    <div class="centerInfoContainer">
        <div class="box box-1">
            <div class="displayFlex marginBottom20">
                <div class="width50 displayFlex alignItemsCenter">
                </div>
                <!--버튼-->
                <div class="buttonBox">
                    <a href="#">
                        <?php
                        if ($_SESSION['instType'] != 1) {
                            echo ' <div id="fileUpload" class="left">식단 등록</div>';
                        }
                        ?>

                    </a>
                    <a id="link" href="#">
                        <div id="downloadMenu" class="right">
                            엑셀로 내보내기
                        </div>
                    </a>
                </div>
                <!--//버튼-->
            </div>
        </div>
    </div>
    <!--//데이터-->
</div>
<!--//콘텐츠-->