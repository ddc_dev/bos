<div class="box">
    <!--1-->
    <div class="optionList">
        <h1>식단 등록/수정</h1>
    </div>
    <!--//1-->
    <!--2-->
    <form id="myForm" enctype="multipart/form-data"
          method="post" accept-charset="utf-8">

        <div class="optionList marginTop30">
            <div class="">
                <div class="textBox marginTop15">
                    <h1>월 선택</h1>
                    <select id="popMonth" name="month" class="textBox marginTop15">
                    </select>
                </div>
                <div class="textBox marginTop15">
                    <h1>연령</h1>
                    <select id="popAge" name="age" class="textBox marginTop15">
                        <option value="-1">연령</option>
                        <option value="0">만 0세</option>
                        <option value="1">만 1-2세</option>
                        <option value="2">만 3-5세</option>

                    </select>
                </div>

                <div class="textBox marginTop15">
                    <h1>식단 종류</h1>
                    <select id="popType" name="type" class="textBox marginTop15">
                        <option value="-1">식단 유형</option>
                    </select>
                </div>

                <style>
                    .buttonBox input[type="submit"] {
                        /* 파일 필드 숨기기 */
                        position: absolute;
                        width: 1px;
                        height: 1px;
                        padding: 0;
                        margin: -1px;
                        overflow: hidden;
                        clip: rect(0, 0, 0, 0);
                        border: 0;
                    }


                    /* named upload */
                    .upload-name {
                        width: 100%;
                        display: inline-block;
                        font-size: inherit;
                        font-family: inherit;
                        line-height: normal;
                        vertical-align: middle;
                        background-color: #f5f5f5;
                        border: 1px solid #ebebeb;
                        border-bottom-color: #e2e2e2;
                        border-radius: .25em;
                        -webkit-appearance: none; /* 네이티브 외형 감추기 */
                        -moz-appearance: none;
                        appearance: none;
                    }

                </style>
                <div class="divider"></div>

                <div class="buttonFull marginTop15 buttonPaddingVertical0">
                    <input name="userfile" type="file" id="file" class="inputFile">
                    <label for="file">엑셀파일 올리기</label>
                </div>
            </div>
        </div>
        <div id="fileList" class="excelFileList marginTop30">
        </div>
        <!--버튼-->
        <div class="buttonBox marginTop15">
            <!--왼쪽 버튼-->
            <a href="#">
                <div class="left cancelPopup">
                    닫기
                </div>
            </a>
            <!--//왼쪽 버튼-->
            <!--오른쪽 버튼-->
            <div id="submitBtn" class="right">
                <input type="submit" value="upload"/>
                <label for="submit">등록</label>
            </div>
            <!--//오른쪽 버튼-->
        </div>
    </form>
</div>


<script>


    var currentDate = new Date();
    console.log(currentDate);
    for (let i = 0; i < 6; i++) {

        var option1 = document.createElement('option');

        var text = document.createTextNode(currentDate.getFullYear() + "년 " + (currentDate.getMonth() + 1) + "월");
        option1.appendChild(text)
        option1.value = currentDate.getFullYear() + "/" + (currentDate.getMonth()+1);
        $('#popMonth').append(option1);

        currentDate.setMonth(currentDate.getMonth() + 1);

    }


    $(document).on('change', '#popAge', function () {

        console.log('나이 바뀜');

        var age = $('#popAge').val();

        $('#popType').children().remove();
        var option = '<option value="-1">식단 유형</option>';

        if (age === "0") {

            console.log('0세');


            option += '<option value="4">이유식</option>';

        } else {
            option += '  <option value="0">일반식</option>\n' +
                '                        <option value="1">죽식</option>\n' +
                '                        <option value="2">아토피 건강식</option>'+
                '                        <option value="3">저녁식</option>\n';

        }


        $('#popType').append(option);


    });


    $('#file').on('change', function () { // 값이 변경되면

        if (window.FileReader) { // modern browser
            var filename = $(this)[0].files[0].name;
            console.log(filename);

        } else { // old IE
            var filename = $(this).val().split('/').pop().split('\\').pop(); // 파일명만 추출
        } // 추출한 파일명 삽입
        var p = document.createElement('p');
        var name = document.createTextNode(filename);
        p.appendChild(name);
        $('#fileList').empty();
        $('#fileList').append(p);
    });


</script>