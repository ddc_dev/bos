<div class="box">
    <!--1-->
    <div class="optionList">
        <h1>염도 등록</h1>
    </div>
    <!--//1-->
    <!--2-->
    <div class="optionList marginTop30">
        <div class="">
            <input id="inputSalt" type="text" class="textBox marginTop15" placeholder="염도를 입력해주세요 (숫자만)" onkeypress="return isNumberKey(event)"/>
            <?php echo $salt;?>
        </div>
    </div>

    <!--//2-->
    <!--버튼-->
    <div class="buttonBox marginTop15">

        <!--왼쪽 버튼-->
        <a href="#">
            <div class="left cancelPopup">
                닫기
            </div>
        </a>
        <!--//왼쪽 버튼-->

        <!--오른쪽 버튼-->
        <div id="submitSalt" class="right">
            등록
        </div>
        <!--//오른쪽 버튼-->
    </div>
    <!--//버튼-->
</div>