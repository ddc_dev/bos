<script type="text/javascript" src="/files/js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="/files/js/menu.js"></script>

<style>

    .foodInfoContainer table td {
        vertical-align: top;
        padding: 8px;
        font-size: 13px;
        margin: 5px;
        line-height: 20px;
    }

    .foodInfoContainer ul li {

        padding: 0;
        margin: 0 0;
        font-size: 13px;
    }



</style>
<!--콘텐츠-->
<div class="mainContentContainer">
    <!--헤드텍스트-->
    <div class="headText">
        <div class="left displayBlock">
            <div class="displayFlex">
                영양정보
            </div>
        </div>
        <div class="right">
            <div id="selectBar" class="childrenListSortingBox displayFlex alignItemsCenter marginTop15">
                <!--left-->
                <div class="left box">
                    <!--반-->
                    <select id="age" class="selectBox marginRight30">
                        <option value="-1">연령</option>
                        <option value="0">0세</option>
                        <option value="1">만 1-2세</option>
                        <option value="2">만 3-5세</option>

                    </select>
                    <!--//반-->
                    <!--반-->
                    <select id="type" class="selectBox">
                        <option value="-1">식단 유형</option>
                    </select>
                    <!--//반-->
                </div>
                <!--//left-->
                <!--right-->
                <div class="right width">
                    <div class="dateBox displayFlex alignItemsCenter">
                        <img id="before" src="/files/css/images/left_arrow_ico.svg">
                        <span id="date" class="date paddingHorizontal20">2019.04</span>
                        <img id="after" src="/files/css/images/right_arrow_ico.svg">
                    </div>
                </div>
                <!--//right-->
            </div>
        </div>
    </div>
    <!--//헤드텍스트-->
    <!--데이터-->
    <div class="foodInfoContainer">
        <div class="box box-1 marginBottom30">
            <div class="displayFlex marginBottom20">
                <div class="width50 displayFlex alignItemsCenter">
                </div>
                <!--버튼-->
                <div class="buttonBox">
                    <a href="#">
                        <?php
                        if ($_SESSION['instType'] != 1) {
                            echo ' <div id="fileUpload" class="right marginRight30">식단 등록</div>';
                        }
                        ?>

                    </a>
                    <a id="link" href="#">

                    </a>
                </div>
                <!--//버튼-->
            </div>
            <!--1주_head-->
            <div id="menu">

            </div>

        </div>
    </div>
    <!--//데이터-->
</div>
<!--//콘텐츠-->
<div id="uploadPopUp"></div>
<div id="notePopUp"></div>
<div id="saltPopUp"></div>