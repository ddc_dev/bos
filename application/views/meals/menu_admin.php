<script type="text/javascript" src="/files/js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="/files/js/menu_admin.js"></script>
<!--콘텐츠-->
<div class="mainContentContainer">
    <!--헤드텍스트-->
    <div class="headText">
        <div class="left displayBlock">
            <div class="displayFlex">
                식단 기록 다운로드
            </div>
        </div>
    </div>
    <!--//헤드텍스트-->
    <!--데이터-->
    <div class="centerInfoContainer">
        <div class="box box-1">
            <div class="displayFlex marginBottom20">
                <div class="width50 displayFlex alignItemsCenter">
                    <div id="selectBar" class="childrenListSortingBox displayFlex alignItemsCenter marginTop15">
                        <!--left-->
                        <div class="left box-1">
                            <!--카테고리-->
                            <select id="dateType" class="selectBox">
                                <option value="day">일별</option>
                                <option value="month">월별</option>

                            </select>
                            <!--카테고리-->
                        </div>
                        <!--//left-->
                        <!--right-->
                        <div class="right">
                            <div class="dateBox displayFlex alignItemsCenter">
                                <img id="before" src="/files/css/images/left_arrow_ico.svg">
                                <span id="date" class="date paddingHorizontal20">2019.04</span>
                                <img id="after" src="/files/css/images/right_arrow_ico.svg">
                            </div>
                        </div>
                        <!--//right-->
                    </div>
                </div>


                <!--버튼-->
                <div class="buttonBox">
                    <a id="excelLink" href="#">
                        <div id="downloadAdminExcel" class="left">파일 다운로드</div>
                    </a>
                </div>
                <!--//버튼-->
            </div>
        </div>
    </div>
    <!--//데이터-->
</div>
