<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>DDC 대시보드</title>
    <link rel="stylesheet" href="/files/css/style.css" type="text/css"/>
    <link rel="stylesheet" href="/files/css/reset.css" type="text/css"/>
    <link rel="stylesheet" href="/files/css/common.css" type="text/css"/>
    <link rel="stylesheet" href="/files/css/MonthPicker.css" type="text/css"/>
    <link rel="stylesheet" href="/files/css/custom.css" type="text/css"/>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="/files/css/all.css">
    <link href="https://fonts.googleapis.com/css?family=Gothic+A1|Nanum+Gothic|Nanum+Gothic+Coding|Nanum+Myeongjo|Noto+Serif+KR|Stylish&display=swap" rel="stylesheet">
    <link href="/files/summernote/summernote-lite.css" rel="stylesheet">
</head>

<body>

<div class="wrapper">
<?php
function splitContact($num){
    $num = preg_replace("/[^0-9]*/s","",$num); //숫자이외 제거

    if (substr($num,0,2) == '02') {
        return preg_replace("/([0-9]{2})([0-9]{3,4})([0-9]{4})$/","\\1.\\2.\\3", $num);
    } else if(substr($num,0,2) == '8' && substr($num,0,2) == '15' || substr($num,0,2) =='16'|| substr($num,0,2) =='18'){
        return preg_replace("/([0-9]{4})([0-9]{4})$/","\\1.\\2",$num);
    } else {
        return preg_replace("/([0-9]{3})([0-9]{3,4})([0-9]{4})$/", "\\1.\\2.\\3", $num);
    }
}
?>