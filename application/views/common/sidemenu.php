<!--사이드 메뉴-->
<div class="sideMenuContainer">
    <!--로고-->
    <div class="logo">
        <img src="/files/img/logo.svg"/>
    </div>
    <!--//로고-->
    <!--메뉴-->
    <ul style="margin-top: 30px">
        <li>
            <?php
            if ($this->uri->segments[1] == 'main') {
                echo '<i class="fas fa-home selected"></i><a class="selected">홈</a>';
            } else {
                echo '<i class="fas fa-home"></i><a href="/main">홈</a>';
            }
            ?>
        </li>
        <li>
            <?php
            $noticeArr = array('all', 'general', 'monthlyMenu', 'recipe', 'telegram');
            if ($this->uri->segments[1] == 'notice' || (isset($this->uri->segments[2]) && in_array($this->uri->segments[2], $noticeArr))) {
                echo '<i class="far fa-clipboard selected"></i><a class="selected">공지사항</a>';
                $notDisplay = 'block';
            } else {
                echo '<i class="far fa-clipboard"></i><a id="smNotice">공지사항</a>';
                $notDisplay = 'none';
            }
            ?>
        </li>
        <div id="smNoticeSub" class="subMenu" style="display: <?=$notDisplay?>;">
            <ul>
                <?php
                $all = '';
                $general = '';
                $monthlyMenu = '';
                $recipe = '';
                $telegram = '';

                if ($this->uri->segments[1] == 'notice' && isset($this->uri->segments[2])) {
                    if ($this->uri->segments[2] == 'all') {
                        $all = 'class="selected"';
                    } else if ($this->uri->segments[2] == 'general') {
                        $general = 'class="selected"';
                    } else if ($this->uri->segments[2] == 'monthlyMenu') {
                        $monthlyMenu = 'class="selected"';
                    } else if ($this->uri->segments[2] == 'recipe') {
                        $recipe = 'class="selected"';
                    } else if ($this->uri->segments[2] == 'telegram') {
                        $telegram = 'class="selected"';
                    }
                }
                ?>
                <li><a href="/notice/all" <?=$all?>>전체</a></li>
                <li><a href="/notice/general" <?=$general?>>공지사항</a></li>
                <li><a href="/notice/monthlyMenu" <?=$monthlyMenu?>>월간식단</a></li>
                <li><a href="/notice/recipe" <?=$recipe?>>표준 레시피</a></li>
                <li><a href="/notice/telegram" <?=$telegram?>>가정통신문</a></li>
                <?php

                ?>
            </ul>
        </div>
        <li>
            <?php
            if ($this->uri->segments[1] == 'menu') {
                echo '<img src="/files/css/images/ico3_selected.svg">';
                echo '<a class="selected">식단</a>';
            } else {
                echo '<img src="/files/css/images/ico3.svg">';
                echo '<a href="/menu">식단</a>';
            }
            ?>
        </li>
        <li>
            <?php
            $arr = array('institution', 'adminClass', 'preschool', 'teacher', 'children');
            if (in_array($this->uri->segments[1], $arr)) {
                echo '<img src="/files/css/images/ico4_selected.svg">';
                echo '<a class="selected">관리</a>';
                $manDisplay = 'block';
            } else {
                echo '<img src="/files/css/images/ico4.svg">';
                echo '<a id="smManage">관리</a>';
                $manDisplay = 'none';
            }
            ?>
        </li>
        <div id="smManageSub" class="subMenu" style="display: <?=$manDisplay?>;">
            <ul>
                <!--                <li><a>기관관리</a></li>-->
                <?php
                $institution = '';
                $preschool = '';
                $class = '';
                $teacher = '';
                $children = '';
                if ($this->uri->segments[1] == 'institution') {
                    $institution = 'class="selected"';
                } else if ($this->uri->segments[1] == 'preschool') {
                    $preschool = 'class="selected"';
                } else if ($this->uri->segments[1] == 'adminClass') {
                    $class = 'class="selected"';
                } else if ($this->uri->segments[1] == 'teacher') {
                    $teacher = 'class="selected"';
                } else if ($this->uri->segments[1] == 'children') {
                    $children = 'class="selected"';
                }

                if ($_SESSION['instType'] == 1) {
                    $instName = $_SESSION['officeName'];
                } else if ($_SESSION['instType'] == 2) {
                    $instName = $_SESSION['preschoolName'];
                }

                if ($_SESSION['instType'] == 1) {
                    ?>
                    <li><a href="/institution" <?= $institution ?>><?= $instName ?> 관리</a></li>
                    <li><a href="/preschool" <?= $preschool ?>>보육기관 관리</a></li>
                    <?php
                } else if ($_SESSION['instType'] == 2) {
                    if ($_SESSION['role'] == 2) {
                        ?>
                        <li><a href="/institution" <?= $institution ?>><?= $instName ?> 관리</a></li>
                        <li><a href="/adminClass" <?= $class ?>>반 관리</a></li>
                        <li><a href="/teacher" <?= $teacher ?>>교사 관리</a></li>
                        <li><a href="/children" <?= $children ?>>원아 관리</a></li>
                        <?php
                    } else if ($_SESSION['role'] == 4) {
                        ?>
                        <li><a href="/children" <?= $children ?>>원아 관리</a></li>
                        <?php
                    }
                }
                ?>
            </ul>
        </div>
<!--        <li><img src="/files/css/images/ico5.svg"><a>설정</a></li>-->
    </ul>
    <!--//메뉴-->
    <!--로그인 박스-->
    <div class="loginBox">
        <a><?=$_SESSION['user_name']?></a>
        <p class="marginTop15"> [ <a id="profile">정보수정</a> &nbsp;&nbsp;|&nbsp;&nbsp; <a href="/auth/logout">로그아웃</a> ] </p>
    </div>

    <!--//로그인 박스-->
</div>
<!--//사이드 메뉴-->

<div id="profilePopup"></div>

