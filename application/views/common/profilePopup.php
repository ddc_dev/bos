<div class="box">
    <div class="optionList">
        <h1>회원 정보 변경</h1>
    </div>
    <div class="optionList marginTop30">
        <div class="">
        <h1>비밀번호 변경</h1>
            <input id="password" type="password" class="textBox marginTop15" placeholder="새 비밀번호 (영문, 숫자, 조합 8자리 이상)">
            <input id="passwordConfirm" type="password" class="textBox borderTopNone" placeholder="새 비밀번호 확인">
            <div class="divider"></div>
            <h1 class="marginTop15">휴대전화 번호 변경</h1>
            <input id="changePhoneNumber" type="text" class="textBox marginTop15" placeholder="변경할 휴대폰 번호 (숫자만 입력)">
            <a href="#">
                <div id="sendChangePhoneNumberSMS" class="buttonFull marginTop15">인증번호 전송</div>
            </a>
            <input id="confirmNumber" type="text" class="textBox marginTop15" placeholder="인증번호 6자리">
            <a href="#">
                <div id="checkConfirmNumber" class="buttonFull marginTop15">확인</div>
            </a>
        </div>
    </div>

    <div class="buttonBox marginTop15">
        <a href="#">
            <div class="left cancelPopup">
                닫기
            </div>
        </a>
        <a href="#">
            <div id="changeProfile" class="right">
                변경
            </div>
        </a>
    </div>
</div>