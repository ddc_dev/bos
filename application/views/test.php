<html>
<head>
    <script src="/files/js/jquery-3.3.1.min.js"></script>
    <script src="/files/js/Highcharts-7.1.1/code/highcharts.js"></script>
    <script src="/files/js/Highcharts-7.1.1/code/highcharts-more.js"></script>
    <script src="/files/js/Highcharts-7.1.1/code/modules/solid-gauge.js"></script>
    <script src="/files/js/Highcharts-7.1.1/code/modules/exporting.js"></script>
    <script src="/files/js/Highcharts-7.1.1/code/modules/export-data.js"></script>

</head>
<body>


<!--<div id="container2" style="min-width: 310px; max-width: 400px; height: 300px; margin: 0 auto"></div>-->
<!---->
<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

<button id="button">그래프 갱신</button>

<textarea id="text_box" rows="10" cols="100"></textarea>

<!--<h1>--><?php //foreach($hour as $item){
//    echo $item['ch_hour_act1'];
//    }?><!--</h1>-->

<script>
    $(function() {

        var data_act1 = [];
        var data_act2 = [];
        var data_act3 = [];

        var category_day = ['00', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23'];
        var category_week = ['일','월','화','수','목','금','토'];
        var category_month = [];
        var category_year = ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월']

        $('#container').highcharts({
            chart: {
                type: 'column'
            },
            exporting:
                {
                    enabled: false
                }
            ,
            credits: {
                enabled: false
            }
            ,
            title: {
                text: '활동 시간'
            }
            ,
            xAxis: {
               type : 'category'
            }
            ,
            yAxis: {
                min: 0,
                title:
                    {
                        text: '활동시간 (분)'
                    }
            }
            ,
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat:
                    '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} 분</b></td></tr>',
                footerFormat:
                    '</table>',
                shared:
                    true,
                useHTML:
                    true
            }
            ,
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth:
                        0
                }
            }
            ,
            series: [{
                name: 'High'

            }, {
                name: 'Medium'

            }, {
                name: 'Low'

            }]
        });


        $('#button').on('click', function () {

            var province;
            var city;
            var village;
            var preschool;
            var preschool_class;
            var term_type = 'month';
            var age;
            var gender;
            var obesity;
            var action_time = new Date(2019,2,26);

            if (term_type === 'month'){

                var lastDay = new Date(action_time.getFullYear(), action_time.getMonth(), 0).getDate();
                category_month = [];
                for (var i =1; i<=lastDay; i++){

                    category_month.push(i);

                }

            }

            $.post('data/option',
                {
                    province: '1100000000',
                    city: '1111000000',
                    village: '1111010100',
                    // province: null,
                    // city: null,
                    // village: null,
                    preschool: null,
                    class: null,
                    term_type: term_type,
                    age : 0,
                    gender : 0,
                    obesity : -1,
                    action_time: action_time.getFullYear()+'-'+action_time.getMonth()+'-'+action_time.getDate()

                },
                function (response, status) {


                    $('#text_box').append(response);

                    if (response){

                        var json_response = JSON.parse(response);

                        data_act1 = json_response.act1;
                        data_act2 = json_response.act2;
                        data_act3 = json_response.act3;

                    }else{

                        alert("해당하는 데이터가 없습니다");

                    }





                }
            )
                .done(function () {
                    var chart1 = $('#container').highcharts()
                    chart1.series[0].update({
                        data: data_act1
                    }, false);
                    chart1.series[1].update({
                        data: data_act2
                    }, false);
                    chart1.series[2].update({
                        data: data_act3
                    }, false);



                    switch (term_type) {
                        case 'day':
                            chart1.xAxis[0].setCategories(category_day,false);
                            break;
                        case 'week':
                            chart1.xAxis[0].setCategories(category_week,false);
                            break;
                        case 'month':
                            chart1.xAxis[0].setCategories(category_month,false);
                            break;
                        case 'year':
                            chart1.xAxis[0].setCategories(category_year,false);
                            break;

                    }

                    chart1.redraw();
                })

                .fail(function (response) {
                    alert(response.responseText);
            });
        });


    });
</script>
</body>


</html>



