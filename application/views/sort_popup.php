
    <div class="box" id="sort_popup">
        <!--나이별-->
        <div class="optionList">
            <h1>나이별</h1>
            <ul id="sortAge">
                <li class="selected" value="allAge">전체</li>
                <li value="age3">3세</li>
                <li value="age4">4세</li>
                <li value="age5">5세</li>
                <li value="age6">6세</li>
                <li value="age7">7세</li>
            </ul>
        </div>
        <!--//나이별-->
        <!--성별-->
        <div class="optionList marginTop30">
            <h1>성별</h1>
            <ul id="sortGender">
                <li class="selected" value="allGender">전체</li>
                <li value="boy">남아</li>
                <li value="girl">여아</li>
            </ul>
        </div>
        <!--//성별-->
        <!--상태별-->
        <div class="optionList marginTop30">
            <h1>상태별</h1>
            <ul id="sortBMI">
                <li class="selected" value="allBMI">전체</li>
                <li value="under">저체중</li>
                <li value="healthy">정상체중</li>
                <li value="over">과체중</li>
                <li value="obese">비만</li>
                <li value="extreme">고도비만</li>
            </ul>
        </div>
        <!--//상태별-->
        <!--버튼-->
        <div class="buttonBox marginTop0">
            <!--왼쪽 버튼-->
            <a href="#">
                <div class="left cancelPopup">
                    취소
                </div>
            </a>
            <!--//왼쪽 버튼-->
            <!--오른쪽 버튼-->
            <a href="#">
                <div id="popupSort" class="right">
                    확인
                </div>
            </a>
            <!--//오른쪽 버튼-->
        </div>
        <!--//버튼-->
    </div>