<div class="box">
    <!--1-->
    <div class="optionList">
        <h1>관리자 등록</h1>
    </div>
    <!--//1-->
    <!--2-->
    <div class="optionList marginTop30">
        <div class="">
            <input type="text" class="textBox marginTop15" placeholder="아이디 (영문 숫자 조합 10자리 이내)">
            <input type="text" class="textBox borderTopNone" placeholder="비밀번호 (영문, 숫자, 조합 6자리 이상)">
            <input type="text" class="textBox borderTopNone" placeholder="비밀번호 확인">
            <input type="text" class="textBox borderTopNone" placeholder="담당자 휴대폰 번호 (숫자만 입력)">
            <a href="#">
                <div class="buttonFull marginTop15">인증번호 전송</div>
            </a>
            <input type="text" class="textBox marginTop15" placeholder="인증번호 6자리">
            <a href="#">
                <div class="buttonFull marginTop15">확인</div>
            </a>
        </div>
    </div>
    <!--//2-->
    <!--버튼-->
    <div class="buttonBox marginTop0">
        <!--오른쪽 버튼-->
        <a href="#">
            <div class="right">
                등록
            </div>
        </a>
        <!--//오른쪽 버튼-->
    </div>
    <!--//버튼-->
</div>