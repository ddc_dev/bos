<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Base_Controller extends CI_Controller {
    function __construct() {
        parent::__construct();

        $this->load->database();
        $this->lang->load(array('common_lang'), isset($_SESSION['language'])?$_SESSION['language']:'en');        
    }
    
    function _head() {        
//        $this->load->model('Auth_model');
//        $admin_seq = $this->session->seq;
//        $data['menu_list'] = $this->Auth_model->get_menu_list($admin_seq);
        
        $this->load->view('common/head');
    }

    function _loginHead() {
        $this->load->view('common/loginHead');
    }

    function _side(){
        $this->load->view('common/sidemenu');
    }
    
    function _bottom() {
        $this->load->view('common/footer');
    }

    function addPopup(){
        $this->load->view('notice/notice_write_popup');
    }

    function splitContact($num){
        $num = preg_replace("/[^0-9]*/s","",$num); //숫자이외 제거

        if (substr($num,0,2) == '02') {
            return preg_replace("/([0-9]{2})([0-9]{3,4})([0-9]{4})$/","\\1.\\2.\\3", $num);
        } else if(substr($num,0,2) == '8' && substr($num,0,2) == '15' || substr($num,0,2) =='16'|| substr($num,0,2) =='18'){
            return preg_replace("/([0-9]{4})([0-9]{4})$/","\\1.\\2",$num);
        } else {
            return preg_replace("/([0-9]{3})([0-9]{3,4})([0-9]{4})$/","\\1.\\2.\\3" ,$num);
        }
    }
            
}