<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
if (!function_exists('common')) {

    // Y-m-d 형식의 date를 unix 타임으로 변환
    if (!function_exists('Ymd2timestamp')) {

        function Ymd2timestamp($Ymd) {
            $Ymd = str_replace("-", "", $Ymd);
            //mktime(int hour, int minute, int second, int month, int day, int year );
            return mktime('0', '0', '0', substr($Ymd, 4, 2), substr($Ymd, 6, 2), substr($Ymd, 0, 4));
        }

    }

    //Alert
    if (!function_exists('Alert')) {

        function Alert($msg = "", $url = "") {
            $CI = & get_instance();
            echo "<meta http-equiv=\"content-type\" content=\"text/html; charset=" . $CI->config->item('charset') . "\">";
            echo "<script type='text/javascript'>alert('" . $msg . "');";
            if ($url)
                echo "location.replace('" . $url . "');";
            else
                echo "history.go(-1);";
            echo "</script>";
            exit;
        }

    }
    //Alert 주소 이동
    if (!function_exists('Alert2url')) {

        function Alert2url($msg, $url) {
            echo "<html><script language='javascript'>alert('" . $msg . "');location.replace('" . $url . "')</script></html>";
            exit;
        }

    }
    //Alert 후 창 닫음
    if (!function_exists('Alert2close')) {

        function Alert2close($msg) {
            $CI = & get_instance();
            echo "<meta http-equiv=\"content-type\" content=\"text/html; charset=" . $CI->config->item('charset') . "\">";
            echo "<script type='text/javascript'> alert('" . $msg . "'); window.opener.location.reload(); window.close(); </script>";
            exit;
        }

    }

    //Paid Method
    function change_2_code($category, $method) {
        $return = "";
        if ($category == "source") {
            if ($method == 'BT:Canceled')
                $return = 'Canceled';
            else if ($method == 'BT:Checked_out')
                $return = 'Checked out';
            else if ($method == 'BT:No_Show')
                $return = 'No Show';
            else if ($method == 'BT:Reservation')
                $return = 'Reservation';
            else if ($method == 'BT:Unconfirmed_Reservation')
                $return = 'Unconfirmed Reservation';
            else if ($method == 'BT:Walk_in')
                $return = 'Walk in';
        } else if ($category == "ota") {
            if ($method == 'OTA:Agoda')
                $return = 'Agoda';
            else if ($method == 'OTA:Airbnb')
                $return = 'Airbnb';
            else if ($method == 'OTA:Asiayo')
                $return = 'Asiayo';
            else if ($method == 'OTA:Hospo')
                $return = 'Walk in / Telephone';
            else if ($method == 'OTA:Booking.com')
                $return = 'Booking.com';
            else if ($method == 'OTA:Expedia')
                $return = 'Expedia';
            else if ($method == 'OTA:Rakuten')
                $return = 'Rakuten';
            else if ($method == 'OTA:Zizaike')
                $return = 'Zizaike';
            else if ($method == 'OTA:Beds24')
                $return = 'Beds24';
        } else if ($category == "paid") {
            if ($method == 'PM:Cash')
                $return = 'Cash';
            else if ($method == 'PM:Credit_Card')
                $return = 'Credit Card';
            else if ($method == 'PM:Prepaid')
                $return = 'Prepaid';
        } else if ($category == 'booking') {
            if ($method == 'BS:Agoda')
                $return = 'Agoda';
            else if ($method == 'BS:AirBnb')
                $return = 'AirBnb';
            else if ($method == 'BS:Booking.com')
                $return = 'Booking.com';
            else if ($method == 'BS:Telephone')
                $return = 'Telephone';
            else if ($method == 'BS:Walk_in')
                $return = 'Walk in';
        }


        return $return;
    }

    function findKey($array, $key) {
        $result = false;
        foreach ($array as $k => $item) {
            if ($k == $key)
                $result = true;
        }

        return $result;
    }

    function findKeyVal($array, $key, $val) {
        $result = false;
        foreach ($array as $k => $item) {
            if (array_key_exists($key, $item) && $item[$key] == $val)
                $result = true;
        }

        return $result;
    }

    function findIndexWithKeyVal($array, $key, $val) {
        $result = -1;
        foreach ($array as $k => $item) {
            $result++;
            if (array_key_exists($key, $item) && $item[$key] == $val)
                break;
        }

        return $result;
    }

    function arrayGroupBy($array, $key) {
        $result = array();
        foreach ($array as $val) {
            $name = $val[$key];
            $result[$name][] = $val;
        }

        return $result;
    }

    function createDateRangeArray($strDateFrom, $strDateTo) {
        // takes two dates formatted as YYYY-MM-DD and creates an
        // inclusive array of the dates between the from and to dates.
        // could test validity of dates here but I'm already doing
        // that in the main script

        $aryRange = array();

        $iDateFrom = mktime(1, 0, 0, substr($strDateFrom, 5, 2), substr($strDateFrom, 8, 2), substr($strDateFrom, 0, 4));
        $iDateTo = mktime(1, 0, 0, substr($strDateTo, 5, 2), substr($strDateTo, 8, 2), substr($strDateTo, 0, 4));

        if ($iDateTo >= $iDateFrom) {
            array_push($aryRange, date('Y-m-d', $iDateFrom)); // first entry
            while ($iDateFrom < $iDateTo) {
                $iDateFrom += 86400; // add 24 hours
                array_push($aryRange, date('Y-m-d', $iDateFrom));
            }
        }
        return $aryRange;
    }


    function listOrderValue($orderTitle, $orderType, $headTitle){
        $value = '';
        if($orderTitle==$headTitle){
            if($orderType=='asc'){
                $value = "desc";
            }else if($orderType=='desc'){
                $value = "asc";
            }
        }else if($orderTitle!=$headTitle){
            $value = 'desc';
        }
        return $value;
    }

    function listOrderArrow($orderTitle, $orderType, $headTitle){
        $arrow = '↕';
        if($orderTitle==$headTitle){
            if($orderType=='asc'){
                $arrow = '↑';
            }else if($orderType=='desc'){
                $arrow = '↓';
            }
        }else{
            $arrow = '↕';
        }
        return $arrow;
    }



    function today_year(){
        $today_year= date("Y");
        return $today_year;
    }

    function today_month(){
        $month= date("m");
        $today_month = str_pad($month, 2, "0", STR_PAD_LEFT);
        return $today_month;
    }

    function last_year(){
        $year = date("Y", strtotime ("-1 months"));
        return $year;
    }
    function last_month(){
        $month = date("m", strtotime ("-1 months"));
        $month = str_pad($month, 2, "0", STR_PAD_LEFT);
        return $month;
    }



}
?>
